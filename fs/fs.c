/**
 * \file fs.c
 * \brief Filesystem support library
 */

#include <aos/aos.h>
#include <fs/fs.h>
#include <fs/dirent.h>
#include <fs/ramfs.h>
#include <fs/fatfs.h>
#include <mmchs/mmchs.h>

#include "fs_internal.h"

/*
 * Copyright (c) 2016 ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetsstrasse 6, CH-8092 Zurich. Attn: Systems Group.
 */

//BPB (Bios Parameter Block)
struct BPB * BPB;
//boot sector
struct boot_sector * boot_sector;
static errval_t filesystem_read_boot_sector(void);
static void print_bytes_from_offset(char * basePtr, size_t offset, 
    size_t size, enum BPB_bytePrintMode mode, char * prefix);

/**
 * @brief initializes the filesystem library
 *
 * @return SYS_ERR_OK on success
 *         errval on failure
 *
 * NOTE: This has to be called before any access to the files
 */
errval_t filesystem_init(void)
{
    errval_t err;

    /* TODO: Filesystem project: hook up your init code here */
    //initialize BPB struct
    BPB = malloc(sizeof(struct BPB));
    if (BPB == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }
    memset(BPB, 0, sizeof(struct BPB));

    //initialize boot sector struct
    boot_sector = malloc(sizeof(struct boot_sector));
    if (boot_sector == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }
    memset(boot_sector, 0, sizeof(struct boot_sector));
    boot_sector->BPB = BPB;

    mmchs_init();
   
    filesystem_read_boot_sector();

    //TODO: Make switching possible
    /*ramfs_mount_t st = NULL;
    err = ramfs_mount("/", &st);
    if (err_is_fail(err)) {
        return err;
    }*/

    fatfs_mount_t st = NULL;
    err = fatfs_mount("/", boot_sector, &st);
    if (err_is_fail(err)) {
        return err;
    }
    printf("Trig\n");

    //TODO: Fix
    /* register libc fopen/fread and friends */
    fs_libc_init(st);

    return SYS_ERR_OK;
}

/**
 * @brief mounts the URI at a give path
 *
 * @param path  path to mount the URI
 * @param uri   uri to mount
 *
 * @return SYS_ERR_OK on success
 *         errval on error
 *
 * This mounts the uri at a given, existing path.
 *
 * path: service-name://fstype/params
 */
errval_t filesystem_mount(const char *path, const char *uri)
{
    return LIB_ERR_NOT_IMPLEMENTED;
}

/**
 * @brief helper function of filesystem_read_boot_sector
 *
 * @param basePtr Pointer to add offset to
 * @param offset basePtr + offset points to the start of the bytes to print
 * @param size Number of bytes to print
 * @param mode How to print the bytes
 * @param prefix What to print before, must be null terminated
 */
void print_bytes_from_offset(char * basePtr, size_t offset, 
    size_t size, enum BPB_bytePrintMode mode, char * prefix) {
    char * it = basePtr + offset;
    char * end = basePtr + offset + size;
    printf("%s ", prefix);
    switch (mode) {
        case BPB_SINGLE_BYTES:
            for (; it < end; it++) {
                printf("0x%x ", *it);
            }
            printf("\n");
            break;
        case BPB_STRING:
            for (; it < end; it++) {
                printf("%c", *it);
            }
            printf("\n");
            break;
        case BPB_INTEGER:
            if (size == 2) {
                printf("%d\n", *((uint16_t *) it));
            } else if (size == 4) {
                printf("%d\n", *((uint32_t *) it));
            }
            break;
        default:
            printf("Unrecognized mode\n");
    }
}

/**
* @brief Reads in the BPB (Bios Parameter Block) of the filesystem on the SD card
*/
errval_t filesystem_read_boot_sector(void) {
    errval_t err;

    //get boot sector with BPB (Bios Parameter Block)
    //Note: boot_sector_ptr must remain a char * to avoid pointer arithmetic problems
    char * boot_sector_ptr = malloc(512);
    if (boot_sector_ptr == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }
    memset(boot_sector_ptr, 0, 512);

    err = mmchs_read_block(0, boot_sector_ptr);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Failure when trying to read boot sector (sector 0)\n");
        return err;
    }


    boot_sector->boot_sector_ptr = boot_sector_ptr;


    //set and print out BPB (note that not everything is saved in the BPB struct)
    printf("//////  Boot Sector >>> START /////////////\n");
    printf("//////  Boot Sector (BPB) >>> START ///////\n");
    print_bytes_from_offset(boot_sector_ptr, BS_jmpBoot_offset, BS_jmpBoot_size,
     BPB_SINGLE_BYTES, "BS_jmpBoot:");

    print_bytes_from_offset(boot_sector_ptr, BS_OEMName_offset, BS_OEMName_size,
     BPB_STRING, "BS_OEMName:");

    BPB->BPB_BytsPerSec = *((uint16_t *) (boot_sector_ptr + BPB_BytsPerSec_offset));
    assert(BPB->BPB_BytsPerSec == 512);
    print_bytes_from_offset(boot_sector_ptr, BPB_BytsPerSec_offset, BPB_BytsPerSec_size,
     BPB_INTEGER, "BPB_BytsPerSec:");

    BPB->BPB_SecPerClus = *((uint8_t *) (boot_sector_ptr + BPB_SecPerClus_offset));
    assert(BPB->BPB_SecPerClus == 8);
    assert(BPB->BPB_SecPerClus*BPB->BPB_BytsPerSec <= (2 << 15));
    print_bytes_from_offset(boot_sector_ptr, BPB_SecPerClus_offset, BPB_SecPerClus_size,
     BPB_SINGLE_BYTES, "BPB_SecPerClus:");

    BPB->BPB_RsvdSecCnt = *((uint16_t *) (boot_sector_ptr + BPB_RsvdSecCnt_offset));
    assert(BPB->BPB_RsvdSecCnt == 32);
    print_bytes_from_offset(boot_sector_ptr, BPB_RsvdSecCnt_offset, BPB_RsvdSecCnt_size,
     BPB_INTEGER, "BPB_RsvdSecCnt:");

    BPB->BPB_NumFATs = *((uint8_t *) (boot_sector_ptr + BPB_NumFATs_offset));
    assert(BPB->BPB_NumFATs == 2);
    print_bytes_from_offset(boot_sector_ptr, BPB_NumFATs_offset, BPB_NumFATs_size,
     BPB_SINGLE_BYTES, "BPB_NumFATs:");

    BPB->BPB_RootEntCnt = *((uint16_t *) (boot_sector_ptr + BPB_RootEntCnt_offset));
    assert(BPB->BPB_RootEntCnt == 0);
    print_bytes_from_offset(boot_sector_ptr, BPB_RootEntCnt_offset, BPB_RootEntCnt_size,
     BPB_INTEGER, "BPB_RootEntCnt:");

    BPB->BPB_TotSec16 = *((uint16_t *) (boot_sector_ptr + BPB_TotSec16_offset));
    assert(BPB->BPB_TotSec16 == 0);
    print_bytes_from_offset(boot_sector_ptr, BPB_TotSec16_offset, BPB_TotSec16_size,
     BPB_INTEGER, "BPB_TotSec16:");

    print_bytes_from_offset(boot_sector_ptr, BPB_Media_offset, BPB_Media_size,
     BPB_SINGLE_BYTES, "BPB_Media:");

    BPB->BPB_FATSz16 = *((uint16_t *) (boot_sector_ptr + BPB_FATSz16_offset));
    assert(BPB->BPB_FATSz16 == 0);
    print_bytes_from_offset(boot_sector_ptr, BPB_FATSz16_offset, BPB_FATSz16_size,
     BPB_INTEGER, "BPB_FATSz16:");

    print_bytes_from_offset(boot_sector_ptr, BPB_SecPerTrk_offset, BPB_SecPerTrk_size,
     BPB_INTEGER, "BPB_SecPerTrk:");

    print_bytes_from_offset(boot_sector_ptr, BPB_NumHeads_offset, BPB_NumHeads_size,
     BPB_INTEGER, "BPB_NumHeads:");

    BPB->BPB_HiddSec = *((uint32_t *) (boot_sector_ptr + BPB_HiddSec_offset));
    //assert(BPB->BPB_HiddSec == 0);
    print_bytes_from_offset(boot_sector_ptr, BPB_HiddSec_offset, BPB_HiddSec_size,
     BPB_INTEGER, "BPB_HiddSec:");

    BPB->BPB_TotSec32 = *((uint32_t *) (boot_sector_ptr + BPB_TotSec32_offset));
    //assert(BPB->BPB_TotSec32 > 0);
    print_bytes_from_offset(boot_sector_ptr, BPB_TotSec32_offset, BPB_TotSec32_size,
     BPB_INTEGER, "BPB_TotSec32:");

    BPB->BPB_FATSz32 = *((uint32_t *) (boot_sector_ptr + BPB_FATSz32_offset));
    print_bytes_from_offset(boot_sector_ptr, BPB_FATSz32_offset, BPB_FATSz32_size,
     BPB_INTEGER, "BPB_FATSz32:");

    print_bytes_from_offset(boot_sector_ptr, BPB_ExtFlags_offset, BPB_ExtFlags_size,
     BPB_SINGLE_BYTES, "BPB_ExtFlags:");

    BPB->BPB_FSVer_major = *((uint8_t *) (boot_sector_ptr + BPB_FSVer_offset + 1));
    BPB->BPB_FSVer_minor = *((uint8_t *) (boot_sector_ptr + BPB_FSVer_offset));
    print_bytes_from_offset(boot_sector_ptr, BPB_FSVer_offset, BPB_FSVer_size,
     BPB_SINGLE_BYTES, "BPB_FSVer:");

    BPB->BPB_RootClus = *((uint32_t *) (boot_sector_ptr + BPB_RootClus_offset));
    print_bytes_from_offset(boot_sector_ptr, BPB_RootClus_offset, BPB_RootClus_size,
     BPB_INTEGER, "BPB_RootClus:");

    BPB->BPB_FSInfo = *((uint16_t *) (boot_sector_ptr + BPB_FSInfo_offset));
    print_bytes_from_offset(boot_sector_ptr, BPB_FSInfo_offset, BPB_FSInfo_size,
     BPB_INTEGER, "BPB_FSInfo:");

    BPB->BPB_BkBootSec = *((uint16_t *) (boot_sector_ptr + BPB_BkBootSec_offset));
    print_bytes_from_offset(boot_sector_ptr, BPB_BkBootSec_offset, BPB_BkBootSec_size,
     BPB_INTEGER, "BPB_BkBootSec:");

    print_bytes_from_offset(boot_sector_ptr, BPB_Reserved_offset, BPB_Reserved_size,
     BPB_SINGLE_BYTES, "BPB_Reserved:");

    print_bytes_from_offset(boot_sector_ptr, BS_DrvNum_offset, BS_DrvNum_size,
     BPB_SINGLE_BYTES, "BS_DrvNum:");

    print_bytes_from_offset(boot_sector_ptr, BS_Reserved1_offset, BS_Reserved1_size,
     BPB_SINGLE_BYTES, "BS_Reserved1:");
    printf("//////  Boot Sector (BPB) >>> END ///////\n");


    boot_sector->BS_BootSig = *((uint8_t *) (boot_sector_ptr + BS_BootSig_offset));
    print_bytes_from_offset(boot_sector_ptr, BS_BootSig_offset, BS_BootSig_size,
     BPB_SINGLE_BYTES, "BS_BootSig:");

    if (boot_sector->BS_BootSig == 0x29) {
        boot_sector->BS_VolID = *((uint32_t *) (boot_sector_ptr + BS_VolID_offset));
        print_bytes_from_offset(boot_sector_ptr, BS_VolID_offset, BS_VolID_size,
         BPB_SINGLE_BYTES, "BS_VolID:");

        boot_sector->BS_VolLab = ((char *) (boot_sector_ptr + BS_VolLab_offset));
        print_bytes_from_offset(boot_sector_ptr, BS_VolLab_offset, BS_VolLab_size,
         BPB_STRING, "BS_VolLab:");

        boot_sector->BS_FilSysType = ((char *) (boot_sector_ptr + BS_FilSysType_offset));
        print_bytes_from_offset(boot_sector_ptr, BS_FilSysType_offset, BS_FilSysType_size,
         BPB_STRING, "BS_FilSysType:");        
    }
    printf("0x%x 0x%x\n", boot_sector_ptr[510], boot_sector_ptr[511]);
    assert(boot_sector_ptr[510] == 0x55 && boot_sector_ptr[511] == 0xAA);
    printf("//////  Boot Sector >>> END ////////////\n");

    //Sanity check:
    uint32_t sizeInSectors = (2 << 23); //num sectors on 4GB card
    printf("sizeInSectors=%d BPB->TotSec32=%d\n", sizeInSectors, BPB->BPB_TotSec32);
    assert(sizeInSectors >= BPB->BPB_TotSec32);


    return SYS_ERR_OK;
}
