/*
 * Copyright (c) 2009, 2011, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Haldeneggsteig 4, CH-8092 Zurich. Attn: Systems Group.
 */

#include <stdio.h>
#include <string.h>
#include <aos/aos.h>

#include <fs/fs.h>
#include <fs/fatfs.h>
#include <mmchs/mmchs.h>

#include "fs_internal.h"


union char_or_uint32_t {
    char chars[4];
    uint32_t integer;
};

/**
 * @brief An entry in a directory -> NOT the same as an entry in the FAT (which is only 4 bytes)
 */
struct fatfs_dirent
{
    char *name;                     ///< name of the file or directory
    size_t size;                    ///< the size of the direntry in bytes or files in main memory
    size_t actual_size;             ///< the size of the direntry in bytes or files on the SD card
    size_t refcount;                ///< reference count for open handles
    struct fatfs_dirent *parent;    ///< parent directory

    struct fatfs_dirent *next;
    struct fatfs_dirent *prev;

    bool is_dir;                    ///< flag indicationg this is a dir
    
    fatfs_fat_entry_t * fat_entry;  ///< pointer to the first FAT entry of the file or directory

    union {
        void *data;                 ///< file data pointer
        struct fatfs_dirent *dir;   ///< directory pointer
    };
};

struct __attribute__((packed)) fatfs_info_sector {
    char FSI_LeadSig[4]; //should be 0x41615252
    char FSI_Reserved1[480]; //should be all zero
    char FSI_StrucSig[4]; //should be 0x61417272
    char FSI_Free_Count[4]; //if 0xFFFFFFFF unknown
    char FSI_Nxt_Free[4]; //next free cluster number, if 0xFFFFFFFF unknown
    char FSI_Reserved2[12]; //should be all zero
    char FSI_TrailSig[4]; //should be 0xAA550000
};

/**
 * @brief This corresponds to the format of a FAT dirent on the SD card, it has to packed!!!
 */
struct __attribute__ ((packed)) fatfs_dirent_internal
{
    char DIR_Name[11];
    char DIR_Attr;
    char DIR_NTRes;
    char DIR_CrtTimeTenth;
    char DIR_CrtTime[2];
    char DIR_CrtDate[2];
    char DIR_LstAccDate[2];
    char DIR_FstClusHI[2];
    char DIR_WrtTime[2];
    char DIR_WrtDate[2];
    char DIR_FstClusLO[2];
    char DIR_FileSize[4]; 
};

/**
 * @brief a handle to the open
 */
struct fatfs_handle
{
    struct fs_handle common;
    char *path;
    bool isdir;
    struct fatfs_dirent *dirent;
    union {
        off_t file_pos;
        struct fatfs_dirent *dir_pos;
    };
};

struct fatfs_mount {
    //regarding shadow fs in memory
    struct fatfs_dirent *root;
    
    //regarding SD card
    struct fatfs_info_sector * info_sector;
    struct boot_sector *boot_sector;
    fatfs_fat_entry_t *start_FAT;
    fatfs_fat_entry_t * end_FAT;
    fatfs_fat_entry_t * start_FAT_0;
    fatfs_fat_entry_t * start_FAT_1;
    uint32_t FATSz; //number of sectors in ONE FAT
    uint32_t first_data_sec; //the first data sector
    uint32_t count_of_clusters; //total number of DATA sectors, start at 2!
    uint32_t num_data_sec; //total number of data sectors
    uint32_t totSec; //total number of sectors
    uint32_t next_free_cluster;
};

/**
 * @brief Open a fatfs_handle given by fatfs_dirent.
 *
 * @param d     Pointer to a directory entry.
*/
static struct fatfs_handle *handle_open(struct fatfs_dirent *d)
{
    struct fatfs_handle *h = calloc(1, sizeof(*h));
    if (h == NULL) {
        return NULL;
    }

    d->refcount++;
    h->isdir = d->is_dir;
    h->dirent = d;

    return h;
}

/**
 * @brief Close a fatfs_handle.
 *
 * @param h     File handle
*/
static inline void handle_close(struct fatfs_handle *h)
{
    assert(h->dirent->refcount > 0);
    h->dirent->refcount--;
    free(h->path);
    free(h);
}


/**
 * @brief Remove a file given by dirent. Helper of dirent_remove_and_free. Don't call except in dirent_remove_and_free.
 *
 * @param entry     Pointer to dirent of file we are trying to remove
*/
static void dirent_remove(struct fatfs_dirent *entry)
{
    if (entry->prev == NULL) {
        /* entry was the first in list, update parent pointer */
        if (entry->parent) {
            assert(entry->parent->is_dir);
            entry->parent->dir = entry->next;
        }
    } else {
        /* there are entries before that one */
        entry->prev->next = entry->next;
    }

    if (entry->next) {
        /* update prev pointer */
        entry->next->prev = entry->prev;
    }
}

/**
 * @brief Remove a file given by dirent.
 *
 * @param entry     Pointer to dirent of file we are trying to remove
*/
static void dirent_remove_and_free(struct fatfs_dirent *entry)
{
    dirent_remove(entry);
    free(entry->name);
    if (!entry->is_dir) {
        free(entry->data);

        //TODO: Make sure everything is written to disk
    }

    memset(entry, 0x00, sizeof(*entry));
    free(entry);

    //TODO: Actually remove file in file system, change FAT, do it atomicly
    //TODO: Remove from FAT and out of directory
}

/**
 * @brief Insert a file given by dirent in a parent directory.
 *
 * @param parent    Parent directory
 * @param entry     Pointer to dirent of file we are trying to insert
 * @param init      True if we are just reading in the directory structure and do not want to create anything
*/
static void dirent_insert(struct fatfs_dirent *parent, struct fatfs_dirent *entry, bool init)
{
    assert(parent);
    assert(parent->is_dir);

    entry->next = NULL;
    entry->prev = NULL;
    entry->parent = parent;
    if (parent->dir) {
        entry->next = parent->dir;
        parent->dir->prev= entry;
    }

    parent->dir = entry;

    //TODO: Actually insert file in file system, change FAT, do it atomicly
    if (!init) {
    
    }
}


/**
 * @brief Create a directory entry with name.
 *
 * @param name      Name of the file we are creating
 * @param is_dir    Is it supposed to be a directory
 * @param init      true if we are just reading in directory structure and don't want to do anything else
 *
 * @returns pointer to a fatfs_dirent
*/
static struct fatfs_dirent *dirent_create(const char *name, bool is_dir, bool init)
{
    struct fatfs_dirent *d = calloc(1, sizeof(*d));
    if (d == NULL) {
        return NULL;
    }

    d->is_dir = is_dir;
    d->name = strdup(name);

    return d;
}

/**
 * @brief Find a directory entry by name.
 *
 * @param root      The directory entry of the directory the file should be in
 * @param name      Name as a string
 * @param ret_de To return a directory entry
 *
 * @returns SYS_ERR_OK
 *          FS_ERR_NOTFOUND If we cannot find the directory entry we are looking for
 *          FS_ERR_NOTDIR If the parent is no directory
*/
static errval_t find_dirent(struct fatfs_dirent *root, const char *name,
                            struct fatfs_dirent **ret_de)
{
    if (!root->is_dir) {
        return FS_ERR_NOTDIR;
    }

    struct fatfs_dirent *d = root->dir;

    while(d) {
        if (strcmp(d->name, name) == 0) {
            *ret_de = d;
            return SYS_ERR_OK;
        }

        d = d->next;
    }

    return FS_ERR_NOTFOUND;
}


/**
 * @brief Resolve a path and create a fatfs_handle.
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param path      Path as a string
 * @param rethandle To return a handle to a file.
 *
 * @returns SYS_ERR_OK
 *          FS_ERR_EXIST If the file already exists
 *          FS_ERR_NOTDIR If the parent is no directory
*/
static errval_t resolve_path(struct fatfs_dirent *root, const char *path,
                             struct fatfs_handle **ret_fh)
{
    errval_t err;

    // skip leading /
    size_t pos = 0;
    if (path[0] == FS_PATH_SEP) {
        pos++;
    }

    struct fatfs_dirent *next_dirent;

    while (path[pos] != '\0') {
        char *nextsep = strchr(&path[pos], FS_PATH_SEP);
        size_t nextlen;
        if (nextsep == NULL) {
            nextlen = strlen(&path[pos]);
        } else {
            nextlen = nextsep - &path[pos];
        }

        char pathbuf[nextlen + 1];
        memcpy(pathbuf, &path[pos], nextlen);
        pathbuf[nextlen] = '\0';

        err = find_dirent(root, pathbuf, &next_dirent);
        if (err_is_fail(err)) {
            return err;
        }

        if (!next_dirent->is_dir && nextsep != NULL) {
            return FS_ERR_NOTDIR;
        }

        root = next_dirent;
        if (nextsep == NULL) {
            break;
        }

        pos += nextlen + 1;
    }

    /* create the handle */

    if (ret_fh) {
        struct fatfs_handle *fh = handle_open(root);
        if (fh == NULL) {
            return LIB_ERR_MALLOC_FAIL;
        }

        fh->path = strdup(path);
        //fh->common.mount = root;

        *ret_fh = fh;
    }

    return SYS_ERR_OK;
}

static errval_t get_fat_entry(void * st, uint32_t cluster_num, uint32_t * sec_num, uint32_t * offset, fatfs_fat_entry_t ** ret) {

    struct fatfs_mount * mount = (struct fatfs_mount *) st;
    struct BPB * BPB = mount->boot_sector->BPB;
    printf("get_fat_entry: cluster_num = %d\n", cluster_num);
    //validate
    if (cluster_num < BPB->BPB_RootClus || cluster_num > mount->count_of_clusters + 1) {
        return FAT_ERR_CLUSTER_BOUNDS;
    }
    
    uint32_t fat_offset = cluster_num * 4;
    uint32_t sec_num_local = BPB->BPB_RsvdSecCnt + (fat_offset / BPB->BPB_BytsPerSec);
    uint32_t offset_local = fat_offset % BPB->BPB_BytsPerSec;
    if (sec_num) {
        *sec_num = sec_num_local;
    }
    if (offset) {
        *offset = offset_local;
    }
    
    if (ret) {

        *ret = mount->start_FAT + cluster_num;
            printf("mount->start_FAT : %p, cluster_num : %p, *ret  : %p, *mount->start_FAT %p\n", mount->start_FAT, cluster_num, *ret, *mount->start_FAT);
    }
    return SYS_ERR_OK;
}

static errval_t get_sec_of_cluster(void * st, uint32_t cluster_num, uint32_t sec_offset, void ** ret) {
    printf("get_sec_of_cluster: cluster_num = 0x%x\n", cluster_num);
    struct fatfs_mount * mount = (struct fatfs_mount *) st;
    struct BPB * BPB = mount->boot_sector->BPB;
    errval_t err;
    
    //validate
    if (cluster_num < BPB->BPB_RootClus || cluster_num > mount->count_of_clusters + 1 
            || sec_offset > BPB->BPB_SecPerClus - 1) {
        return FAT_ERR_CLUSTER_BOUNDS;
    }
    
    uint32_t sec_num = ((BPB->BPB_SecPerClus * (cluster_num - 2)) + mount->first_data_sec) + sec_offset;
    printf("get_sec_of_cluster: Loading sector %d\n", sec_num);
    
    if (ret) {
        *ret = malloc(512);
        if (*ret == NULL) {
            return LIB_ERR_MALLOC_FAIL;
        }
        err = mmchs_read_block(sec_num, *ret);
        if (err_is_fail(err)) {
            DEBUG_ERR(err, "Failure when trying to read sector %d\n", sec_num);
            return err;
        }
    }
    return SYS_ERR_OK;
}



__attribute__((unused))
static errval_t set_fat_entry(void *st) {
    
    //TODO: Implement
    
    return SYS_ERR_OK;
}

static errval_t load_dirs_from_FAT(void *st) {
    errval_t err;
    struct fatfs_mount * mount = (struct fatfs_mount *) st;
    struct BPB * BPB = mount->boot_sector->BPB;
    fatfs_fat_entry_t *e = NULL;
    
    //set fat entry on the root directory
    err = get_fat_entry(mount, BPB->BPB_RootClus, NULL, NULL, &e);
    if (err_is_fail(err)) {
        return err;
    }
    mount->root->fat_entry = e;
    
    //load 1. sector of root directory
    void * data;
    err = get_sec_of_cluster(mount, BPB->BPB_RootClus, 0, &data);
    if (err_is_fail(err)) {
        return err;
    }
    
    //traverse the directory
    struct fatfs_dirent_internal * fatfs_root_dirent_internal = (struct fatfs_dirent_internal *) data;
    
    uint8_t queue[1024]; //directory we still need to traverse
    uint32_t next_dir = 0;
    uint32_t next_free_slot = 0;
    struct fatfs_dirent *current_directory = mount->root;
    fatfs_fat_entry_t * current_e = e;
    union char_or_uint32_t current_cluster_num;
    union char_or_uint32_t converter;
    current_cluster_num.integer = BPB->BPB_RootClus;
    struct fatfs_dirent *dir;
    struct fatfs_dirent *file;
    struct fatfs_dirent_internal *it = fatfs_root_dirent_internal;
    uint32_t inner_loop_count = 0;
    uint32_t sector_count = 0;
    
    do {
        while (it->DIR_Name[0] != 0x0 && inner_loop_count < 16) {
            if (it->DIR_Name[0] == 0xEF) { //empty dirent
                inner_loop_count++;
                continue;
            }
            if (it->DIR_Attr & 0x20) { //skip archive files
                inner_loop_count++;
                continue;
            }
            if (it->DIR_Name[0] == '.') {
                inner_loop_count++;
                continue;
            }
            printf("Parent fat_entry = 0x%x\n", (*current_e & FAT_ENTRY_MASK));
            printf("DIR_Name: %c", it->DIR_Name[0]);
            for (int i = 1; i < 11; i++) {
                printf("%c", it->DIR_Name[i]);
            }
            printf("\n");
            it->DIR_Name[10] = '\0';
            printf("DIR_Attr: 0x%x\n", it->DIR_Attr);
            printf("DIR_NTRes: 0x%x\n", it->DIR_NTRes);
            printf("DIR_CrtTimeTenth: 0x%x\n", it->DIR_CrtTimeTenth);
        
            //If it is a directory, add it to queue to be traversed later
            if (it->DIR_Attr & 0x10) {
                if (it->DIR_Name[0] != '.') { //exclude . and ..
                    queue[next_free_slot++] = it->DIR_FstClusLO[0];
                    queue[next_free_slot++] = it->DIR_FstClusLO[1];
                    queue[next_free_slot++] = it->DIR_FstClusHI[0];
                    queue[next_free_slot++] = it->DIR_FstClusHI[1];
                    if (next_free_slot == 1024) {
                        next_free_slot = 0;
                    }
                }
                
                //create and insert directory
                dir = dirent_create(it->DIR_Name, 0x1, true);
                converter.chars[0] = it->DIR_FstClusLO[0];
                converter.chars[1] = it->DIR_FstClusLO[1];
                converter.chars[2] = it->DIR_FstClusHI[0];
                converter.chars[3] = it->DIR_FstClusHI[1];
                printf("converter.integer= %d\n", converter.integer);
                if (converter.integer != 0) {
                    err = get_fat_entry(mount, converter.integer, NULL, NULL, &dir->fat_entry);
                    if (err_is_fail(err)) {
                        dir->fat_entry = NULL;
                    }
                }
                converter.chars[0] = it->DIR_FileSize[0];
                converter.chars[1] = it->DIR_FileSize[1];
                converter.chars[2] = it->DIR_FileSize[2];
                converter.chars[3] = it->DIR_FileSize[3];
                dir->actual_size = converter.integer;
                printf("DIR_FileSize=%d\n", dir->actual_size);
                printf("DIR_FstClus=%d\n", converter.integer);
                printf("dir->fat_entry=0x%x\n", dir->fat_entry == NULL ? 0 : *dir->fat_entry);
                dirent_insert(current_directory, dir, true);
                
            } else {
                //create and insert file
                file = dirent_create(it->DIR_Name, 0x0, true);
                converter.chars[0] = it->DIR_FstClusLO[0];
                converter.chars[1] = it->DIR_FstClusLO[1];
                converter.chars[2] = it->DIR_FstClusHI[0];
                converter.chars[3] = it->DIR_FstClusHI[1];
                printf("converter.integer= %u\n", converter.integer);
                if (converter.integer > 0) {
                    err = get_fat_entry(mount, converter.integer, NULL, NULL, &file->fat_entry);
                    if (err_is_fail(err)) {
                        file->fat_entry = NULL;
                    }
                }
                converter.chars[0] = it->DIR_FileSize[0];
                converter.chars[1] = it->DIR_FileSize[1];
                converter.chars[2] = it->DIR_FileSize[2];
                converter.chars[3] = it->DIR_FileSize[3];
                file->actual_size = converter.integer;
                dirent_insert(current_directory, file, true);
                
            }
            printf("***************************************\n");
            inner_loop_count++;
            it++;
        } 
        printf("================================================\n");
        inner_loop_count = 0;
        sector_count++;
        if (sector_count < 4 && it->DIR_Name[0] != 0x0) {
            printf("Get another sector, sector_count=%d.\n", sector_count);
            err = get_sec_of_cluster(mount, current_cluster_num.integer, sector_count, &data);
            if (err_is_fail(err)) {
                return err;
            }
            it = (struct fatfs_dirent_internal *) data;
            
        } else if ((*current_e & FAT_ENTRY_MASK) >= FAT_EOC && (next_free_slot == next_dir)) { //we are done
            printf("We are done.\n");
            break;
        } else if ((*current_e & FAT_ENTRY_MASK) >= FAT_EOC) { //we are done with current directory, get next
            printf("Done with current directory, get next.\n");
            sector_count = 0;
            current_cluster_num.chars[0] = queue[next_dir++];
            current_cluster_num.chars[1] = queue[next_dir++];
            current_cluster_num.chars[2] = queue[next_dir++];
            current_cluster_num.chars[3] = queue[next_dir++];
            if (next_dir == 1024) {
                next_dir = 0;
            }
            
            err = get_fat_entry(mount, current_cluster_num.integer, NULL, NULL, &current_e); 
            if (err_is_fail(err)) {
                return err;
            }
            err = get_sec_of_cluster(mount, current_cluster_num.integer, 0, &data);
            if (err_is_fail(err)) {
                return err;
            }
            it = (struct fatfs_dirent_internal *) data;
            
        } else { //there are more clusters for this directory
            printf("There are more clusters for this directory, fat_entry=0x%x.\n", (*current_e & FAT_ENTRY_MASK));
            sector_count = 0;
            printf("There are more clusters for this directory, fat_entry=0x%x.\n", (*current_e & FAT_ENTRY_MASK));
            err = get_sec_of_cluster(mount, (*current_e & FAT_ENTRY_MASK), 0, &data);
            if (err_is_fail(err)) {
                return err;
            }
            err = get_fat_entry(mount, (*current_e & FAT_ENTRY_MASK), NULL, NULL, &current_e); 
            if (err_is_fail(err)) {
                return err;
            }

            
            it = (struct fatfs_dirent_internal *) data;
        }
    } while (true);
    printf("Loading directory structure: Done.\n");
    return SYS_ERR_OK;
}

/**
 * @brief Open a file.
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param path      Path as a string
 * @param rethandle To return a handle to the created file, can be NULL
 *
 * @returns SYS_ERR_OK
 *          FS_ERR_NOTFILE If it is a directory
*/
errval_t fatfs_open(void *st, const char *path, fatfs_handle_t *rethandle)
{
    errval_t err;

    struct fatfs_mount *mount = st;

    struct fatfs_handle *handle;
    err = resolve_path(mount->root, path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    if (handle->isdir) {
        handle_close(handle);
        return FS_ERR_NOTFILE;
    }

    *rethandle = handle;

    return SYS_ERR_OK;
}

/**
 * @brief Create a file.
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param path      Path as a string
 * @param rethandle To return a handle to the created file, can be NULL
 *
 * @returns SYS_ERR_OK
 *          FS_ERR_EXIST If the file already exists
 *          FS_ERR_NOTDIR If the parent is no directory
*/
errval_t fatfs_create(void *st, const char *path, fatfs_handle_t *rethandle)
{
    errval_t err;

    struct fatfs_mount *mount = st;

    err = resolve_path(mount->root, path, NULL);
    if (err_is_ok(err)) {
        return FS_ERR_EXISTS;
    }

    struct fatfs_handle *parent = NULL;
    const char *childname;

    // find parent directory
    char *lastsep = strrchr(path, FS_PATH_SEP);
    if (lastsep != NULL) {
        childname = lastsep + 1;

        size_t pathlen = lastsep - path;
        char pathbuf[pathlen + 1];
        memcpy(pathbuf, path, pathlen);
        pathbuf[pathlen] = '\0';

        // resolve parent directory
        err = resolve_path(mount->root, pathbuf, &parent);
        if (err_is_fail(err)) {
            return err;
        } else if (!parent->isdir) {
            return FS_ERR_NOTDIR; // parent is not a directory
        }
    } else {
        childname = path;
    }

    struct fatfs_dirent *dirent = dirent_create(childname, false, false);
    if (dirent == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }

    if (parent) {
        dirent_insert(parent->dirent, dirent, false);
        handle_close(parent);
    } else {
        dirent_insert(mount->root, dirent, false);
    }

    if (rethandle) {
        struct fatfs_handle *fh = handle_open(dirent);
        if (fh  == NULL) {
            return LIB_ERR_MALLOC_FAIL;
        }
        fh->path = strdup(path);
        *rethandle = fh;
    }

    return SYS_ERR_OK;
}

/**
 * @brief This is like rm, it removes a file and fails on directories.
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param handle    Handle to a file that we want to append to
 * @param buffer    The stuff we want to write
 * @param bytes     The number of bytes that we want to write
 * @param bytes_written The number of actually written bytes
 *
 * @returns SYS_ERR_OK
 *          FS_ERR_NOTFILE If it is a directory
 *          FS_ERR_BUSY If there are other handles around
*/
errval_t fatfs_remove(void *st, const char *path)
{
    errval_t err;

    struct fatfs_mount *mount = st;

    struct fatfs_handle *handle;
    err = resolve_path(mount->root, path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    if (handle->isdir) {
        return FS_ERR_NOTFILE;
    }

    struct fatfs_dirent *dirent = handle->dirent;
    if (dirent->    refcount != 1) {
        handle_close(handle);
        return FS_ERR_BUSY;
    }


    dirent_remove_and_free(dirent);

    return SYS_ERR_OK;
}

/**
 * @brief This reads from a file, the number of actually read bytes is returned in bytes_read.
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param handle    Handle to a file that we want to read from
 * @param buffer    Destination buffer
 * @param bytes     The number of bytes that we want to read
 * @param bytes_read The number of actually read bytes
 *
 * @returns SYS_ERR_OK
            FS_ERR_NOTFILE If it is a directory
*/
errval_t fatfs_read(void *st, fatfs_handle_t handle, void *buffer, size_t bytes,
                           size_t *bytes_read)
{
    struct fatfs_handle *h = handle;
    errval_t err;
    if (h->isdir) {
        return FS_ERR_NOTFILE;
    }

    assert(h->file_pos >= 0);

    if (h->dirent->actual_size == 0) {
        bytes = 0;
    }
    else if (h->dirent->data == NULL) {
        //TODO: Allow multiple sectors and clusters to be read
        //Do a lazy read
        h->dirent->size = h->dirent->actual_size < 512 ? h->dirent->actual_size : 512;
        h->dirent->data = malloc(512);
        if (h->dirent->data == NULL) {
            return LIB_ERR_MALLOC_FAIL;
        }
        err = get_sec_of_cluster(st, *h->dirent->fat_entry & FAT_ENTRY_MASK, 0, &h->dirent->data);
        if (err_is_fail(err)) {
            return err;
        }
        bytes = h->dirent->size > bytes ? bytes: h->dirent->size;
    } else if (h->dirent->actual_size < h->file_pos) {
        bytes = 0;
    } else if (h->dirent->size < h->file_pos + bytes) {
        bytes = h->dirent->size - h->file_pos;
        assert(h->file_pos + bytes == h->dirent->size);
    }

    memcpy(buffer, h->dirent->data + h->file_pos, bytes);

    h->file_pos += bytes;

    *bytes_read = bytes;

    return SYS_ERR_OK;
}

/**
 * @brief This writes to a file, the number of actually written bytes is returned in bytes written
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param handle    Handle to a file that we want to append to
 * @param buffer    The stuff we want to write
 * @param bytes     The number of bytes that we want to write
 * @param bytes_written The number of actually written bytes
 *
 * @returns SYS_ERR_OK
            FS_ERR_NOTFILE If it is a directory
*/
errval_t fatfs_write(void *st, fatfs_handle_t handle, const void *buffer,
                            size_t bytes, size_t *bytes_written)
{
    struct fatfs_handle *h = handle;
    assert(h->file_pos >= 0);

    size_t offset = h->file_pos;

    if (h->isdir) {
        return FS_ERR_NOTFILE;
    }

    if (h->dirent->size < offset + bytes) {
        /* need to realloc the buffer */
        void *newbuf = realloc(h->dirent->data, offset + bytes);
        if (newbuf == NULL) {
            return LIB_ERR_MALLOC_FAIL;
        }
        h->dirent->data = newbuf;
    }

    memcpy(h->dirent->data + offset, buffer, bytes);

    if (bytes_written) {
        *bytes_written = bytes;
    }

    h->file_pos += bytes;
    h->dirent->size += bytes;

    //TODO: Implement, keep in mind that this has to be atomic!!!
    //TODO: Consider to defer this to speed things up

    return SYS_ERR_OK;
}

/**
 * @brief This takes a handle to a file (not dir) and truncates it to be of bytes size
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param handle    Handle to a file that is not a directory
 *
 * @returns SYS_ERR_OK
            FS_ERR_NOTFILE If it is a directory
*/
errval_t fatfs_truncate(void *st, fatfs_handle_t handle, size_t bytes)
{
    struct fatfs_handle *h = handle;

    if (h->isdir) {
        return FS_ERR_NOTFILE;
    }

    void *newdata = realloc(h->dirent->data, bytes);
    if (newdata == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }

    //TODO: Implement, keep in mind that this has to be atomic!!!

    h->dirent->data = newdata;
    h->dirent->size = bytes;

    return SYS_ERR_OK;
}

/**
 * @brief This takes a handle to a file (can be directory or a normal file) and sets pos to file_pos or 0 if handle points a dir
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param handle    Handle to a file
 *
 * @returns SYS_ERR_OK Always.
*/
errval_t fatfs_tell(void *st, fatfs_handle_t handle, size_t *pos)
{
    struct fatfs_handle *h = handle;
    if (h->isdir) {
        *pos = 0;
    } else {
        *pos = h->file_pos;
    }
    return SYS_ERR_OK;
}

/**
 * @brief This takes a handle to a file (can be directory or a normal file) and fills out the struct fs_fileinfo * info
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param info      This gets filled out, should not be a NULL pointer ...
 *
 * @returns SYS_ERR_OK Always.
*/
errval_t fatfs_stat(void *st, fatfs_handle_t inhandle, struct fs_fileinfo *info)
{
    struct fatfs_handle *h = inhandle;

    assert(info != NULL);
    info->type = h->isdir ? FS_DIRECTORY : FS_FILE;
    info->size = h->dirent->size;

    return SYS_ERR_OK;
}

//TODO: Find out how this works ...
errval_t fatfs_seek(void *st, fatfs_handle_t handle, enum fs_seekpos whence,
                     off_t offset)
{
    struct fatfs_handle *h = handle;
    struct fs_fileinfo info;
    errval_t err;

    switch (whence) {
    case FS_SEEK_SET:
        assert(offset >= 0);
        if (h->isdir) {
            h->dir_pos = h->dirent->parent->dir;
            for (size_t i = 0; i < offset; i++) {
                if (h->dir_pos  == NULL) {
                    break;
                }
                h->dir_pos = h->dir_pos->next;
            }
        } else {
            h->file_pos = offset;
        }
        break;

    case FS_SEEK_CUR:
        if (h->isdir) {
            assert(!"NYI");
        } else {
            assert(offset >= 0 || -offset <= h->file_pos);
            h->file_pos += offset;
        }

        break;

    case FS_SEEK_END:
        if (h->isdir) {
            assert(!"NYI");
        } else {
            err = fatfs_stat(st, handle, &info);
            if (err_is_fail(err)) {
                return err;
            }
            assert(offset >= 0 || -offset <= info.size);
            h->file_pos = info.size + offset;
        }
        break;

    default:
        USER_PANIC("invalid whence argument to ramfs seek");
    }

    return SYS_ERR_OK;
}


/**
 * @brief This takes a handle to a file that is not a directory and closes it
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 *
 * @returns SYS_ERR_OK if everything is okay
 *          FS_ERR_NOTFILE if it is not a file
*/
errval_t fatfs_close(void *st, fatfs_handle_t inhandle)
{
    struct fatfs_handle *handle = inhandle;
    if (handle->isdir) {
        return FS_ERR_NOTFILE;
    }
    handle_close(handle);
    return SYS_ERR_OK;
}

/**
 * @brief This takes a handle to a directory, gives back the name and file info and then advances the handle.
 *
 * @param st        This must be of the type fatfs_mount, no idea why it is passed as a void *
 * @param rethandle To return the created fatfs_handle_t to the opened directory
 *
 * @returns SYS_ERR_OK if everything is okay
 *          FS_ERR_NOTDIR if it is not a directory
*/
errval_t fatfs_opendir(void *st, const char *path, fatfs_handle_t *rethandle)
{
    errval_t err;

    struct fatfs_mount *mount = st;

    struct fatfs_handle *handle;
    err = resolve_path(mount->root, path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    if (!handle->isdir) {
        handle_close(handle);
        return FS_ERR_NOTDIR;
    }

    handle->dir_pos = handle->dirent->dir;

    *rethandle = handle;

    return SYS_ERR_OK;
}

/**
 * @brief This takes a handle to a directory, gives back the name and file info and then advances the handle.
 *
 * @param st        ???? No idea ... does not seem to be used by this function.
 * @param path      The handle to a directory
 *
 * @returns SYS_ERR_OK if everything is okay
 *          FS_ERR_INDEX_BOUNDS if inhandle->dir_pos == NULL
*/
errval_t fatfs_dir_read_next(void *st, fatfs_handle_t inhandle, char **retname,
                              struct fs_fileinfo *info)
{
    struct fatfs_handle *h = inhandle;

    if (!h->isdir) {
        return FS_ERR_NOTDIR;
    }

    struct fatfs_dirent *d = h->dir_pos;
    if (d == NULL) {
        return FS_ERR_INDEX_BOUNDS;
    }

    if (retname != NULL) {
        *retname = strdup(d->name);
    }


    if (info != NULL) {
        info->type = d->is_dir ? FS_DIRECTORY : FS_FILE;
        info->size = d->size;
    }

    h->dir_pos = d->next;
    return SYS_ERR_OK;
}

/**
 * @brief I think this gets rid of the handle to a directory
 *
 * @param st        ???? No idea ...
 * @param path      The path as a string
 *
 * @returns SYS_ERR_OK if the handle was closed successfully
 *          FS_ERR_NOT_DIR if the handle was not to a directory but to a normal file
*/
errval_t fatfs_closedir(void *st, fatfs_handle_t dhandle)
{
    struct fatfs_handle *handle = dhandle;
    if (!handle->isdir) {
        return FS_ERR_NOTDIR;
    }

    free(handle->path);
    free(handle);

    return SYS_ERR_OK;
}

/**
 * @brief Creates a directory, fails if it already exists
 *
 * @param st        ???? No idea ...
 * @param path      The path as a string
 *
 * @returns SYS_ERR_OK if the directory was created successfully
 *          FS_ERR_NOT_DIR if the parent directory was not a directory but a normal file
 *          FS_ERR_EXISTS if the directory already exists
*/
errval_t fatfs_mkdir(void *st, const char *path)
{
    errval_t err;

    struct fatfs_mount *mount = st;

    err = resolve_path(mount->root, path, NULL);
    if (err_is_ok(err)) {
        return FS_ERR_EXISTS;
    }


    struct fatfs_handle *parent  = NULL;
    const char *childname;

    // find parent directory
    char *lastsep = strrchr(path, FS_PATH_SEP);
    if (lastsep != NULL) {
        childname = lastsep + 1;

        size_t pathlen = lastsep - path;
        char pathbuf[pathlen + 1];
        memcpy(pathbuf, path, pathlen);
        pathbuf[pathlen] = '\0';

        // resolve parent directory
        err = resolve_path(mount->root, pathbuf, &parent);
        if (err_is_fail(err)) {
            handle_close(parent);
            return err;
        } else if (!parent->isdir) {
            handle_close(parent);
            return FS_ERR_NOTDIR; // parent is not a directory
        }
    } else {
        childname = path;
    }

    struct fatfs_dirent *dirent = dirent_create(childname, true, false);
    if (dirent == NULL) {
        //TODO: Change this
        return LIB_ERR_MALLOC_FAIL;
    }

    if (parent) {
        dirent_insert(parent->dirent, dirent, false);
        handle_close(parent);
    } else {
        dirent_insert(mount->root, dirent, false);
    }

    return SYS_ERR_OK;
}



/**
 * @brief Removes a directory
 *
 * @param st        ???? No idea ...
 * @param path      The path as a string to the "hopefully" directory
 *
 * @returns SYS_ERR_OK if the directory was removed successfully
 *          FS_ERR_NOT_DIR if it was not a directory but a normal file
 *          FS_ERR_NOTEMPTY there was something in the directory
 *          FS_ERR_BUSY someone/ something is using this directory ...
*/
errval_t fatfs_rmdir(void *st, const char *path)
{
    errval_t err;

    struct fatfs_mount *mount = st;

    struct fatfs_handle *handle;
    err = resolve_path(mount->root, path, &handle);
    if (err_is_fail(err)) {
        return err;
    }

    if (!handle->isdir) {
        err =  FS_ERR_NOTDIR;
        goto out;
    }

    if (handle->dirent->refcount != 1) {
        handle_close(handle);
        return FS_ERR_BUSY;
    }

    assert(handle->dirent->is_dir);

    if (handle->dirent->dir) {
        err = FS_ERR_NOTEMPTY;
        goto out;
    }

    dirent_remove_and_free(handle->dirent);

    out:
    free(handle);

    return err;
}


/**
 * @brief Mounts the FAT filesystem on the SD card
 *
 * @param uri       ???? Not sure I need this
 * @param retst     A sort of handle
 *
 * @returns SYS_ERR_OK if everything is fine
*/
//TODO: Handle filesystem could not be found/ opened error ... return BAD_FS
errval_t fatfs_mount(const char *uri, struct boot_sector * bs, fatfs_mount_t *retst)
{
    
    errval_t err;
    /* Setup channel and connect at service */
    /* TODO: setup channel to init for multiboot files */
    struct fatfs_mount *mount = calloc(1, sizeof(struct fatfs_mount));
    if (mount == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }
    
    struct fatfs_dirent *fatfs_root;
    fatfs_root = calloc(1, sizeof(*fatfs_root));
    if (fatfs_root == NULL) {
        free(fatfs_mount);
        return LIB_ERR_MALLOC_FAIL;
    }
    
    fatfs_root->size = 0;
    fatfs_root->is_dir = true;
    fatfs_root->name = "/";
    fatfs_root->parent = NULL;

    mount->root = fatfs_root;
    
    printf("TRIG1\n");
    mount->boot_sector = bs;
    struct BPB * BPB = mount->boot_sector->BPB;
    
    //make sure all structures and types are of the right size
    assert(sizeof(struct fatfs_dirent_internal) == 32);
    assert(sizeof(struct fatfs_info_sector) == 512);
    assert(sizeof(fatfs_fat_entry_t) == 4);
    
    //get FSInfo
    struct fatfs_info_sector * fs_info = malloc(512);
    if (fs_info == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }
    err = mmchs_read_block(BPB->BPB_FSInfo, fs_info);
    if (err_is_fail(err)) {
        DEBUG_ERR(err, "Failure when trying to read FS info sector\n");
        return err;
    }
    //check all sig fields
    assert(fs_info->FSI_LeadSig[0] == 0x52 && fs_info->FSI_LeadSig[1] == 0x52 
            && fs_info->FSI_LeadSig[2] == 0x61 && fs_info->FSI_LeadSig[3] == 0x41);
    assert(fs_info->FSI_StrucSig[0] == 0x72 && fs_info->FSI_StrucSig[1] == 0x72 
            && fs_info->FSI_StrucSig[2] == 0x41 && fs_info->FSI_StrucSig[3] == 0x61);
    assert(fs_info->FSI_TrailSig[0] == 0x00 && fs_info->FSI_TrailSig[1] == 0x00 
            && fs_info->FSI_TrailSig[2] == 0x55 && fs_info->FSI_TrailSig[3] == 0xAA);
    mount->info_sector = fs_info;
    printf("TRIG2\n");

    //set useful stuff
    uint32_t rootDirSectors = ((BPB->BPB_RootEntCnt * 32) + (BPB->BPB_BytsPerSec - 1)) / BPB->BPB_BytsPerSec; //should be 0 for FAT32
    //compute start of data region, first sector of cluster 2
    if (BPB->BPB_FATSz16 != 0) {
        debug_printf("Warning: BPB->BPB_FATSz16 != 0\n");
        mount->FATSz = BPB->BPB_FATSz16;
    } else {
        mount->FATSz = BPB->BPB_FATSz32;
    }
    printf("TRIG3\n");

    if (BPB->BPB_TotSec16 != 0) {
        debug_printf("Warning: BPB->BPB_TotSec16 != 0\n");
        mount->totSec = BPB->BPB_TotSec16;
    } else {
        mount->totSec = BPB->BPB_TotSec32;
    }
    mount->num_data_sec = mount->totSec - (BPB->BPB_RsvdSecCnt + (BPB->BPB_NumFATs * mount->FATSz) + rootDirSectors);
    mount->first_data_sec = BPB->BPB_RsvdSecCnt + (BPB->BPB_NumFATs * mount->FATSz) + rootDirSectors;
    mount->count_of_clusters = mount->num_data_sec / BPB->BPB_SecPerClus;
    printf("FATSz=%d rootDirSectors=%d\n", mount->FATSz, rootDirSectors);
    printf("firstDataSector=%d\n", mount->first_data_sec);


    //load the two FATs into memory and cache them
    uint32_t sector_number_start_FAT = BPB->BPB_RsvdSecCnt;
    uint32_t number_of_sectors_for_FAT = BPB->BPB_NumFATs * mount->FATSz;
    printf("fatfs_mount: Before trying to allocate a huge chunk of memory ...\n");
    char * temp_ptr = malloc(512*number_of_sectors_for_FAT); //This should currently be about 7.375MB
    if (temp_ptr == NULL) {
        return LIB_ERR_MALLOC_FAIL;
    }
    printf("fatfs_mount: After trying to allocate a huge chunk of memory ...\n");
    printf("fatfs_mount: Before reading in about 7MB from the SD card (takes long) ...\n");
    memset(temp_ptr, 0, 512*number_of_sectors_for_FAT);
    char * start_temp_ptr = temp_ptr;
    union char_or_uint32_t converter;
    converter.chars[0] = fs_info->FSI_Nxt_Free[0];
    converter.chars[1] = fs_info->FSI_Nxt_Free[1];
    converter.chars[2] = fs_info->FSI_Nxt_Free[2];
    converter.chars[3] = fs_info->FSI_Nxt_Free[3];
    mount->next_free_cluster = converter.integer;
    
    for (uint32_t i = 0; i < number_of_sectors_for_FAT; i++, temp_ptr+=512) {
        if (i < mount->next_free_cluster || mount->next_free_cluster == 0xFFFFFFFF) { //TODO: Set in this case
            err = mmchs_read_block(sector_number_start_FAT, temp_ptr);
            if (err_is_fail(err)) {
                DEBUG_ERR(err, "Failure when trying to read %i-th FAT sector (sector %d)\n", i, sector_number_start_FAT);
            return err;
            }
        }
    }
    printf("fatfs_mount: After reading in about 7MB from the SD card ...\n");
    
    //set FAT pointers
    mount->start_FAT = (fatfs_fat_entry_t *) start_temp_ptr;
    mount->end_FAT = (fatfs_fat_entry_t *) temp_ptr;


    //initialize the directory structure
    err = load_dirs_from_FAT(mount);
    if (err_is_fail(err)) {
        return err;
    }

    *retst = mount;

    return SYS_ERR_OK;
}
