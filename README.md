Everything under netutil implements an IP/UDP stack on a fork of the barrelfish Operating
system. Packets are routed cross-cores via rpcs to the appropriate processes are
found under aos/aos_rpc_network.c. aos/aos_rpc* files are helpful in understanding how
messages are routed from core to core, and split up into individual UMP
messages.