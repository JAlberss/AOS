#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/inthandler.h>
#include <aos/kernel_cap_invocations.h>

#include <arch/arm/omap44xx/device_registers.h>
#include <dev/omap/omap44xx_uart3_dev.h>
#include <maps/omap44xx_map.h>

#include <sys/types.h>
//#include <netutil/htons.h>
#include <netutil/checksum.h>
#include <netutil/user_serial.h>
#include <netinet/in.h>
#include <netinet/in_systm.h>
#include <netinet/ip.h>
#include <netinet/udp.h>

#define MAX_RECEIVE_SIZE 256 // bad

static omap44xx_uart3_t port;

static struct quick_socket *sock_buf;

static void serial_poll(omap44xx_uart3_t *uart)
{
  // Read while we can
  while (omap44xx_uart3_lsr_rx_fifo_e_rdf(uart))
  {
    uint8_t c = omap44xx_uart3_rhr_rhr_rdf(uart);
    serial_input(&c, 1);
  }
}

static void serial_interrupt(void *arg)
{
  // get type
  omap44xx_uart3_iir_t iir = omap44xx_uart3_iir_rd(&port);

  if (omap44xx_uart3_iir_it_pending_extract(iir) == 0)
  {
    omap44xx_uart3_it_type_status_t it_type =
        omap44xx_uart3_iir_it_type_extract(iir);
    switch (it_type)
    {
    case omap44xx_uart3_it_modem:
      omap44xx_uart3_msr_rd(&port);
      break;
    case omap44xx_uart3_it_rxtimeout:
    case omap44xx_uart3_it_rhr:
      serial_poll(&port);
      break;
    default:
      debug_printf("serial_interrupt: unhandled irq: %d\n", it_type);
      break;
    }
  }
}

static bool convert_rx_simple(uint8_t *trig)
{
  switch (*trig)
  {
  case 8:
    *trig = 0;
    return true;
  case 16:
    *trig = 1;
    return true;
  case 56:
    *trig = 2;
    return true;
  case 60:
    *trig = 3;
    return true;
  default:
    return false;
  }
}

static bool convert_tx_simple(uint8_t *trig)
{
  switch (*trig)
  {
  case 8:
    *trig = 0;
    return true;
  case 16:
    *trig = 1;
    return true;
  case 32:
    *trig = 2;
    return true;
  case 56:
    *trig = 3;
    return true;
  default:
    return false;
  }
}

/*
 * Initialzie OMAP UART with interrupt
 * UART TRM 23.3
 */
static void omap44xx_uart3_init(omap44xx_uart3_t *uart, lvaddr_t base)
{
  // XXX: test this with other values
  // rx and tx FIFO threshold values (1 -- 63)
  uint8_t rx_trig = 1;  // amount of characters in fifo
  uint8_t tx_trig = 63; // amount of free spaces in fifo
  // LH: Why not keep these always at 0??
  bool need_rx_1b = convert_rx_simple(&rx_trig);
  bool need_tx_1b = convert_tx_simple(&tx_trig);

  omap44xx_uart3_initialize(uart, (mackerel_addr_t)base);
  // do soft reset -- not the best idea if we rely on the same UART for
  // debug output
  //omap44xx_uart3_sysc_softreset_wrf(uart, 0x1);
  //while (!omap44xx_uart3_syss_resetdone_rdf(uart)); // poll for reset completion

  // configure FIFOs according to TRM (section 25.3.5.1.1.2)
  //1 switch to config mode B (access to efr register): set lcr to 0xbf
  omap44xx_uart3_lcr_t old_lcr = omap44xx_uart3_lcr_rd(uart);
  omap44xx_uart3_lcr_wr(uart, 0xbf);
  // 1.1 disable baud clock so we can write to FCR[0] and FCR[3]
  omap44xx_uart3_dll_clock_lsb_wrf(uart, 0x0);
  omap44xx_uart3_dlh_clock_msb_wrf(uart, 0x0);
  //2 enable register submode tlr to access tlr register
  omap44xx_uart3_enhanced_en_status_t old_enhanced_en =
      omap44xx_uart3_efr_enhanced_en_rdf(uart);
  omap44xx_uart3_efr_enhanced_en_wrf(uart, 1);
  //3 switch to config mode A (access to mcr register): set lcr to: 0x80
  omap44xx_uart3_lcr_wr(uart, 0x80);
  //4 enable register submode tlr to access tlr register
  omap44xx_uart3_tcr_tlr_status_t old_tcr_tlr =
      omap44xx_uart3_mcr_tcr_tlr_rdf(uart);
  omap44xx_uart3_mcr_tcr_tlr_wrf(uart, 1);
  //5 enable FIFO, load FIFO triggers, load DMA mode (part1)
  omap44xx_uart3_fcr_t fcr = omap44xx_uart3_fcr_default;
  // set trigger lvls of rx and tx FIFOs (defined above)
  fcr = omap44xx_uart3_fcr_rx_fifo_trig_insert(fcr, rx_trig & 0x3);
  fcr = omap44xx_uart3_fcr_tx_fifo_trig_insert(fcr, tx_trig & 0x3);
  fcr = omap44xx_uart3_fcr_dma_mode_insert(fcr, 0); // no DMA
  fcr = omap44xx_uart3_fcr_fifo_en_insert(fcr, 1);  // enable FIFOs
  omap44xx_uart3_fcr_wr(uart, fcr);

  //6 switch to config mode B
  omap44xx_uart3_lcr_wr(uart, 0xbf);
  //7 load FIFO triggers (Apply to DMA and interrupts, part2)
  omap44xx_uart3_tlr_t tlr = omap44xx_uart3_tlr_default;
  omap44xx_uart3_tlr_rx_fifo_trig_dma_insert(tlr, rx_trig >> 2);
  omap44xx_uart3_tlr_tx_fifo_trig_dma_insert(tlr, tx_trig >> 2);
  omap44xx_uart3_tlr_wr(uart, tlr);
  //7.1 Enable and set triggers for flow control
  //omap44xx_uart3_tcr_rx_fifo_trig_halt_wrf(uart, 1); // This gets multiplied by 4
  //omap44xx_uart3_tcr_rx_fifo_trig_start_wrf(uart, 0);
  //omap44xx_uart3_efr_auto_cts_en_wrf(uart, 0);
  //omap44xx_uart3_efr_auto_rts_en_wrf(uart, 0);

  //8 load new FIFO triggers & new DMA mode (part3)
  omap44xx_uart3_scr_t scr = omap44xx_uart3_scr_default;
  // make FIFO trigger levels byte granularity
  // --> lvl = { *_fifo_trig_dma : *_fifo_trig }
  scr = omap44xx_uart3_scr_rx_trig_granu1_insert(scr, need_rx_1b);
  scr = omap44xx_uart3_scr_tx_trig_granu1_insert(scr, need_tx_1b);
  scr = omap44xx_uart3_scr_dma_mode_2_insert(scr, 0);   // no DMA
  scr = omap44xx_uart3_scr_dma_mode_ctl_insert(scr, 0); // shouldn't matter w/ DMA off
  omap44xx_uart3_scr_wr(uart, scr);
  //8b clear fifo queues
  omap44xx_uart3_fcr_rx_fifo_clear_wrf(uart, 1);
  omap44xx_uart3_fcr_tx_fifo_clear_wrf(uart, 1);
  //9 restore enhanced_en
  omap44xx_uart3_efr_enhanced_en_wrf(uart, old_enhanced_en);

  //10 switch to config mode A
  omap44xx_uart3_lcr_wr(uart, 0x80);
  //11 restore TCR_TLR
  omap44xx_uart3_mcr_tcr_tlr_wrf(uart, old_tcr_tlr);
  //12 restore LCR
  omap44xx_uart3_lcr_wr(uart, old_lcr);

  // configure protocol, baud rate and irq according to trm (section
  // 23.3.5.1.1.3)
  //1 disable UART access to DLL and DLH regs
  omap44xx_uart3_mdr1_mode_select_wrf(uart, 0x7);
  //2 switch to register config mode B
  omap44xx_uart3_lcr_wr(uart, 0xbf);
  //3 enable access to IER bit field
  old_enhanced_en = omap44xx_uart3_efr_enhanced_en_rdf(uart);
  omap44xx_uart3_efr_enhanced_en_wrf(uart, 1);
  //4 switch to reg operational mode to access IER register
  omap44xx_uart3_lcr_wr(uart, 0);
  //5 clear IER register
  omap44xx_uart3_ier_wr(uart, 0);
  //6 config mode B
  omap44xx_uart3_lcr_wr(uart, 0xbf);
  //7 new divisor value --> 115200 baud == 0x00, 0x1A (dlh, dll)
  omap44xx_uart3_dll_clock_lsb_wrf(uart, 0x1a);
  omap44xx_uart3_dlh_clock_msb_wrf(uart, 0x0);
  //8 register operational mode
  omap44xx_uart3_lcr_wr(uart, 0);
  //9 load irq config --> only rhr irq for now
  omap44xx_uart3_ier_rhr_it_wrf(uart, 1);
  //10 register config mode B
  omap44xx_uart3_lcr_wr(uart, 0xbf);
  //11 restore efr.enhanced_en
  omap44xx_uart3_efr_enhanced_en_wrf(uart, old_enhanced_en);
  //12 load protocol formatting --> 8N1
  omap44xx_uart3_lcr_t lcr = omap44xx_uart3_lcr_default;
  lcr = omap44xx_uart3_lcr_parity_en_insert(lcr, 0);                    // No parity
  lcr = omap44xx_uart3_lcr_nb_stop_insert(lcr, 0);                      // 1 stop bit
  lcr = omap44xx_uart3_lcr_char_length_insert(lcr, omap44xx_uart3_cl8); // 8 data bits
  omap44xx_uart3_lcr_wr(uart, lcr);
  //13 load UART mode
  omap44xx_uart3_mdr1_mode_select_wrf(uart, 0x0);
  // DONE
}

void socket_clear(struct quick_socket *sock)
{
  sock->tx = 0;
  sock->len = 0;
  sock->slip_len = 0;
  sock->response_slip_len = 0;
  memset(sock->buf, 0, sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  memset(sock->ip, 0, sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  memset(sock->response, 0, sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  memset(sock->response_slip, 0, sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  //debug_printf("Sock clear\n");
}

// Make dynamic
errval_t socket_init(struct quick_socket *sock)
{
  sock->name = "test";
  sock->buf = malloc(sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  sock->ip = malloc(sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  sock->response = malloc(sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  sock->response_slip = malloc(sizeof(uint8_t) * MAX_RECEIVE_SIZE);
  socket_clear(sock);
  return SYS_ERR_OK;
}

errval_t serial_init(lvaddr_t vbase, uint32_t irq)
{
  errval_t err;
  assert(vbase);
  debug_printf("%s\n", __FUNCTION__);
  if (sock_buf == NULL)
  {
    sock_buf = malloc(sizeof(struct quick_socket));
  }
  socket_init(sock_buf);
  // paging_map_device returns an address pointing to the beginning of
  // a section, need to add the offset for within the section again
  debug_printf("omap serial_init base = 0x%" PRIxLVADDR "\n", vbase);
  omap44xx_uart3_init(&port, vbase);
  debug_printf("omap serial_init[%d]: done.\n", port);

  err = inthandler_setup_arm(serial_interrupt, NULL, irq);
  if (err_is_fail(err))
  {
    USER_PANIC_ERR(err, "interrupt setup failed.");
  }

  return SYS_ERR_OK;
}

/** output a single character */
static void serial_putchar(uint8_t c)
{
  // Wait until FIFO can hold more characters
  int maxit = 100000;
  while (!omap44xx_uart3_lsr_tx_fifo_e_rdf(&port) && maxit-- > 0)
    ;
  if (maxit == 0)
  {
    printf("TX does not get ready.\n");
  }
  else
  {
    // Write character
    omap44xx_uart3_thr_thr_wrf(&port, c);
  }
}

/* Give the information we have received in response, how to respond */
void form_response(uint8_t *recv, uint8_t *resp, size_t *resp_len)
{
  u_char type = recv[9];
  u_int16_t ip_header_len = (recv[0] & 0x0F) * 4;     // Gives the number of 32 bit fields, => 4 bytes
  u_int16_t packet_len_1 = ((u_int16_t)recv[2]) << 8; // First byte of packet length field
  u_int16_t packet_len = packet_len_1 | recv[3];
  u_int32_t recv_src_bytes; //
  u_int32_t recv_dst_bytes;
  recv_src_bytes = (*(u_int32_t *)(&recv[12]));
  struct in_addr recv_src = {
      .s_addr = recv_src_bytes,
  };
  recv_dst_bytes = (*(u_int32_t *)(&recv[16]));
  struct in_addr recv_dst = {
      .s_addr = recv_dst_bytes,
  };
  switch (type)
  {
  case IPPROTO_ICMP:
    if (recv[20] != 8)
    {
      return;
    }
    u_int16_t icmp_len = packet_len - (u_int16_t)ip_header_len;
    u_int16_t icmp_payload_len = icmp_len - ICMP_HEADER_LEN;
    /* This is all IP stuff, should be moved above the switch */
    memcpy(resp, recv, ip_header_len);
    *(u_int32_t *)(&resp[12]) = recv_dst.s_addr;
    *(u_int32_t *)(&resp[16]) = recv_src.s_addr;
    /* End of IP stuff */
    memcpy(resp + ip_header_len, recv + ip_header_len, ICMP_HEADER_LEN);
    memset(recv + 21, 0, 1);

    resp[20] = 0; // setting the type of the echo reply // For testing the checksum
    resp[22] = 0; // setting the checksum field to be zero
    resp[23] = 0; // setting the checksum field to be zero, Must be zero to calculate the checksum field

    memcpy(resp + 28, recv + 28, icmp_payload_len);
    u_int16_t checksum = (inet_checksum((void *)(&resp[20]), icmp_len)); //TODO replace with generic
    resp[22] = (checksum & 0x00FF);
    resp[23] = (checksum >> 8);
    buf_print(resp, packet_len);
    *resp_len = packet_len;
    break;
  case IPPROTO_IP:
    break;
  case IPPROTO_TCP:
    break;
  case IPPROTO_UDP:
  {
    //TODO Design decision, how do we change socket struct on different types of protocols
    // TODO check for correct port here
    errval_t err;
    struct udphdr *recv_header = malloc(sizeof(struct udphdr));
    uint8_t *udp_header = recv + ip_header_len; //Start of the UDP Header
    deserialize_udp_header(udp_header, recv_header);
    buf_print(recv, IP_HEADER_LEN + recv_header->uh_ulen);
    err = validate_udp_header(recv_header);
    if (err_is_fail(err))
    {
      *resp_len = 0;
      debug_printf("invalid udp received. Ignore\n");
      return;
    }
    size_t payload_recv_len = recv_header->uh_ulen - UDP_HEADER_LEN; // todo check for too large payload
    uint8_t *payload_recv = malloc(sizeof(uint8_t) * payload_recv_len);
    memset(payload_recv, 0, sizeof(uint8_t) * payload_recv_len);
    get_udp_payload(udp_header + UDP_HEADER_LEN, payload_recv_len, payload_recv);
    // TODO payload will vary greatly depending on packets, should be allocted and freed instead of memset

    /* Insert magical RPC call here with the udp payload, maybe include the source ip, and ports */
    struct aos_rpc *rpc = get_init_rpc();
    // recv_header->uh_dport);
    buf_print(payload_recv, payload_recv_len);

    err = aos_rpc_network_in_packet(rpc, payload_recv_len, recv_src, recv_dst, recv_header->uh_sport, recv_header->uh_dport, payload_recv);

    if (err_is_fail(err))
    {
      DEBUG_ERR(err, "failed to send packet\n");
      return;
    }
    free(recv_header); // WE really don't need the recv header at this point
    free(payload_recv);
    break;
  }
  default:
    *resp_len = 0;
  }
  return;
}

void serial_input(uint8_t *buf, size_t len)
{
  assert(len == 1);
  for (int i = 0; i < len; i++)
  {
    if (buf[0] == END)
    {
      sock_buf->buf[sock_buf->tx] = buf[0];
      sock_buf->tx++;
      /* At this point, transfer control over to network */
      errval_t err;

      err = unslip(sock_buf->buf, sock_buf->ip);
      form_response(sock_buf->ip, sock_buf->response, &(sock_buf->len));
      enslip(sock_buf->response, sock_buf->response_slip, sock_buf->len, &(sock_buf->response_slip_len));
      //      debug_printf("slipped resp\n");

      if (sock_buf->response[9] == IPPROTO_ICMP)
      {
        serial_write(sock_buf->response_slip, sock_buf->response_slip_len); //slip_len represents the length of the received packet
                                                                            //debug_printf("done writing\n");
      }
      socket_clear(sock_buf);
      return;
    }
    sock_buf->buf[sock_buf->tx] = buf[0];
    sock_buf->tx++;
  }
  return;
}

errval_t buf_comp(uint8_t *buf1, uint8_t *buf2, size_t len)
{
  for (int i = 0; i < len; i++)
  {
    if (buf1[i] != buf2[i])
    {
      debug_printf("%hhx != %hhx at %d\n", buf1[i], buf2[i], i);
      return !SYS_ERR_OK;
    }
  }
  return SYS_ERR_OK;
}

/* Takes a buffer representing a packet, and writes it into the slip protocol */
void enslip(uint8_t *packet, uint8_t *slip_packet, size_t len, size_t *slip_len)
{
  int tx = 0;
  //debug_printf("preslip length %d\n", len);
  for (int i = 0; i < len; i++)
  {
    switch (packet[i])
    {
    case 0:
      slip_packet[tx] = ESC;
      tx++;
      slip_packet[tx] = ESC_NUL;
      break;
    case END:
      slip_packet[tx] = ESC;
      tx++;
      slip_packet[tx] = ESC_END;
      break;
    case ESC:
      slip_packet[tx] = ESC;
      tx++;
      slip_packet[tx] = ESC_ESC;
      break;
    default:
      slip_packet[tx] = packet[i];
    }
    tx++;
  }
  slip_packet[tx] = END;
  *slip_len = tx + 1; //size is one more than the index
  //  debug_printf("postslip len: %d\n", *slip_len);
}

struct ip *deserialize_packet(uint8_t *packet, struct ip *recv_packet)
{
  // we have received an array of bytes representing a packet
  // build the packet up
  assert(recv_packet != NULL);
  recv_packet->ip_hl = packet[0];
  recv_packet->ip_v = packet[0];
  recv_packet->ip_tos = packet[1];
  recv_packet->ip_len = packet[2];
  recv_packet->ip_id = packet[3];
  recv_packet->ip_off = packet[4];
  recv_packet->ip_ttl = packet[5];
  recv_packet->ip_p = packet[6];
  recv_packet->ip_sum = packet[7];
  recv_packet->ip_src = (struct in_addr){
      .s_addr = packet[8],
  };
  recv_packet->ip_dst = (struct in_addr){
      .s_addr = packet[12],
  };
  return recv_packet;
}

void print_packet(struct ip *recv_packet)
{
  return;
}

void buf_print(uint8_t *buf, size_t len)
{
  return;
  for (int i = 0; i < len; i += 8)
  {
    debug_printf("%02x %02x %02x %02x %02x %02x %02x %02x 00%d\n", buf[i], buf[i + 1], buf[i + 2], buf[i + 3], buf[i + 4], buf[i + 5], buf[i + 6], buf[i + 7], i);
  }
  debug_printf("\n");
}

void quick_socket_print(void)
{
  buf_print(sock_buf->buf, 128);
}

void serial_write(uint8_t *c, size_t len)
{
  for (int i = 0; i < len; i++)
  {
    serial_putchar(c[i]);
  }
}

/* Function will remove the SLIP protocol from the received packets */
errval_t unslip(uint8_t *buf, uint8_t *dest)
{
  // uint8_t *ip = malloc(sizeof(uint8_t)*64); // ip will be the buffer representing the unslipped packet
  uint8_t tx = 0; // determines the next place to write in the ip buffer;
  for (int i = 0; i < MAX_RECEIVE_SIZE; i++)
  {
    switch (buf[i])
    {
    case END:
      sock_buf->slip_len = ++i; // slip len won't include the last END otherwise
      return SYS_ERR_OK;
    case ESC:
      sock_buf->len++;
      if (buf[i + 1] == ESC_END)
      {
        dest[tx] = END;
        i++;
        tx++;
        break;
      }
      else if (buf[i + 1] == ESC_ESC)
      {
        dest[tx] = ESC;
        i++;
        tx++;
        break;
      }
      else if (buf[i + 1] == ESC_NUL)
      {
        dest[tx] = 0;
        i++;
        tx++;
        break;
      }
      else
      {
      }
    default:
      dest[tx] = buf[i];
      tx++;
      sock_buf->len++;
    }
  }
  debug_printf("Prior to %d return\n", __LINE__);
  return SYS_ERR_OK;
}
