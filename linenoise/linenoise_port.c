
#include <aos/aos.h>
#include "linenoise_port.h"
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <termios.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int	tcgetattr(int a, struct termios *term){
    return 0;
}

int	tcsetattr(int a, int b, const struct termios *termios) {
    return 0;
}
ssize_t
read_a(int fd, void *buf, size_t nbytes)
{
    if(!nbytes) {
        return 0;
    }
    size_t ret = 0;
    while (ret != nbytes)
    {
       char c = getchar();
        switch (c)
        {
        case 0:
            continue;
        default:
            ((char *)buf)[ret] = c;
            ret++;
            break;
        }
    }
    return ret;
}
ssize_t
write_a(int fd, const char *buf, size_t nbytes)
{
    for (uint32_t i = 0; i < nbytes; i++)
    {
        if (buf[i] != 0)
        {
            putchar(buf[i]);
        }
        else
        {
            break;
        }
    }
    fflush(STDIN_FILENO);

    return nbytes;
}
