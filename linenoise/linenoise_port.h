#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <linenoise/linenoise.h>
#define PROMPT_OFFSET 11

/**
 * \brief Wrapper function which poorly emulate a posix write function
 *
 * Since linenoiose uses posix read and write calls we have to wrap them into libc file stuff
 */
ssize_t write_a(int fd, const char *buf, size_t nbytes);

/**
 * \brief Wrapper function which poorly emulate a posix read function
 *
 * Since linenoiose uses posix read and write calls we have to wrap them into libc file stuff
 */
ssize_t read_a(int fd, void *buf, size_t nbytes);
