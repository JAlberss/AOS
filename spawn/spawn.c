#include <aos/aos.h>
#include <spawn/spawn.h>

#include <elf/elf.h>
#include <aos/dispatcher_arch.h>
#include <barrelfish_kpi/paging_arm_v7.h>
#include <barrelfish_kpi/domain_params.h>
#include <spawn/multiboot.h>

extern struct bootinfo *bi;
extern struct spawninfo *si_head;
extern struct aos_process *pt_head;

static errval_t elf_alloc(void *state, genvaddr_t base, size_t size, uint32_t flags, void **ret)
{
    // We wish to find the difference between the aligned and unaligned pages
    genvaddr_t diff = BASE_PAGE_OFFSET(base);
    size += diff;
    struct capref frame_dest;
    size_t realsize = 0;
    errval_t err = 0;
    struct spawninfo *si = state;
    err = frame_alloc(&frame_dest, size, &realsize);
    if (err_is_fail(err))
    {
        return err;
    }
    err = paging_map_fixed_attr(&si->child_pdir, base, frame_dest, realsize, flags);
    if (err_is_fail(err))
    {
        return err;
    }
    struct capref frame_copy;
    err = slot_alloc(&frame_copy);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_SLOT_ALLOC);
    }
    err = cap_copy(frame_copy, frame_dest);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_CAP_COPY);
    }

    err = paging_map_frame_attr(si->local_pdir, ret, realsize, frame_copy, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }
    *ret = *ret + diff;
    return SYS_ERR_OK;
}

void endpoint_cap_recv_handler(void *arg);
void endpoint_cap_recv_handler(void *arg)
{
    struct spawninfo *si = arg;
    si->channel.remote_cap = si->channel.endpoint->recv_slot;
    si->channel.connstate = LMP_CONNECTED;
    lmp_chan_send0(&si->channel, LMP_SEND_FLAGS_DEFAULT, NULL_CAP);
}

errval_t spawn_setup_cspace(struct spawninfo *si)
{
    errval_t err;
    struct capref t1;
    err = cnode_create_l1(&si->rootcn_cap, &si->rootcn);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_ROOTCN);
    }

    // Create task cnode
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_TASKCN, &si->taskcn);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }
    // Create empty l2 cnodes used later to seed the slot allocator
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_SLOT_ALLOC0, NULL);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_SLOT_ALLOC1, NULL);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_SLOT_ALLOC2, NULL);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_SLOTALLOC_CNODE);
    }
    // Create a capability for the dispatcher control block
    err = slot_alloc(&si->dcb);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_DISPATCHER);
    }
    err = dispatcher_create(si->dcb);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_DISPATCHER);
    }

    t1.cnode = si->taskcn;
    t1.slot = TASKCN_SLOT_DISPATCHER;
    err = cap_copy(t1, si->dcb);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_CAP_COPY);
    }

    // Give domain endpoint to itself (in taskcn)
    struct capref selfep = {
        .cnode = si->taskcn,
        .slot = TASKCN_SLOT_SELFEP,
    };
    err = cap_retype(selfep, si->dcb, 0, ObjType_EndPoint, 0, 1);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_SELFEP);
    }

    err = slot_alloc(&(si->child_ep_cap)); // In parent's cspace
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "slot alloc fails\n");
    }
    err = cap_copy(si->child_ep_cap, selfep); // Copy the endpoint of the child into the parent's cspace;
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Cap copy fails\n");
    }

    t1.cnode = si->taskcn;
    t1.slot = TASKCN_SLOT_ROOTCN;
    err = cap_copy(t1, si->rootcn_cap);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_MINT_ROOTCN);
    }

    memset(&si->argspg, 0, sizeof(si->argspg));

    struct cnoderef basecn;
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_BASE_PAGE_CN, &basecn);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_CNODE_CREATE);
    }

    //DIFFS
    struct capref ram;
    err = ram_alloc(&ram, L2_CNODE_SLOTS * BASE_PAGE_SIZE);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_RAM_ALLOC);
    }

    struct capref base = {
        .cnode = basecn,
        .slot = 0,
    };
    err = cap_retype(base, ram, 0, ObjType_RAM, BASE_PAGE_SIZE, L2_CNODE_SLOTS);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_CAP_RETYPE);
    }

    // delete big RAM cap
    err = cap_destroy(ram);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_CAP_DESTROY);
    }

    // Begin of endpoint stuff

    struct capref initep_cap;
    initep_cap.cnode = si->taskcn;
    initep_cap.slot = TASKCN_SLOT_INITEP;

    struct waitset *default_ws = get_default_waitset();
    struct lmp_chan *channel = &si->channel;
    lmp_chan_init(channel);
    /* create local endpoint */
    struct capref endpoint_cap;
    struct lmp_endpoint *local_endpoint;
    //TODO: move to init
    err = endpoint_create(LMP_RECV_LENGTH, &endpoint_cap, &local_endpoint);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in lmp_endpoint_create_in_slot");
        return err_push(err, LIB_ERR_ENDPOINT_CREATE);
    }

    struct capref child_ep_cap;
    err = slot_alloc(&child_ep_cap);
    if (err_is_fail(err))
    {
        return err;
    }
    lmp_endpoint_set_recv_slot(local_endpoint, child_ep_cap);

    channel->endpoint = local_endpoint;
    channel->local_cap = endpoint_cap;

    err = lmp_endpoint_register(local_endpoint, default_ws,
                                MKCLOSURE((void *)endpoint_cap_recv_handler, si));

    err = cap_copy(initep_cap, endpoint_cap);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "<<endpoint copy into child's cspace failed");
        err_push(err, LIB_ERR_CAP_COPY);
    }

    return SYS_ERR_OK;
}

errval_t spawn_setup_vspace(struct spawninfo *si)
{
    // Create pagecn
    errval_t err;
    err = cnode_create_foreign_l2(si->rootcn_cap, ROOTCN_SLOT_PAGECN, &si->pagecn);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_PAGECN);
    }
    si->pagecn_cap.cnode = si->rootcn;
    si->pagecn_cap.slot = ROOTCN_SLOT_PAGECN;

    size_t bufsize = SINGLE_SLOT_ALLOC_BUFLEN(L2_CNODE_SLOTS);
    void *buf = malloc(bufsize);
    assert(buf != NULL);
    if (buf == NULL)
    {
        return LIB_ERR_MALLOC_FAIL;
    }
    err = single_slot_alloc_init_raw(&si->pagecn_slot_alloc, si->pagecn_cap, si->pagecn, L2_CNODE_SLOTS, buf, bufsize);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_SINGLE_SLOT_ALLOC_INIT_RAW);
    }
    err = si->pagecn_slot_alloc.a.alloc(&si->pagecn_slot_alloc.a, &si->vtree);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_SLOT_ALLOC);
    }
    assert(si->vtree.slot == 0);
    err = vnode_create(si->vtree, ObjType_VNode_ARM_l1);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_VNODE);
    }

    return SYS_ERR_OK;
}

errval_t spawn_setup_dispatcher(struct aos_process *pt, coreid_t core_id, const char *name, genvaddr_t entry, uintptr_t got_base)
{
    errval_t err;

    struct spawninfo *si = pt->si;
    si->dispframe.cnode = si->taskcn;
    si->dispframe.slot = TASKCN_SLOT_DISPFRAME;
    size_t retsize;
    err = frame_create(si->dispframe, (1 << DISPATCHER_FRAME_BITS), &retsize);
    if (err_is_fail(err))
    {
        err_push(err, LIB_ERR_SLOT_ALLOC);
    }
    dispatcher_handle_t handle;
    err = paging_map_frame_attr(si->local_pdir, (void **)&handle, retsize, si->dispframe, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }

    genvaddr_t spawn_dispatcher_base;
    err = paging_map_frame_attr(&si->child_pdir, (void **)&spawn_dispatcher_base, retsize, si->dispframe, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }

    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(handle);
    struct dispatcher_generic *disp_gen = get_dispatcher_generic(handle);
    struct dispatcher_shared_arm *disp_arm = get_dispatcher_shared_arm(handle);
    arch_registers_state_t *enabled_area = dispatcher_get_enabled_save_area(handle);
    arch_registers_state_t *disabled_area = dispatcher_get_disabled_save_area(handle);

    disp_gen->core_id = core_id;              // core id of the process
    disp_gen->domain_id = pt->pid;                // Pid of target process
    disp->udisp = spawn_dispatcher_base;      // Virtual address of the dispatcher frame in childs
    disp->disabled = 1;                       // Start in disabled mode
    disp->fpu_trap = 1;                       // Trap on fpu instructions
    strncpy(disp->name, name, DISP_NAME_LEN); // A name (for debugging)

    disabled_area->named.pc = ((uint32_t)entry);
    // Set program counter (where it should start to execute)
    // Initialize offset registers
    disp_arm->got_base = got_base;                            // Address of .got in childs VSpace.
    enabled_area->regs[REG_OFFSET(PIC_REGISTER)] = got_base;  // same as above
    disabled_area->regs[REG_OFFSET(PIC_REGISTER)] = got_base; // same as above
    enabled_area->named.cpsr = CPSR_F_MASK | ARM_MODE_USR;
    disabled_area->named.cpsr = CPSR_F_MASK | ARM_MODE_USR;
    disp_gen->eh_frame = 0;
    disp_gen->eh_frame_size = 0;
    disp_gen->eh_frame_hdr = 0;
    disp_gen->eh_frame_hdr_size = 0;

    si->handle = handle;
    return SYS_ERR_OK;
}

errval_t spawn_setup_env(struct spawninfo *si, char *const argv[], char *const envp[])
{
    errval_t err;
    si->argspg.cnode = si->taskcn;
    si->argspg.slot = TASKCN_SLOT_ARGSPAGE;
    size_t actual_args_size;
    err = frame_create(si->argspg, ARGS_SIZE, &actual_args_size);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_ARGSPG);
    }

    void *spawn_args_base;
    err = paging_map_frame_attr(&si->child_pdir, &spawn_args_base, actual_args_size, si->argspg, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }
    void *argspg;
    err = paging_map_frame_attr(si->local_pdir, &argspg, actual_args_size, si->argspg, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }
    struct spawn_domain_params *params = argspg;
    char *buf = (char *)(params + 1);
    size_t buflen = ARGS_SIZE - (buf - (char *)argspg);
    int i;
    size_t len;

    for (i = 0; argv[i] != NULL; i++)
    {
        len = strlen(argv[i]) + 1;
        if (len > buflen)
        {
            return SPAWN_ERR_ARGSPG_OVERFLOW;
        }
        strcpy(buf, argv[i]);
        params->argv[i] = buf - (char *)argspg + (char *)(lvaddr_t)spawn_args_base;
        buf += len;
        buflen -= len;
    }

    assert(i <= MAX_CMDLINE_ARGS);
    int argc = i;
    assert(argv[i] == NULL);
    params->argv[i] = NULL;

    /* Copy environment strings */
    for (i = 0; envp[i] != NULL; i++)
    {
        len = strlen(envp[i]) + 1;
        if (len > buflen)
        {
            return SPAWN_ERR_ARGSPG_OVERFLOW;
        }
        strcpy(buf, envp[i]);
        params->envp[i] = buf - (char *)argspg + (char *)(lvaddr_t)spawn_args_base;
        buf += len;
        buflen -= len;
    }

    assert(i <= MAX_ENVIRON_VARS);
    params->envp[i] = NULL;
    assert(envp[i] == NULL);
    params->argc = argc;

    /* Serialise vspace data */
    // XXX: align buf to next word
    char *vspace_buf = (char *)ROUND_UP((lvaddr_t)buf, sizeof(uintptr_t));
    buf = (char *)ROUND_UP((lvaddr_t)buf, sizeof(uintptr_t));
    buflen -= vspace_buf - buf;
    /* Setup environment pointer and vspace pointer */

    //Pointer for l2 state on frame. Referenced via parents vspace
    struct l2_page_state *state_ptr, *target_l2_state = NULL, *l2_history = NULL;
    for (state_ptr = si->child_pdir.head; state_ptr != NULL && buflen > sizeof(struct l2_page_state); state_ptr = state_ptr->next)
    {
        //Set target l2 state to current position in frame referenced by parent
        target_l2_state = (struct l2_page_state *)(buf);
        buf += sizeof(struct l2_page_state);
        *target_l2_state = *state_ptr;

        //If there are any existing mappings on this l2 we need to copy them also
        if (state_ptr->l2_frame_mappings != NULL)
        {
            struct l2_frame_mappings *mapping = NULL;
            struct l2_frame_mappings *mapping_history = NULL;

            //Modify pointer to childs vspace
            target_l2_state->l2_frame_mappings = (struct l2_frame_mappings *)((char *)buf - (char *)argspg + (char *)(lvaddr_t)spawn_args_base);

            for (mapping = state_ptr->l2_frame_mappings; mapping != NULL; mapping = mapping->next)
            {
                memcpy(buf, mapping, sizeof(struct l2_frame_mappings));
                if (mapping->next == NULL)
                {
                    //Modify pointer to childs vspace
                    target_l2_state->l2_frame_mappings_end = (struct l2_frame_mappings *)(buf - (char *)argspg + (char *)(lvaddr_t)spawn_args_base);
                }
                else
                {
                    ((struct l2_frame_mappings *)(buf))->next = (struct l2_frame_mappings *)((buf + sizeof(struct l2_frame_mappings)) - (char *)argspg + (char *)(lvaddr_t)spawn_args_base);
                }
                buf += sizeof(struct l2_frame_mappings);

                //If there is any older mapping we need to update the linked list
                if (mapping_history != NULL)
                {
                    mapping_history->next = mapping;
                }
                mapping_history = mapping;
            }
        }
        //Modify pointer of list to childs vspace
        if (l2_history)
        {
            l2_history->next = (struct l2_page_state *)((char *)target_l2_state - (char *)argspg + (char *)(lvaddr_t)spawn_args_base);
        }
        l2_history = target_l2_state;
        buflen -= sizeof(struct l2_page_state);
    }

    //Set meta information
    params->vspace_buf = ((char *)vspace_buf - (char *)argspg) + (char *)(lvaddr_t)spawn_args_base;
    params->vspace_buf_len = ((char *)buf - (char *)argspg);

    arch_registers_state_t *enabled_area = dispatcher_get_enabled_save_area(si->handle);
    registers_set_param(enabled_area, (uintptr_t)spawn_args_base);
    return SYS_ERR_OK;
}

// TODO(M2): Implement this function such that it starts a new process
// TODO(M4): Build and pass a messaging channel to your child process
errval_t spawn_load_by_name(void *binary_name, struct aos_process *pt)
{
    errval_t err = SYS_ERR_OK;
    if(!pt) {
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
    struct mem_region *module = multiboot_find_module(bi, binary_name);

    if (module == NULL)
    {
        //printf("spawn_start_child >>> mem_region is NULL, could not find module\n");
        return err_push(err, SPAWN_ERR_FIND_MODULE);
    }

    struct capref child_frame = {
        .cnode = cnode_module,
        .slot = module->mrmod_slot,
    };
    const char *command_line = NULL;
    if (bi)
    {
        command_line = multiboot_module_opts(module);
    }
    err = spawn(pt, child_frame,module->mrmod_size,binary_name,command_line);

    return err;
}

errval_t spawn(struct aos_process *pt,struct capref child_frame, size_t size, char *binary_name, const char *command_line) {

    assert((lvaddr_t)pt > BASE_PAGE_SIZE);
    assert((lvaddr_t)binary_name > BASE_PAGE_SIZE);
    errval_t err;
    lvaddr_t address = 0;
    memset(pt, 0, sizeof(struct aos_process));
    pt->si = malloc(sizeof(struct spawninfo));

    struct spawninfo *si = pt->si;

    memset(si, 0, sizeof(struct spawninfo));

    pt->binary_name = malloc(strlen(binary_name));
    pt->out_buf = malloc(SERIAL_OUTPUT_BUF_SIZE);
    if (!pt->binary_name || !pt->out_buf)
    {
        return LIB_ERR_MALLOC_FAIL;
    }

    strcpy(pt->binary_name, binary_name);
    
    pt->next = NULL;
    genvaddr_t entry;

    struct aos_process *last = pt_last();

    if (disp_get_domain_id() != INIT_PID)
    {
        pt->pid = 1000;
        last = pt;
    }
    else if (!last)
    {
        pt_head = pt;
        pt->pid = 1000;
        last = pt;
    }
    else
    {

        last->next = pt;
        coreid_t coreid = disp_get_core_id();

        domainid_t max_pid = get_max_pid(coreid);

        last->next->pid = max_pid + 1;

        last = last->next;

    }

    last->core = disp_get_core_id();
    last->curr_buf = 0;
    last->next = NULL;
    // Get the binary from multiboot image

    si->local_pdir = get_current_paging_state();

    err = paging_map_frame_attr(si->local_pdir, (void **)&address, size,
                                child_frame, VREGION_FLAGS_READ, NULL, NULL);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }

    // Verify module is an ELF
    if (!IS_ELF(*(struct Elf64_Ehdr *)address))
    {
        return ELF_ERR_HEADER;
    }

    ////////////////////////
    // Setup childs cspace//
    ////////////////////////
    err = spawn_setup_cspace(si);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    err = spawn_setup_vspace(si);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    err = paging_init_state(&si->child_pdir, VADDR_OFFSET, si->vtree, &si->pagecn_slot_alloc.a);
    if (err_is_fail(err))
    {
        return err;
    }

    size_t retbytes;
    struct capref frame;
    int minbytes = 64 * sizeof(struct l2_page_state);
    err = frame_alloc(&frame, minbytes, &retbytes);
    if (err_is_fail(err))
    {
        return err;
    }
    lvaddr_t addr;
    err = paging_map_frame_attr(&si->child_pdir, (void **)&addr, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }
    err = paging_map_frame_attr(si->local_pdir, (void **)&addr, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }

    slab_grow(&si->child_pdir.slabs, (void *)addr, retbytes);

    err = elf_load(EM_ARM, elf_alloc, si, address, size, &entry);
    if (err_is_fail(err))
    {
        return err_push(err, SPAWN_ERR_CREATE_PAGECN);
    }
    void *got_addr = NULL;
    struct Elf32_Shdr *got_shdr = elf32_find_section_header_name(address, size, ".got");
    if (got_shdr)
    {
        got_addr = (void *)got_shdr->sh_addr;
    }
    else
    {
        return SPAWN_ERR_LOAD;
    }

    assert(got_addr != NULL);
    coreid_t core_id = disp_get_core_id();
    err = spawn_setup_dispatcher(last, core_id, binary_name, entry, (uintptr_t)got_addr);
    if (err_is_fail(err))
    {
        return err;
    }

    char *argv[64];
    memset(argv, 0, sizeof(char *)*64);
    char *arg_ptr = malloc(64 * sizeof(char) * 64);
    if (arg_ptr == NULL)
    {
        return LIB_ERR_MALLOC_FAIL;
    }

    if (command_line)
    {
        parseCommandlineArgs(command_line, strlen(command_line), arg_ptr, argv);
    }

    argv[0] = malloc(64 * sizeof(char));
    memset(argv[0],0,64);
    if (argv[0] == NULL)
    {
        return LIB_ERR_MALLOC_FAIL;
    }
    strcpy(argv[0], pt->binary_name);

    char *envp[2];
    envp[0] = malloc(13 * sizeof(char));
    if (envp[0] == NULL)
    {
        return LIB_ERR_MALLOC_FAIL;
    }
    if(bi) {
      strncpy(envp[0], multiboot_module_opts(bi->regions), 13 * sizeof(char));
    } else {
        envp[0] = NULL;
    }

    envp[1] = NULL;

    err = spawn_setup_env(si, argv, envp);
    if (err_is_fail(err))
    {
        return err;
    }

    err = invoke_dispatcher(si->dcb, cap_dispatcher, si->rootcn_cap,
                            si->vtree, si->dispframe, true);
    if (err_is_fail(err))
    {
        return err;
    }

    while (si->channel.connstate != LMP_CONNECTED)
    {
        err = event_dispatch(get_default_waitset());
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "in event_dispatch");
        }
    }

    return SYS_ERR_OK;
}

errval_t parseCommandlineArgs(const char *optstring, size_t len, char *space_for_args, char **args_pointers)
{
    //transition function FSM
    //normal -> space -> ReadSpace
    //normal -> \ -> escaped
    //normal -> " -> inString
    //normal -> else -> normal
    //escaped -> elses -> normal
    //inString -> \ -> escapedInString
    //inString -> " -> normal
    //inString -> else -> inString
    //escapedInString -> else -> inString
    //ReadSpace -> Space -> ReadSpace
    //ReadSpace -> \ -> escaped
    //ReadSpace -> " -> inString
    //ReadSpace -> else -> normal

    fsm_state cur_state = READSPACE;
    const char *opt_it = optstring;
    char *args_it = space_for_args;
    uint8_t args_count = 0;
    for (const char *opt_end = opt_it + len + 1; opt_it < opt_end; opt_it++)
    {
        char c = *opt_it;
        if (c == '\0')
        {
            *args_it = '\0';
            args_it++;
            //*((void **) args_it) = NULL;
            break;
        }
        switch (cur_state)
        {
        case NORMAL:
            if (c == ' ')
            {
                cur_state = READSPACE;
                *args_it = '\0';
                args_it++;
            }
            else if (c == '\\')
            {
                cur_state = ESCAPED;
            }
            else if (c == '"')
            {
                cur_state = INSTRING;
            }
            else
            {
                cur_state = NORMAL;
                *args_it = c;
                args_it++;
            }
            break;
        case ESCAPED:
            cur_state = NORMAL;
            *args_it = c;
            args_it++;
            break;
        case INSTRING:
            if (c == '\\')
            {
                cur_state = ESCAPEDINSTRING;
            }
            else if (c == '"')
            {
                cur_state = NORMAL;
            }
            else
            {
                cur_state = INSTRING;
                *args_it = c;
                args_it++;
            }
            break;
        case ESCAPEDINSTRING:
            cur_state = INSTRING;
            *args_it = c;
            args_it++;
        case READSPACE:
            if (c == ' ')
            {
                cur_state = READSPACE;
            }
            else if (c == '\\')
            {
                cur_state = ESCAPED;
                args_pointers[args_count++] = args_it;
            }
            else if (c == '"')
            {
                cur_state = INSTRING;
                args_pointers[args_count++] = args_it;
            }
            else
            {
                cur_state = NORMAL;
                args_pointers[args_count++] = args_it;
                *args_it = c;
                args_it++;
            }
            break;
        default:
            return SPAWN_ERR_GET_CMDLINE_ARGS;
        }
    }
    args_pointers[args_count] = NULL;
    return SYS_ERR_OK;
}

struct aos_process *pt_last(void)
{
    struct aos_process *curr;
    if (!pt_head)
    {
        return NULL;
    }

    for (curr = pt_head; curr->next != NULL; curr = curr->next);
    return curr;
}

domainid_t get_max_pid(coreid_t core) {
    domainid_t pid_highest = 1000;
    for (struct aos_process *pt = pt_head; pt != NULL; pt = pt->next)
    {
        if(pt->core == core && pt->pid > pid_highest) {
            pid_highest = pt->pid;
        }
    }
    return pid_highest;
}

extern struct aos_process *get_process_by_pid(coreid_t coreid, domainid_t domainid) {
    struct aos_process *curr;
    if (!pt_head)
    {
        return NULL;
    }
    for (curr = pt_head; curr != NULL; curr = curr->next) {
        if (curr->pid == domainid && curr->core == coreid)
        {
            return curr;
        }
    }
    return NULL;
}

struct aos_process *get_process_by_name(const char *name) {
    struct aos_process *curr;
    if (!pt_head)
    {
        return NULL;
    }
    for (curr = pt_head; curr != NULL; curr = curr->next) {
        if (strcmp(curr->binary_name,name) == 0)
        {
            return curr;
        }
    }
    return NULL;
}

struct spawninfo *get_spawninfo_by_pid(coreid_t coreid, domainid_t domainid) {
    struct aos_process *curr;
    if (!pt_head)
    {
        return NULL;
    }
    for (curr = pt_head; curr != NULL; curr = curr->next) {
        if (curr->core == coreid && curr->pid == domainid)
        {
            return curr->si;
        }
    }
    return NULL;
}
