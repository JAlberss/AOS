/**
 * \file
 * \brief A library for managing physical memory (i.e., caps)
 */

#include <mm/mm.h>
#include <aos/debug.h>

/**
 * Initialize the memory manager.mee
 *
 * \param  mm               The mm struct to initialize.
 * \param  objtype          The cap type this manager should deal with.
 * \param  slab_refill_func Custom function for refilling the slab allocator.
 * \param  slot_alloc_func  Function for allocating capability slots.
 * \param  slot_refill_func Function for refilling (making) new slots.
 * \param  slot_alloc_inst  Pointer to a slot allocator instance (typically passed to the alloc and refill functions).
 */
errval_t mm_init(struct mm *mm,
                 enum objtype objtype,
                 slab_refill_func_t slab_refill_func,
                 slot_alloc_t slot_alloc_func,
                 slot_refill_t slot_refill_func,
                 void *slot_alloc_inst)
{
    assert(mm != NULL);

    struct slab_allocator slab;
    slab_init(&slab, sizeof(struct mmnode), slab_refill_func);
    mm->slabs = slab;

    mm->objtype = objtype;
    mm->slot_alloc = slot_alloc_func;
    mm->slot_alloc_inst = slot_alloc_inst;
    mm->slot_refill = slot_refill_func;
    mm->head = NULL;
    return SYS_ERR_OK;
}

/**
 * Destroys the memory allocator.
 */
void mm_destroy(struct mm *mm)
{
    if (mm->head != NULL)
    {
        struct mmnode *node = mm->head;
        while (node->next != NULL)
        {
            node = node->next;
            node->prev->prev = NULL;
            node->prev->next = NULL;
        }
        node->next = NULL;
        node->prev = NULL;
    }
    mm->slot_alloc_inst = NULL;
    mm = NULL;
}

/**
 * Adds a capability to the memory manager.
 *
 * \param  cap  Capability to add
 * \param  base Physical base address of the capability
 * \paramiterator  size Size of the capability (in bytes)
 */
errval_t mm_add(struct mm *mm, struct capref cap, genpaddr_t base, size_t size)
{
    assert(size > 0);
    assert(mm != NULL);
    struct mmnode *node = slab_alloc(&mm->slabs);
    assert(node != NULL);
    errval_t err;
    node->cap.cap = cap;
    node->cap.base = base;
    node->cap.size = size;

    node->type = NodeType_Free;
    node->base = base;
    node->size = size;

    if (mm->head == NULL)
    {
        mm->head = node;
        node->next = NULL;
        node->prev = NULL;
        node->left = NULL;
        node->right = NULL;
        if (slab_freecount(&mm->slabs) == 1)
        {
            err = mm->slabs.refill_func(&mm->slabs);
            if (err_is_fail(err))
            {
                debug_printf("err in slab refill mm\n");
                return err;
            }
        }
        return SYS_ERR_OK;
    }
    struct mmnode *iterator;
    for (iterator = mm->head; iterator->next != NULL; iterator = iterator->next)
        ; //Get last element of mmnodes

    iterator->next = node;
    node->prev = iterator;

    if (slab_freecount(&mm->slabs) == 1)
    {
        err = mm->slabs.refill_func(&mm->slabs);
        if (err_is_fail(err))
        {
            debug_printf("err in slab refill mm\n");
            return err;
        }
    }
    return SYS_ERR_OK;
}

/**
 * Allocate aligned physical memory.
 *
 * \param       mm        The memory manager.
 * \param       size      How much memory to allocate.
 * \param       alignment The alignment requirement of the base address for your memory.
 * \param[out]  retcap    Capability for the allocated region.
 */
errval_t mm_alloc_aligned(struct mm *mm, size_t size, size_t alignment, struct capref *retcap)
{
    assert(mm);
    alignment = ROUND_UP(alignment, BASE_PAGE_SIZE);

    //mm_advance(mm);
    // TODO: Implement
    // So, we should use cap_retype to rebrand area we currently
    // have as ram, to two areas; Then pass one back as retcap;
    static bool refilling;
    errval_t err;
    err = mm->slot_refill(mm->slot_alloc_inst);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in slot_refill");
        return err;
    }

    if (!refilling && slab_freecount(&mm->slabs) < 15)
    {
        //debug_printf("slab refill in alloc ram freecount: %d\n", slab_freecount(&mm->slabs));
        //mm_print(mm);

        refilling = 1;
        err = mm->slabs.refill_func(&mm->slabs);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err,"err in slab refill mm\n");
            return err;
        }
        //debug_printf("refill done\n");
        refilling = 0;
    }

    struct mmnode *curr = mm_find(mm);

    // calculate size aligned to given alignment
    size = ROUND_UP(size, alignment);
    if (curr->size == size)
    {
        curr->type = NodeType_Allocated;
        *retcap = curr->cap.cap;
        return SYS_ERR_OK;
    }

    //debug_printf("mm_alloc_aligned size %d alignment: %d curr->size: %" PRIu64 " type: %s cap %d\n", size, alignment, curr->size, node_type_to_string(curr->type), curr->cap.cap);
    err = mm->slot_alloc(mm->slot_alloc_inst, 1, retcap);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in retcap slot_alloc");
        return err;
    }
    struct capref mmnode_capref;
    err = mm->slot_alloc(mm->slot_alloc_inst, 1, &mmnode_capref);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in mmnode_capref slot_alloc");
        return err;
    }    


    if (curr->cap.size < size)
    {
        //Implement error handling
        debug_printf("OOM in mm\n");
    }

    genvaddr_t new_base = ROUND_UP(curr->base, alignment);
    size_t offset = new_base - curr->base;
    size_t new_size = curr->cap.size - size - offset;
    //debug_printf("new size: %d\n",new_size);
    err = cap_retype(mmnode_capref, curr->cap.cap, size + offset, ObjType_RAM, new_size, 1);
    if (err_is_fail(err))
    {

        DEBUG_ERR(err, "in mmnode_capref cap_retype");
        return err;
    }        


    // In this function we will chop up other side of the capability. This is the RAM we are keeping and putting in a new capref
    // TODO: Figure out how to delete the old cap ref
    err = cap_retype(*retcap, curr->cap.cap, offset, ObjType_RAM, size, 1); // This is the RAM we are giving away
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in retcap cap_retype");
        return err;
    }
    //mm_add(mm, mmnode_capref, curr->cap.base + size, new_size);

    struct mmnode *retcap_node = slab_alloc(&mm->slabs);
    struct mmnode *node = slab_alloc(&mm->slabs);
assert(retcap_node != NULL);
assert(node != NULL);
if (retcap_node)
{
    struct mmnode tmp_node = {
        .type = NodeType_Allocated,
        .cap = {
            .cap = *retcap,
            .base = new_base,
            .size = size,
        },
        .prev = curr,
        .next = node,
        .base = new_base,
        .size = size,
        .parent = curr,
        .left = NULL,
        .right = NULL,
    };
    *retcap_node = tmp_node;
    curr->next = retcap_node;
    }

    //assert(false);
    if (node)
    {
        struct mmnode tmp_node =
            {
                .type = NodeType_Free,
                .cap = {
                    .cap = mmnode_capref,
                    .base = new_base + size,
                    .size = new_size,
                },
                .prev = retcap_node,
                .next = NULL,
                .base = new_base + size,
                .size = new_size,
                .parent = curr,
                .left = NULL,
                .right = NULL,
            };
        *node = tmp_node;
    }

    curr->type = NodeType_Allocated;
    curr->left = retcap_node;
    curr->right = node;
    return SYS_ERR_OK;
}

/**
 * Allocate physical memory.
 *
 * \param       mm        The memory manager.
 * \param       size      How much memory to allocate.
 * \param[out]  retcap    Capability for the allocated region.
 */
errval_t mm_alloc(struct mm *mm, size_t size, struct capref *retcap)
{
    return mm_alloc_aligned(mm, size, BASE_PAGE_SIZE, retcap);
}

/**
 * Free a certain region (for later re-use).
 *
 * \param       mm        The memory manager.
 * \param       cap       The capability to free.
 * \param       base      The physical base address of the region.
 * \param       size      The size of the region.
 */
errval_t mm_free(struct mm *mm, struct capref cap, genpaddr_t base, gensize_t size)
{
    struct mmnode *runner = mm->head;
    while (runner != NULL)
    {
        //debug_printf("runner: %x\n", runner);
        if (base == runner->base && size == runner->size)
        {
            struct capref ramcap;
            errval_t err = mm->slot_alloc(mm->slot_alloc_inst, 1, &ramcap);
            if (err_is_fail(err))
            {
                DEBUG_ERR(err, "in slot_alloc");
                return err;
            }
            cap_destroy(cap);
            size_t offset = base - runner->parent->base;
            err = cap_retype(ramcap, runner->parent->cap.cap, offset, ObjType_RAM, size, 1);
            if (err_is_fail(err))
            {
                DEBUG_ERR(err, "in cap_retype");
                return err;
            }
            runner->cap.cap = ramcap;
            runner->cap.base = base;
            runner->cap.size = size;

            runner->type = NodeType_Free;
            runner->base = base;
            runner->size = size;

            break;
        }
        runner = runner->next;
    }
    return SYS_ERR_OK;
}

/**
 * This function will find the first free mmnode
 */
struct mmnode *mm_find(struct mm *mm)
{
    //mm_print(mm);
    assert(mm);
    struct mmnode *runner = mm->head;
    while (runner->type != NodeType_Free)
    {
        runner = runner->next;
    }
    return runner;
}

void mm_print(struct mm *mm)
{
    struct mmnode *mmnode;
    int i = 0;
    for (mmnode = mm->head; mmnode != NULL; mmnode = mmnode->next)
    {
        debug_printf("node %d, type: %s, base: %" PRIx64 " size: %" PRIu64 "\n", i, node_type_to_string(mmnode->type), mmnode->base, mmnode->size);
        i++;
    }
}

char *node_type_to_string(enum nodetype type)
{
    switch (type)
    {
    case NodeType_Allocated:
        return "NodeType_Allocated";
    case NodeType_Free:
        return "NodeType_Free";
    default:
        return "Unknown Nodetype";
    }
}
