/**
 * \file
 * \brief Implementation of Shell helper functions like parsing
 */

#include <aos/aos.h>
#include <aos/aos_shell.h>

errval_t parse_line(char *input, struct aos_inputline *inputline, struct hashtable *ht)
{
    errval_t err;
    struct aos_inputline *start = inputline;
    memset(inputline, 0, sizeof(struct aos_inputline));
    inputline->ht = ht;
    inputline->next = NULL;
    struct cmd_parser cmd_parser;
    init_parser(&cmd_parser, strlen(input));
    while (cmd_parser.end != cmd_parser.len)
    {
        if (cmd_parser.state == CMD_PARSER_REDIRECTION)
        {
            switch (input[cmd_parser.end])
            {
            case '|':
                inputline->redirection_type = REDIRECTION_PIPE;
                break;
            case '>':
                if (input[cmd_parser.end + 1] == '>')
                {
                    inputline->redirection_type = REDIRECTION_APP;
                    cmd_parser.start++;
                    cmd_parser.end++;
                    break;
                }
                inputline->redirection_type = REDIRECTION_OUT;
                break;
            case '<':
                inputline->redirection_type = REDIRECTION_IN;
                break;
            }
            inputline->next = malloc(sizeof(struct aos_inputline));
            inputline = inputline->next;
            memset(inputline, 0, sizeof(struct aos_inputline));
            inputline->ht = ht;
        }
        err = parse(&cmd_parser, input);
        if (err_is_fail(err))
        {
            return SYS_ERR_OK;
        }
        if (cmd_parser.end - cmd_parser.start != 0)
        {
            inputline->argc++;
        }
    }
    inputline = start;
    init_parser(&cmd_parser, strlen(input));
    for (inputline = start; inputline != NULL; inputline = inputline->next)
    {
        inputline->argv = malloc(inputline->argc * sizeof(char *));
        for (int i = 0; i < inputline->argc; i++)
        {
            err = parse(&cmd_parser, input);
            if (err_is_fail(err))
            {
                return SYS_ERR_OK;
            }
            size_t line_length = (cmd_parser.end - cmd_parser.start);
            if (line_length == 0)
            {
                i--;
                continue;
            }
            inputline->argv[i] = malloc(line_length + 1);
            memset(inputline->argv[i], 0, line_length + 1);
            memcpy(inputline->argv[i], input + cmd_parser.start, line_length * sizeof(char));
            inputline->argv[i][line_length + 1] = '\0';
            sanitize_line(inputline->argv[i], line_length);
        }
    }
    return SYS_ERR_OK;
}

errval_t sanitize_line(char *line, size_t size)
{
    if (line[0] == '\'' && line[size - 1] == '\'')
    {
        for (int i = 0; i < size - 1; i++)
        {
            line[i] = line[i + 1];
        }
        line[size - 2] = '\0';
    }
    return SYS_ERR_OK;
}

bool input_is_redirection(char c)
{
    switch (c)
    {
    case '|':
    case '>':
    case '<':
        return true;
    }
    return false;
}

errval_t parse(struct cmd_parser *cmd_parser, char *input)
{
    if (cmd_parser->start != cmd_parser->end)
    {
        cmd_parser->start = cmd_parser->end;
        cmd_parser->state = CMD_PARSER_INIT;
    }
    if (cmd_parser->state == CMD_PARSER_REDIRECTION)
    {
        cmd_parser->start++;
        cmd_parser->end++;
    }

    for (int i = cmd_parser->end; i < cmd_parser->len; i++)
    {
        if (cmd_parser->state == CMD_PARSER_REDIRECTION)
        {
            cmd_parser->state = CMD_PARSER_INIT;
        }
        if (cmd_parser->state == CMD_PARSER_INIT && input[i] == ' ')
        {
            //Skip n spaces
            cmd_parser->start++;
            cmd_parser->end++;
            continue;
        }
        else if (cmd_parser->state == CMD_PARSER_INIT && input[i] == '\0')
        {
            //Return if parser reaches end of line before reading anything
            return SYS_ERR_OK;
        }
        else if (cmd_parser->state == CMD_PARSER_INIT)
        {
            //real start after space skip
            cmd_parser->state = CMD_PARSER_START;
        }

        if (cmd_parser->state == CMD_PARSER_START && input[i] == '\'')
        {
            //Get Quote switch to skip mode
            cmd_parser->state = CMD_PARSER_SKIP;
        }
        else if (cmd_parser->state == CMD_PARSER_START && input_is_redirection(input[i]))
        {

            cmd_parser->state = CMD_PARSER_REDIRECTION;

            return SYS_ERR_OK;
        }
        else if (cmd_parser->state == CMD_PARSER_START && (input[i] == ' ' || input[i] == '\0'))
        {
            //word successfull parsed at space or end
            cmd_parser->state = CMD_PARSER_END;
            return SYS_ERR_OK;
        }
        else if (cmd_parser->state == CMD_PARSER_START)
        {
            //Normal case consume one char
            cmd_parser->end++;
            continue;
        }
        else if (cmd_parser->state == CMD_PARSER_SKIP && input[i] == '\'')
        {
            //End of quoted string
            cmd_parser->state = CMD_PARSER_START;
        }
        else if (cmd_parser->state == CMD_PARSER_SKIP && input[i] != '\'')
        {
            //In quoted string. Consume one char independet of type
            cmd_parser->end++;
            continue;
        }
        else
        {
            return LIB_ERR_SHELL_PARSE_LINE;
        }
        cmd_parser->end++;
    }
    return SYS_ERR_OK;
}

errval_t free_line(struct aos_inputline *inputline)
{
    if (!inputline)
    {
        return SYS_ERR_OK;
    }
    for (struct aos_inputline *curr_line = inputline; curr_line != NULL; curr_line = curr_line->next)
    {
        for (uint32_t i = 0; i < curr_line->argc; i++)
        {
            if (curr_line->argv && curr_line->argv[i])
            {
                free(curr_line->argv[i]);
            }
        }
    }
    free(inputline);
    return SYS_ERR_OK;
}

void debug_print_inputline(bool debug, struct aos_inputline *inputline)
{
    if (!debug)
        return;
    if (!inputline)
    {
        debug_printf("Inputline is NULL\n");
        return;
    }
    uint32_t e = 0;
    debug_printf_clean("\n");
    for (struct aos_inputline *curr_line = inputline; curr_line != NULL; curr_line = curr_line->next)
    {
        debug_printf("inputline %d (%p) has %d parameters redirection type: %s\n", e, curr_line, curr_line->argc, redirection_type_to_str(curr_line->redirection_type));
        for (uint32_t i = 0; i < curr_line->argc; i++)
        {
            if (curr_line->argv && curr_line->argv[i])
            {
                debug_printf("\targv[%d]: |%s|\n", i, curr_line->argv[i]);
            }
            else
            {
                debug_printf("argv[%d] is null\n", i);
            }
        }
        e++;
    }
}

errval_t init_parser(struct cmd_parser *cmd_parser, size_t len)
{
    cmd_parser->state = CMD_PARSER_INIT;
    cmd_parser->start = 0;
    cmd_parser->end = 0;
    cmd_parser->len = len;
    return SYS_ERR_OK;
}

char *redirection_type_to_str(enum redirection_type type)
{
    switch (type)
    {
    case REDIRECTION_NULL:
        return "REDIRECTION_NULL";
    case REDIRECTION_PIPE:
        return "REDIRECTION_PIPE";
    case REDIRECTION_OUT:
        return "REDIRECTION_OUT";
    case REDIRECTION_IN:
        return "REDIRECTION_IN";
    default:
        return "Redirection Type unknown";
    }
}