/**
 * \file
 * \brief Implementation of AOS rpc-like messaging
 */

/*
 * Copyright (c) 2013 - 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
 */

#include <sys/types.h>

#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_helper.h>
#include <aos/aos_ump.h>
#include <spawn/spawn.h>
#include <barrelfish_kpi/asm_inlines_arch.h>
#include <aos/caddr.h>
#include <aos/domain.h>

struct aos_process *pt_head = NULL;
struct aos_process *pt_focused = NULL;
struct aos_process *serialdrv = NULL;

errval_t aos_rpc_send_number(struct aos_rpc *chan, uintptr_t val)
{
  return aos_rpc_send_number_type(chan, val, MSG_TYPE_INT);
}

errval_t aos_rpc_ns_lookup(struct aos_rpc *rpc,char *name,struct capref *retcap) {
  errval_t err;
  if (strlen(name) > (LMP_MSG_LENGTH-1)*sizeof(uintptr_t))
  {
    return LIB_ERR_STRING_TOO_LONG;
  }
  err = lmp_chan_alloc_recv_slot(&rpc->chan);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "%s failed to alloc slot\n", __FUNCTION__);
    return err;
  }
  err = aos_rpc_send_string_type(rpc, MSG_TYPE_NS_LOOKUP_REQ, name);
  if(err_is_fail(err)) {
    DEBUG_ERR(err, "in send MSG_TYPE_NS_LOOKUP_REQ to init\n");
    return err;
  }
  err = wait_for_msg_or_err(rpc, get_default_waitset(), MSG_TYPE_NS_LOOKUP_RESP_OK, MSG_TYPE_NS_LOOKUP_RESP_FAIL);

  if(err_is_fail(err)) {
    return err;
  }
  err = cap_copy(*retcap, rpc->chan.endpoint->recv_slot);

  if(err_is_fail(err)) {
    return err;
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_ns_register(struct aos_rpc *rpc, char *name)
{
  errval_t err;
  if (strlen(name) > 21)
  {
    return LIB_ERR_STRING_TOO_LONG;
  }
  err = aos_rpc_send_string_type(rpc, MSG_TYPE_NS_REGISTER_REQ, name);
  if(err_is_fail(err)) {
    DEBUG_ERR(err, "in send MSG_TYPE_NS_REGISTER to init\n");
    return err;
  }

  err = wait_for_msg_or_err(rpc, get_default_waitset(), MSG_TYPE_NS_REGISTER_RESP_OK,MSG_TYPE_NS_REGISTER_RESP_FAIL);
  if(err_is_fail(err)) {
    return err;
  }

    struct capref a = rpc->chan.local_cap;
    err = aos_rpc_send_cap_type(rpc, a, MSG_TYPE_NS_REGISTER_CAP);
    if (err_is_fail(err))
    {
      DEBUG_ERR(err, "Error in sending endpoint cap\n");
      return err;
  }

  err = wait_for_msg(rpc, get_default_waitset(), MSG_TYPE_NS_REGISTER_CAP_OK);
  if(err_is_fail(err)) {
    DEBUG_ERR(err, "in wait for ns register cap ok");
    return err;
  }
  err = aos_rpc_send_number_type(rpc, 0, MSG_TYPE_NS_REGISTER_FIN);
  if (err_is_fail(err)){
    DEBUG_ERR(err,"aos_rpc_send_number type failed in aos rpc request cap\n");
    return err;
  }
  return SYS_ERR_OK;
}

errval_t wait_for_msg(struct aos_rpc *rpc,struct waitset *ws,enum msg_type type) {
  return wait_for_msg_or_err(rpc, ws, type, MSG_TYPE_NULL);
}

errval_t wait_for_msg_or_err(struct aos_rpc *chan,struct waitset *ws,enum msg_type ok_type,enum msg_type err_type) {
  errval_t err;
  while (true)
  {
    err = event_dispatch(ws);
    if (err_is_fail(err))
    {
      DEBUG_ERR(err, "cannot get default waitset\n");
      return err;
    }
    if (chan->msg->words[RPC_TYPE_SLOT] == ok_type)
    {
      chan->msg->words[RPC_TYPE_SLOT] = 0;
      return SYS_ERR_OK;
    } else if (chan->msg->words[RPC_TYPE_SLOT] == err_type)
    {
      chan->msg->words[RPC_TYPE_SLOT] = 0;
      return LIB_ERR_RPC_ERR_RESP;
    } else {
      //debug_printf("unexp: %d %s\n", chan->msg->words[RPC_TYPE_SLOT], msg_to_string(chan->msg->words[RPC_TYPE_SLOT]));
    }
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_process_get_pid(struct aos_rpc *chan, domainid_t *pid) {
  errval_t err = aos_rpc_send_number_type(chan, 0, MSG_TYPE_PID_REQ);
  if(err_is_fail(err)){
    return err;
  }
  err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_PID_RESP);
  if (err_is_fail(err))
  {
    return err;
  }
  *pid = chan->msg->words[2];

  return SYS_ERR_OK;
}

errval_t aos_rpc_send_number_type_chan(struct lmp_chan *chan ,uintptr_t len, uintptr_t val, enum msg_type msg_type)
{
  errval_t err;
  while (true)
  {
   // if(msg_words[RPC_TYPE_SLOT] == MSG_TYPE_GET_CHAR_RESP)
      //debug_printf("a %c %x %d\n",msg_words[RPC_NUMBER_PAYLOAD_FIELD],msg_words[RPC_NUMBER_PAYLOAD_FIELD],i);

   err = aos_rpc_send_number_type_chan_nonblock(chan, len, val, msg_type);
   if (lmp_err_is_transient(err))
   {
     thread_yield();
     continue;
    }
    else if (err_is_fail(err))
    {
      return err;
    }
    else
    {
       break;
    }
  }
  return SYS_ERR_OK;
}
errval_t aos_rpc_send_number_type_chan_nonblock(struct lmp_chan *chan ,uintptr_t len, uintptr_t val, enum msg_type msg_type)
{
  uintptr_t msg_words[LMP_MSG_LENGTH];
  errval_t err;
  msg_words[RPC_TYPE_SLOT] = msg_type;
  msg_words[RPC_NUMBER_LEN_FIELD] = len;
  msg_words[RPC_NUMBER_PAYLOAD_FIELD] = val;
  msg_words[RPC_NUMBER_CORE_FIELD] = disp_get_core_id();
  msg_words[RPC_NUMBER_PID_FIELD] = disp_get_domain_id();

  err = lmp_chan_send5(chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, msg_words[RPC_TYPE_SLOT],
                          msg_words[RPC_NUMBER_LEN_FIELD], msg_words[RPC_NUMBER_PAYLOAD_FIELD],
                          msg_words[RPC_NUMBER_CORE_FIELD], msg_words[RPC_NUMBER_PID_FIELD]);
  return err;
}

void aos_rpc_dev_null(struct aos_rpc *rpc) {
  return;
}

errval_t aos_rpc_send_number_type(struct aos_rpc *chan, uintptr_t val, enum msg_type msg_type)
{
  return aos_rpc_send_number_type_chan(&chan->chan,1,val, msg_type);
}

errval_t aos_rpc_send_string(struct aos_rpc *chan, const char *string)
{
  return aos_rpc_send_string_type(chan, MSG_TYPE_STRING_FULL, string);
}

errval_t aos_rpc_send_string_type(struct aos_rpc *chan, enum msg_type type, const char *string)
{
  errval_t err;
  size_t len = strlen(string);
  size_t junks = ((len / (sizeof(uintptr_t) * 6)) + 1);
  uintptr_t msg_words[LMP_MSG_LENGTH];
  if (junks > 1)
  {
    msg_words[RPC_TYPE_SLOT] = MSG_TYPE_STRING_PARTIAL;
  }
  else
  {
    msg_words[RPC_TYPE_SLOT] = type;
  }
  msg_words[RPC_STRING_LEN_FIELD] = len;
  uint32_t offset = 0;
  char *part;
  for (int i = 0; i < junks; i++)
  {
    if (i == junks - 1 && junks != 1)
    {
      msg_words[RPC_TYPE_SLOT] = MSG_TYPE_STRING_FULL;
    }
    int e;
    char c;
    for (e = 0; e < LMP_MSG_LENGTH - LMP_MSG_HEADER; e++)
    {
      c = 0;
      msg_words[e + LMP_MSG_HEADER] = 0;
      part = ((char *)string + offset);
      c = part[0];
      msg_words[e + LMP_MSG_HEADER] |= (c << 24);
      if (c == '\0')
      {
        break;
      }
      c = part[1];
      msg_words[e + LMP_MSG_HEADER] |= (c << 16);
      if (c == '\0')
      {
        break;
      }
      c = part[2];
      msg_words[e + LMP_MSG_HEADER] |= (c << 8);
      if (c == '\0')
      {
        break;
      }
      c = part[3];
      msg_words[e + LMP_MSG_HEADER] |= c;
      if (c == '\0')
      {
        break;
      }
      offset += sizeof(uintptr_t);
    }
    for (int fill = e + 1; fill < LMP_MSG_LENGTH - LMP_MSG_HEADER; fill++)
    {
      msg_words[fill + LMP_MSG_HEADER] = 0;
    }
    while (true)
    {
      err = lmp_chan_send9(&chan->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, msg_words[RPC_TYPE_SLOT], msg_words[RPC_STRING_LEN_FIELD],
                           msg_words[2], msg_words[3], msg_words[4], msg_words[5], msg_words[6],
                           msg_words[7], msg_words[8]);
      if (err == SYS_ERR_LMP_BUF_OVERFLOW)
      {
        debug_printf("overflow\n");
        continue;
      }
      else if (err_is_fail(err))
      {
        DEBUG_ERR(err, "send string");
        return err;
      }
      else
      {
        //debug_printf("sended\n");
        break;
      }
    }
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_get_ram_cap(struct aos_rpc *chan, size_t size, size_t align,
                             struct capref *retcap, size_t *ret_size)
{
  // TODO: implement functionality to request a RAM capability over the
  // given channel and wait until it is delivered.
  if (chan->ram_request_handler == default_handler)
  {
    set_ram_response_handler(get_init_rpc(), ram_response_handler);
  }
  uintptr_t msg_words[LMP_MSG_LENGTH];
  msg_words[RPC_TYPE_SLOT] = MSG_TYPE_RAM_REQ;
  msg_words[RPC_RAM_SIZE_FIELD] = size;
  msg_words[RPC_RAM_ALIGN_FIELD] = align;
  errval_t err = SYS_ERR_OK;
  err = lmp_chan_alloc_recv_slot(&chan->chan);

  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "%s failed to alloc slot\n", __FUNCTION__);
  }

  while (true)
  {
    err = lmp_chan_send3(&chan->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, msg_words[RPC_TYPE_SLOT],
                          msg_words[RPC_RAM_SIZE_FIELD], msg_words[RPC_RAM_ALIGN_FIELD]);
    if (lmp_err_is_transient(err))
    {
      continue;
    }
    else if (err_is_fail(err))
    {
      return err;
    }
    else
    {
      break;
    }
  }

  err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_RAM_RESP_OK);
  if(err_is_fail(err)) {
    return err;
  }

  retcap->slot = chan->chan.endpoint->recv_slot.slot;
  retcap->cnode = chan->chan.endpoint->recv_slot.cnode;
  return SYS_ERR_OK;
}


errval_t aos_rpc_process_spawn(struct aos_rpc *chan, char *name,
                               coreid_t core, domainid_t *newpid)
{
  // TODO (milestone 5): implement spawn new process rpc
  uintptr_t msg_words[LMP_MSG_LENGTH];
  memset(msg_words, 0, LMP_MSG_LENGTH * sizeof(uintptr_t));
  msg_words[RPC_TYPE_SLOT] = MSG_TYPE_SPAWN_REQ;
  msg_words[RPC_SPAWN_CORE_FIELD] = core;
  errval_t err;
  if (strlen(name) > 21)
  { //TODO add constant
    DEBUG_ERR(-1, "Failure in %s bc of length\n", __FUNCTION__);
    return -1;
  }
  str_to_ptr(name, msg_words + 2, (LMP_MSG_LENGTH - LMP_MSG_HEADER));
  while (true)
  {
    err = lmp_chan_send9(&chan->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, msg_words[RPC_TYPE_SLOT], msg_words[RPC_SPAWN_CORE_FIELD],
                         msg_words[2], msg_words[3], msg_words[4], msg_words[5], msg_words[6],
                         msg_words[7], msg_words[8]);
    if (lmp_err_is_transient(err))
    {
      continue;
    } else if (err_is_fail(err))
    {
      DEBUG_ERR(err, "lmp chan send 9 error\n");
      return err;
    } else {
      break;
    }
  }
  err = wait_for_msg_or_err(chan, get_default_waitset(), MSG_TYPE_SPAWN_RESP_OK, MSG_TYPE_SPAWN_RESP_FAIL);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "SPAWN RESP FAIL received\n");
    return err;
  }
  *newpid = chan->msg->words[RPC_NUMBER_PAYLOAD_FIELD];

  return SYS_ERR_OK;
}

errval_t aos_rpc_process_get_name(struct aos_rpc *chan, domainid_t pid,
                                  char **name)
{
  errval_t err;
  err = lmp_chan_send2(&chan->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, MSG_TYPE_PNAME_REQ, pid);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "lmp chan send 9 error\n");
    return err;
  }

  err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_STRING_FULL);
  if(err_is_fail(err)) {
    DEBUG_ERR(err, "in wait for string full");
    return err;
  }
  *name = get_init_rpc()->string_buf;
  return SYS_ERR_OK;
}


errval_t aos_rpc_process_get_all_pids(struct aos_rpc *chan, domainid_t **pids, size_t *pid_count)
{
  errval_t err;
  if (chan->get_all_pids_resp_handler != get_all_pids_resp_handler)
  {
    debug_printf("set pids resp handler on chan %p\n", chan);
    set_get_pid_resp_handler(get_init_rpc(), get_all_pids_resp_handler);
  }
  err = lmp_chan_send1(&chan->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, MSG_TYPE_PIDLIST_REQ);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "lmp chan send 2 error\n");
    return err;
  }
  int i = 0;
  while(true) {
    err = wait_for_msg_or_err(chan, get_default_waitset(), MSG_TYPE_PIDLIST_RESP, MSG_TYPE_PIDLIST_END);
    if (err_is_fail(err))
    {
      *pid_count = i;
      break;
    }
    *((*pids) + i) = chan->msg->words[2];
    i++;
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_process_get_ptable(struct aos_rpc *chan, char **ptable)
{
  errval_t err;
  err = lmp_chan_send1(&chan->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, MSG_TYPE_PT_REQ);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "lmp chan send 1 error\n");
    return err;
  }

  while (true)
  {
    err = event_dispatch(get_default_waitset());
    if (err_is_fail(err))
    {
      DEBUG_ERR(err, "Cannot get default waitset\n");
      return err;
    }
    if (chan->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_STRING_FIN || chan->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_STRING_FULL)
    {
      break;
    }
  }
  *ptable = get_init_rpc()->string_buf;
  return SYS_ERR_OK;
}

errval_t aos_rpc_get_device_cap(struct aos_rpc *rpc,
                                lpaddr_t paddr, size_t bytes,
                                struct capref *frame)
{
  errval_t err;
  struct capref dev_frame;
  err = slot_alloc(&dev_frame);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "Error in slot alloc for dev cap\n");
    return err;
  }
  err = aos_rpc_request_cap(rpc, TASKCN_SLOT_DEVCAP, &dev_frame);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "Error in request dev cap\n");
    return err;
  }

  err = slot_alloc(frame);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "Error in slot alloc for return frame cap\n");
    return err;
  }
  struct frame_identity dev_frame_id;
  err = frame_identify(dev_frame, &dev_frame_id);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "Error in identify dev frame\n");
    return err;
  }
  gensize_t offset = paddr - dev_frame_id.base;
  err = cap_retype(*frame, dev_frame, offset, ObjType_DevFrame, bytes, 1);
  if (err_is_fail(err))
  {
    DEBUG_ERR(err, "Error in cap retype to dev_frame %s %s\n",err_getcode(err),err_getstring(err));
    return err;
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_bind(struct aos_rpc *rpc, coreid_t dest_core, domainid_t dest_pid, coreid_t source_core, domainid_t source_pid, lvaddr_t *urpc_frame){
  errval_t err;

  struct capref urpc_frame_cap;
  struct frame_identity urpc_frame_id;
  size_t retbytes;
  err = frame_alloc(&urpc_frame_cap, MON_URPC_SIZE, &retbytes);
  if (err_is_fail(err)) {
    return err;
  }
  err= frame_identify(urpc_frame_cap, &urpc_frame_id);
  if (err_is_fail(err)) {
    return err;
  }
  err = paging_map_frame_attr(get_current_paging_state(), (void**) urpc_frame, retbytes, urpc_frame_cap, VREGION_FLAGS_READ_WRITE, NULL, NULL);
  if (err_is_fail(err)){
    return err;
  }
  memset((void *)*urpc_frame, 0, retbytes);
  //debug_printf("mapped urpc frame on %p\n", urpc_frame);

  err = aos_rpc_forward_bind(rpc, MSG_TYPE_BIND_REQ,dest_core, dest_pid, source_core, source_pid, urpc_frame_id.base, retbytes);
  if(err_is_fail(err)) {
    return err;
  }

  err = wait_for_msg_or_err(rpc, get_default_waitset(), MSG_TYPE_BIND_ACK, MSG_TYPE_BIND_FAIL);
  if(err_is_fail(err)) {
    return LIB_ERR_UMP_CHAN_BIND;
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_send_bind_fail(struct aos_rpc *rpc, coreid_t dest_core, domainid_t dest_pid, coreid_t source_core, domainid_t source_pid, genpaddr_t urpc_frame, size_t bytes){
  return aos_rpc_forward_bind(rpc, MSG_TYPE_BIND_FAIL, dest_core, dest_pid, source_core, source_pid, urpc_frame, bytes);
}


errval_t aos_rpc_request_cap(struct aos_rpc *rpc, cslot_t slot, struct capref *retcap) {
  errval_t err;
  err = lmp_chan_alloc_recv_slot(&rpc->chan);
  if (err_is_fail(err)){
    DEBUG_ERR(err,"aos_rpc_send_number type failed in aos rpc request cap\n");
    return err;
  }
  err = aos_rpc_send_number_type(rpc, slot, MSG_TYPE_CAP_REQ);
  if (err_is_fail(err)){
    DEBUG_ERR(err,"aos_rpc_send_number type failed in aos rpc request cap\n");
    return err;
  }
  err = wait_for_msg_or_err(rpc, get_default_waitset(), MSG_TYPE_CAP_RESP_OK,MSG_TYPE_CAP_RESP_FAIL);
  if(err_is_fail(err)) {
    return LIB_ERR_RPC_REQ_CAP_FAIL;
  }

  err = cap_copy(*retcap, rpc->chan.endpoint->recv_slot);
  if(err_is_fail(err)) {
    return err;
  }
  return SYS_ERR_OK;
}
errval_t aos_rpc_request_spawn_cap(struct aos_rpc *rpc, char *name, struct capref *retcap) {
  errval_t err;
  err = lmp_chan_alloc_recv_slot(&rpc->chan);
  if (err_is_fail(err)){
    DEBUG_ERR(err,"aos_rpc_send_number type failed in aos rpc request cap\n");
    return err;
  }
  uintptr_t msg_words[9];
  msg_words[RPC_TYPE_SLOT] = MSG_TYPE_SPAWN_CAP_REQ;
  msg_words[RPC_SPAWN_CORE_FIELD] = 0;
  str_to_ptr(name, msg_words + 2, (LMP_MSG_LENGTH - LMP_MSG_HEADER));
  while((err = lmp_chan_send9(&rpc->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, msg_words[RPC_TYPE_SLOT], msg_words[RPC_SPAWN_CORE_FIELD],
                       msg_words[2], msg_words[3], msg_words[4], msg_words[5], msg_words[6],
                       msg_words[7], msg_words[8])) != SYS_ERR_OK);

  err = wait_for_msg_or_err(rpc, get_default_waitset(), MSG_TYPE_SPAWN_CAP_RESP_OK, MSG_TYPE_SPAWN_CAP_RESP_FAIL);
  if (err_is_fail(err))
  {
    return SPAWN_ERR_DOMAIN_NOTFOUND;
  }
  cap_copy(*retcap, rpc->chan.endpoint->recv_slot);

  return SYS_ERR_OK;
}
errval_t aos_rpc_send_cap_type(struct aos_rpc *chan, struct capref cap, enum msg_type type)
{
  errval_t err;
  while (true)
  {
    err = lmp_chan_send1(&chan->chan, LMP_SEND_FLAGS_DEFAULT, cap, type);
    if (lmp_err_is_transient(err))
    {
      DEBUG_ERR(err, "lmp_chan_send1: Transient error");
      continue;
    }
    else if (err_is_fail(err))
    {
      DEBUG_ERR(err, "lmp_chan_send1: Failed to send Ram cap\n");
      return err;
    }
    else
    {
      break;
    }
  }
  return SYS_ERR_OK;
}

errval_t str_to_ptr(char *buf, uintptr_t *msg_words, size_t size)
{
  assert(buf != NULL);
  assert(msg_words != NULL);
  assert(size > 0);
  char *part;
  int e;
  char c;
  uint32_t offset = 0; // from above
  for (e = 0; e < size; e++)
  {
    c = 0;
    msg_words[e] = 0;
    part = ((char *)buf + offset);
    c = part[0];
    msg_words[e] |= (c << 24);
    if (c == '\0')
    {
      break;
    }
    c = part[1];
    msg_words[e] |= (c << 16);
    if (c == '\0')
    {
      break;
    }
    c = part[2];
    msg_words[e] |= (c << 8);
    if (c == '\0')
    {
      break;
    }
    c = part[3];
    msg_words[e] |= c;
    if (c == '\0')
    {
      break;
    }
    // debug_printf("Word %d is %d 0x%10x\n",i+e,msg_words[e+LMP_MSG_HEADER],msg_words[e+LMP_MSG_HEADER]);
    offset += sizeof(uintptr_t);
  }
  for (int fill = e + 1; fill < size; fill++)
  {
    msg_words[fill] = 0;
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_init(struct aos_rpc *rpc)
{
  // TODO: Initialize given rpc channel
  *rpc = *aos_rpc_get_init_channel();
  return SYS_ERR_OK;
}

errval_t aos_rpc_alloc(struct aos_rpc *rpc, struct lmp_chan *chan, bool reregister)
{
  if (!rpc)
  {
    return LIB_ERR_MALLOC_FAIL;
  }
  rpc->chan = *chan;
  rpc->remote_cap = chan->remote_cap;
  rpc->reregister = reregister;
  rpc->default_waitset = get_default_waitset();
  set_number_handler(rpc, default_handler);
  set_string_handler(rpc, string_handler);
  set_getchar_handler(rpc, default_handler);
  set_putchar_handler(rpc, default_handler);
  set_ram_request_handler(rpc, default_handler);
  set_exit_request_handler(rpc, default_handler);
  set_terminal_req_handler(rpc, default_handler);
  set_spawn_request_handler(rpc, default_handler);
  set_get_name_handler(rpc, default_handler);
  set_get_pid_resp_handler(rpc, default_handler);
  set_get_all_pids_req_handler(rpc, default_handler);
  set_get_device_cap_handler(rpc, default_handler);
  set_get_ptable_handler(rpc, default_handler);
  set_default_handler(rpc, default_handler);
  set_bind_ack_handler(rpc, default_handler);
  set_bind_handler(rpc, bind_req_handler);
  set_bind_cap_handler(rpc, aos_rpc_bind_cap_resp_handler);
  set_bind_fail_handler(rpc, bind_fail_handler);
  set_cap_handler(rpc, default_handler);
  set_serialdrv_recv_handler(rpc, default_handler);
  set_serialdrv_send_handler(rpc, default_handler);
  set_get_spawn_cap_handler(rpc, default_handler);
  set_generic_handler(&rpc->ns_register_handler, default_handler);
  set_generic_handler(&rpc->ns_lookup_handler, default_handler);
  set_generic_handler(&rpc->network_bind_handler, default_handler);
  set_generic_handler(&rpc->network_in_packet_handler, aos_rpc_network_in_packet_handler);
  set_generic_handler(&rpc->network_udp_payload_handler, aos_rpc_network_out_packet_handler);
  set_remote_cap_invoke_req_handler(rpc, default_handler);
  set_remote_cap_invoke_handler(rpc, default_handler);
  set_remote_cap_send_req_handler(rpc, default_handler);
  set_remote_cap_send_handler(rpc, default_handler);
  return SYS_ERR_OK;
}
/**
 * \brief Returns the RPC channel to init.
 */
struct aos_rpc *aos_rpc_get_init_channel(void)
{
  return get_init_rpc();
}

/**
 * \brief Returns the channel to the memory server
 */
struct aos_rpc *aos_rpc_get_memory_channel(void)
{
  return get_init_rpc();
}

/**
 * \brief Returns the channel to the process manager
 */
struct aos_rpc *aos_rpc_get_process_channel(void)
{
  return get_init_rpc();
}

/**
 * \brief Returns the channel to the serial console
 */
struct aos_rpc *aos_rpc_get_serial_channel(void)
{
  if(serialdrv) {
    return serialdrv->rpc;
  }
  //debug_printf("no serialdrv\n");
  return get_init_rpc();
}

char *aos_rpc_send_string_receive(struct aos_rpc *chan)
{
  errval_t err;
  struct lmp_chan *lc = &chan->chan;
  struct capref cap;
  if (chan->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_STRING_PARTIAL)
  {

    char *longstr = chan->string_buf;
    memset(longstr, '\0', 1024);
    uint32_t offset = 0;
    while (true)
    {
      if (chan->msg->words[RPC_TYPE_SLOT] != MSG_TYPE_NULL)
      {
        for (int a = 0; a < (*(chan->msg)).buf.msglen - 2; a++)
        {
          parse_int_to_string(chan->msg->words[a + 2], longstr + offset);
          offset += sizeof(uintptr_t);
        }
        if (chan->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_STRING_FULL)
        {
          chan->msg->words[RPC_TYPE_SLOT] = MSG_TYPE_STRING_FIN;
          break;
        }
      }
      chan->msg->words[RPC_TYPE_SLOT] = MSG_TYPE_NULL;
      err = lmp_chan_recv(lc, chan->msg, &cap);
    }
    //return longstr;
    //debug_printf("received longstring: %s\n", longstr);
    return longstr;
  }
  else
  {
    char *buf = chan->string_buf;
    memset(buf, '\0', 1024);
    for (int i = 0; i < (*(chan->msg)).buf.msglen - 2; i++)
    {
      parse_int_to_string(chan->msg->words[i + 2], &buf[i * sizeof(uintptr_t)]);
    }
    return buf;
  }
  return NULL;
}

/**
 * \brief Our default message receive handler
 */

void default_handler(struct aos_rpc *arg)
{
  debug_printf("default handler not implemented yet Type: %s\n",msg_to_string(arg->msg->words[RPC_TYPE_SLOT]));
}

/**
 * \brief handler for binding requests
 */
void bind_req_handler(struct aos_rpc *arg)
{
  coreid_t dest_core;
  coreid_t source_core;
  domainid_t dest_pid;
  domainid_t source_pid;
  genvaddr_t frame_addr;
  size_t bytes;
  aos_rpc_bind_receive(arg->msg->words, &dest_core, &dest_pid, &source_core, &source_pid, &frame_addr, &bytes);

  errval_t err;
  err = lmp_chan_alloc_recv_slot(&arg->chan);
  if (err_is_fail(err))
  {
    debug_printf("%s failed to alloc slot\n", __FUNCTION__);
    aos_rpc_send_bind_fail(arg, dest_core, dest_pid, source_core, source_pid, frame_addr, bytes);
    return;
  }

  while (true)
  {
    err = lmp_chan_send8(&arg->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, MSG_TYPE_BIND_ACK, source_core, source_pid, dest_core, dest_pid, arg->msg->words[5], arg->msg->words[6], bytes);
    if (lmp_err_is_transient(err))
    {
      continue;
    }
    else if (err_is_fail(err))
    {
      return;
    }
    else
    {
      break;
    }
  }
}

/**
 * \brief Handles incomming RAM RESPONSES. Normaly there is nothing to do beside debugging output -> cap gets handled by sender functionc
 */

void ram_response_handler(struct aos_rpc *arg)
{
  if (arg->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_RAM_RESP_OK)
  {
    return;
    //debug_printf("Successfull ram response received\n");
  }
  else if (arg->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_RAM_RESP_FAIL)
  {
    debug_printf("Error in Ram Recieve\n");
  }
  else
  {
    debug_printf("In ram response handler with MSG_TYPE: %s\n", msg_to_string(arg->msg->words[RPC_TYPE_SLOT]));
  }
}

void get_all_pids_resp_handler(struct aos_rpc *arg)
{
  if (arg->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_PIDLIST_RESP)
  {
    return;
  }
  else if (arg->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_PIDLIST_END)
  {
    return;
  }
  else
  {
    debug_printf("in get_all_pids_resp_handler with MSG_TYPE: %s\n", msg_to_string(arg->msg->words[RPC_TYPE_SLOT]));
  }
}

void string_handler(struct aos_rpc *rpc)
{
  aos_rpc_send_string_receive(rpc);
  
}

void parse_int_to_string(uintptr_t number, char *buf)
{
  *buf = (number >> 24);
  *(buf + 1) = (number >> 16);
  *(buf + 2) = (number >> 8);
  *(buf + 3) = ((char)number);
}

char *msg_to_string(enum msg_type type)
{
  switch (type)
  {
  case MSG_TYPE_STRING_PARTIAL:
    return "MSG_TYPE_STRING_PARTIAL";
  case MSG_TYPE_STRING_FULL:
    return "MSG_TYPE_STRING_FULL";
  case MSG_TYPE_INT:
    return "MSG_TYPE_INT";
  case MSG_TYPE_RAM_REQ:
    return "MSG_TYPE_RAM_REQ";
  case MSG_TYPE_GET_CHAR_REQ:
    return "MSG_TYPE_GET_CHAR_REQ";
      case MSG_TYPE_GET_CHAR_RESP:
    return "MSG_TYPE_GET_CHAR_RESP";
  case MSG_TYPE_RAM_RESP_OK:
    return "MSG_TYPE_RAM_RESP_OK";
  case MSG_TYPE_RAM_RESP_FAIL:
    return "MSG_TYPE_RAM_RESP_FAIL";
  case MSG_TYPE_PT_REQ:
    return "MSG_TYPE_PT_REQ";
  case MSG_TYPE_PNAME_REQ:
    return "MSG_TYPE_PNAME_REQ";
  case MSG_TYPE_PIDLIST_REQ:
    return "MSG_TYPE_PIDLIST_REQ";
  case MSG_TYPE_SPAWN_REQ:
    return "MSG_TYPE_SPAWN_REQ";
  case MSG_TYPE_NULL:
    return "MSG_TYPE_NULL";
  case MSG_TYPE_NOT_SUPPORTED:
    return "MSG_TYPE_NOT_SUPPORTED";
  case MSG_TYPE_SPAWN_RESP_OK:
    return "MSG_TYPE_SPAWN_RESP_OK";
  case MSG_TYPE_SPAWN_RESP_FAIL:
    return "MSG_TYPE_SPAWN_RESP_FAIL";
  case MSG_TYPE_PNAME_RESP_OK:
    return "MSG_TYPE_PNAME_RESP_OK";
  case MSG_TYPE_PNAME_RESP_FAIL:
    return "MSG_TYPE_PNAME_RESP_FAIL";
  case MSG_TYPE_PT_RESP:
    return "MSG_TYPE_PT_RESP";
  case MSG_TYPE_PIDLIST_RESP:
    return "MSG_TYPE_PIDLIST_RESP";
  case MSG_TYPE_PIDLIST_END:
    return "MSG_TYPE_PIDLIST_END";
  case MSG_TYPE_STRING_FIN:
    return "MSG_TYPE_STRING_FIN";
  case MSG_TYPE_EXIT:
    return "MSG_TYPE_EXIT";
  case MSG_TYPE_PID_RESP:
    return "MSG_TYPE_PID_RESP";
  case MSG_TYPE_PID_REQ:
    return "MSG_TYPE_PID_REQ";
  case MSG_TYPE_BIND_ACK:
    return "MSG_TYPE_BIND_ACK";
  case MSG_TYPE_BIND_CAP:
    return "MSG_TYPE_BIND_CAP";
  case MSG_TYPE_BIND_REQ:
    return "MSG_TYPE_BIND_REQ";
  case MSG_TYPE_BIND_RESP:
    return "MSG_TYPE_BIND_RESP";
  case MSG_TYPE_BIND_FAIL:
    return "MSG_TYPE_BIND_FAIL";
  case MSG_TYPE_TEST:
    return "MSG_TYPE_TEST";
  case MSG_TYPE_CAP_REQ:
    return "MSG_TYPE_CAP_REQ";
  case MSG_TYPE_CAP_RESP_OK:
    return "MSG_TYPE_CAP_RESP_OK";
  case MSG_TYPE_CAP_RESP_FAIL:
    return "MSG_TYPE_CAP_RESP_FAIL";
  case MSG_TYPE_CHILD_SPAWNED:
    return "MSG_TYPE_CHILD_SPAWNED";
  case MSG_TYPE_TERMINAL_CTRL:
    return "MSG_TYPE_TERMINAL_CTRL";
  case MSG_TYPE_PUT_CHAR:
    return "MSG_TYPE_PUT_CHAR";
  case MSG_TYPE_SERIALDRV_RECV:
    return "MSG_TYPE_SERIALDRV_RECV";
  case MSG_TYPE_SERIALDRV_SEND:
    return "MSG_TYPE_SERIALDRV_SEND";
  case MSG_TYPE_SPAWN_CAP_REQ:
    return "MSG_TYPE_SPAWN_CAP_REQ";
  case MSG_TYPE_SPAWN_CAP_RESP_FAIL:
    return "MSG_TYPE_SPAWN_CAP_RESP_FAIL";
  case MSG_TYPE_SPAWN_CAP_RESP_OK:
    return "MSG_TYPE_SPAWN_CAP_RESP_OK";
  case MSG_TYPE_NS_REGISTER_REQ:
    return "MSG_TYPE_NS_REGISTER_REQ";
  case MSG_TYPE_NS_REGISTER_RESP_FAIL:
    return "MSG_TYPE_NS_REGISTER_RESP_FAIL";
  case MSG_TYPE_NS_REGISTER_RESP_OK:
    return "MSG_TYPE_NS_REGISTER_RESP_OK";
  case MSG_TYPE_NS_REGISTER_CAP:
    return "MSG_TYPE_NS_REGISTER_CAP";
  case MSG_TYPE_NS_REGISTER_CAP_OK:
    return "MSG_TYPE_NS_REGISTER_CAP_OK";
  case MSG_TYPE_NS_REGISTER_FIN:
    return "MSG_TYPE_NS_REGISTER_FIN";
  case MSG_TYPE_NS_LOOKUP_REQ:
    return "MSG_TYPE_NS_LOOKUP_REQ";
  case MSG_TYPE_NS_LOOKUP_RESP_FAIL:
    return "MSG_TYPE_NS_LOOKUP_RESP_FAIL";
  case MSG_TYPE_NS_LOOKUP_RESP_OK:
    return "MSG_TYPE_NS_LOOKUP_RESP_OK";
  case MSG_TYPE_RPC_BOOTSTRAP:
    return "MSG_TYPE_RPC_BOOTSTRAP";
  case MSG_TYPE_RPC_BOOTSTRAP_OK:
    return "MSG_TYPE_RPC_BOOTSTRAP_OK";
  case MSG_TYPE_NETWORK_PAYLOAD_START:
    return "MSG_TYPE_NETWORK_PAYLOAD_START";
  case MSG_TYPE_NETWORK_PAYLOAD_CONTENT:
    return "MSG_TYPE_NETWORK_PAYLOAD_CONTENT";
  case MSG_TYPE_NETWORK_PAYLOAD_FIN:
    return "MSG_TYPE_NETWORK_PAYLOAD_FIN";
  case MSG_TYPE_NETWORK_BIND:
    return "MSG_TYPE_NETWORK_BIND";
  case MSG_TYPE_NETWORK_BIND_OK:
    return "MSG_TYPE_NETWORK_BIND_OK";
  case MSG_TYPE_NETWORK_BIND_FAIL:
    return "MSG_TYPE_NETWORK_BIND_FAIL";
  case MSG_TYPE_NETWORK_UDP_PAYLOAD_START:
    return "MSG_TYPE_NETWORK_UDP_PAYLOAD_START";
  case MSG_TYPE_NETWORK_UDP_PAYLOAD_CONTENT:
    return "MSG_TYPE_NETWORK_UDP_PAYLOAD_CONTENT";
  case MSG_TYPE_NETWORK_UDP_PAYLOAD_FIN:
    return "MSG_TYPE_NETWORK_UDP_PAYLOAD_FIN";
  case MSG_TYPE_CAP_REMOTE_SEND_REQ:
    return "MSG_TYPE_CAP_REMOTE_SEND_REQ";
  case MSG_TYPE_CAP_REMOTE_SEND_ACK:
    return "MSG_TYPE_CAP_REMOTE_SEND_ACK";
  case MSG_TYPE_CAP_REMOTE_SEND:
    return "MSG_TYPE_CAP_REMOTE_SEND";
  case MSG_TYPE_REMOTE_CAP_INVOKE_REQ:
    return "MSG_TYPE_REMOTE_CAP_INVOKE_REQ";
  case MSG_TYPE_REMOTE_CAP_INVOKE_ACK:
    return "MSG_TYPE_REMOTE_CAP_INVOKE_ACK";
  case MSG_TYPE_REMOTE_CAP_INVOKE_SEND:
    return "MSG_TYPE_REMOTE_CAP_INVOKE_SEND";
  case MSG_TYPE_REMOTE_CAP_INVOKE_LOCAL:
    return "MSG_TYPE_REMOTE_CAP_INVOKE_LOCAL";
  case MSG_TYPE_REMOTE_CAP_INVOKE_FAIL:
    return "MSG_TYPE_REMOTE_CAP_INVOKE_FAIL";
  default:
    return "msg_to_str: unknown";
  }
}

void msg_recv_handler(void *arg)
{
  errval_t err;
  struct aos_rpc *aos_rpc = arg;
  struct lmp_chan *lc = &aos_rpc->chan;
  memset(aos_rpc->msg, 0, sizeof(struct lmp_recv_msg));
  (*(aos_rpc->msg)).buf.buflen = LMP_MSG_LENGTH; //?
  //try to get cap, reregister if transient error occurs
  //struct capref cap;
  err = lmp_chan_recv(lc, aos_rpc->msg, &aos_rpc->recv_cap);
  if (err_is_fail(err) && lmp_err_is_transient(err))
  {
    err = lmp_chan_register_recv(&aos_rpc->chan, aos_rpc->default_waitset,
                                 MKCLOSURE(msg_recv_handler, aos_rpc));
    return;
  }

  if ((*(aos_rpc->msg)).buf.msglen > 0)
  {
    //debug_printf("recv: %s\n",msg_to_string(aos_rpc->msg->words[RPC_TYPE_SLOT]));
    switch (aos_rpc->msg->words[RPC_TYPE_SLOT])
    {
    case MSG_TYPE_STRING_PARTIAL:
    case MSG_TYPE_STRING_FULL:
      //debug_printf("string handler\n");
      err = invoke_handler(aos_rpc->string_handler,aos_rpc);
      break;
    case MSG_TYPE_INT:
      //debug_printf("number handler\n");
      if ((*(aos_rpc->msg)).buf.msglen == 3)
      {
        err = invoke_handler(aos_rpc->number_handler,aos_rpc);
      }
      break;
    case MSG_TYPE_RAM_REQ:
      //debug_printf("ram request handler\n");
      err = invoke_handler(aos_rpc->ram_request_handler,aos_rpc);
      break;
    case MSG_TYPE_EXIT:
      err = invoke_handler(aos_rpc->exit_request_handler,aos_rpc);
      break;
    case MSG_TYPE_TERMINAL_CTRL:
      err = invoke_handler(aos_rpc->terminal_req_handler,aos_rpc);
    break;
    case MSG_TYPE_PID_REQ:
      err = invoke_handler(aos_rpc->pid_request_handler,aos_rpc);
      break;
    case MSG_TYPE_PUT_CHAR:
      //debug_printf("put char handler\n");
      if ((*(aos_rpc->msg)).buf.msglen >= 3)
      {
        err = invoke_handler(aos_rpc->putchar_handler,aos_rpc);
      }
      break;
      case MSG_TYPE_GET_CHAR_REQ:
      if ((*(aos_rpc->msg)).buf.msglen >= 3)
      {
        err = invoke_handler(aos_rpc->getchar_handler,aos_rpc);
      }
      break;
    case MSG_TYPE_RAM_RESP_OK:
    case MSG_TYPE_RAM_RESP_FAIL:
      err = invoke_handler(aos_rpc->ram_response_handler,aos_rpc);
      break;
    case MSG_TYPE_PT_REQ:
      err = invoke_handler(aos_rpc->get_ptable_handler,aos_rpc);
      break;
    case MSG_TYPE_PNAME_REQ:
      err = invoke_handler(aos_rpc->get_name_handler,aos_rpc);
      break;
    case MSG_TYPE_PIDLIST_REQ:
      err = invoke_handler(aos_rpc->get_all_pids_req_handler,aos_rpc);
      break;
    case MSG_TYPE_PIDLIST_RESP:
    case MSG_TYPE_PIDLIST_END:
      err = invoke_handler(aos_rpc->get_all_pids_resp_handler,aos_rpc);
      break;
    case MSG_TYPE_SPAWN_REQ:
      err = invoke_handler(aos_rpc->spawn_request_handler,aos_rpc);
      break;
    case MSG_TYPE_BIND_REQ:
      err = invoke_handler(aos_rpc->bind_handler,aos_rpc);
      break;
    case MSG_TYPE_BIND_ACK:
      err = invoke_handler(aos_rpc->bind_ack_handler,aos_rpc);
      break;
    case MSG_TYPE_BIND_CAP:
      err = invoke_handler(aos_rpc->bind_cap_handler,aos_rpc);
      break;
    case MSG_TYPE_BIND_FAIL:
      err = invoke_handler(aos_rpc->bind_fail_handler,aos_rpc);
      break;
    case MSG_TYPE_CAP_REQ:
      err = invoke_handler(aos_rpc->cap_response_handler, aos_rpc);
      break;
    case MSG_TYPE_CHILD_SPAWNED:
      err = invoke_handler(aos_rpc->child_spawn_handler, aos_rpc);
      break;
    case MSG_TYPE_SERIALDRV_RECV:
      err = invoke_handler(aos_rpc->serialdrv_recv_handler, aos_rpc);
      break;
    case MSG_TYPE_SERIALDRV_SEND:
      err = invoke_handler(aos_rpc->serialdrv_send_handler, aos_rpc);
      break;
    case MSG_TYPE_SPAWN_CAP_REQ:
      err = invoke_handler(aos_rpc->get_spawn_cap_handler, aos_rpc);
      break;
    case MSG_TYPE_NS_REGISTER_REQ:
      err = invoke_handler(aos_rpc->ns_register_handler, aos_rpc);
      break;
    case MSG_TYPE_NETWORK_BIND:
      err = invoke_handler(aos_rpc->network_bind_handler, aos_rpc);
      break;
    case MSG_TYPE_NETWORK_PAYLOAD_START:
    case MSG_TYPE_NETWORK_PAYLOAD_CONTENT:
    case MSG_TYPE_NETWORK_PAYLOAD_FIN:
      err = invoke_handler(aos_rpc->network_in_packet_handler, aos_rpc);
      break;
    case MSG_TYPE_NS_LOOKUP_REQ:
      err = invoke_handler(aos_rpc->ns_lookup_handler, aos_rpc);
      break;
    case MSG_TYPE_RPC_BOOTSTRAP:
      err = invoke_handler(aos_rpc->rpc_bootstrap_handler, aos_rpc);
      break;
    case MSG_TYPE_NETWORK_UDP_PAYLOAD_START:
    case MSG_TYPE_NETWORK_UDP_PAYLOAD_CONTENT:
    case MSG_TYPE_NETWORK_UDP_PAYLOAD_FIN:
      err = invoke_handler(aos_rpc->network_udp_payload_handler, aos_rpc);
      break;
    case MSG_TYPE_REMOTE_CAP_INVOKE_REQ:
      err = invoke_handler(aos_rpc->remote_cap_invoke_req_handler, aos_rpc);
      break;
    case MSG_TYPE_REMOTE_CAP_INVOKE_SEND:
      err = invoke_handler(aos_rpc->remote_cap_invoke_handler, aos_rpc);
      break;  
    case MSG_TYPE_CAP_REMOTE_SEND_REQ:
      err = invoke_handler(aos_rpc->remote_cap_send_req_handler, aos_rpc);
      break;
    case MSG_TYPE_CAP_REMOTE_SEND:
      err = invoke_handler(aos_rpc->remote_cap_send_handler, aos_rpc);
      break;
    case MSG_TYPE_REMOTE_CAP_INVOKE_FAIL:
    case MSG_TYPE_REMOTE_CAP_INVOKE_LOCAL:
    case MSG_TYPE_REMOTE_CAP_INVOKE_ACK:
    case MSG_TYPE_CAP_REMOTE_SEND_ACK:
    case MSG_TYPE_NS_REGISTER_FIN:
    case MSG_TYPE_NS_REGISTER_RESP_FAIL:
    case MSG_TYPE_NS_REGISTER_RESP_OK:
    case MSG_TYPE_NS_REGISTER_CAP_OK:
    case MSG_TYPE_NS_REGISTER_CAP:
    case MSG_TYPE_GET_CHAR_RESP:
    case MSG_TYPE_NETWORK_BIND_OK:
    case MSG_TYPE_CAP_RESP_OK:
    case MSG_TYPE_SPAWN_CAP_RESP_OK:
    case MSG_TYPE_SPAWN_CAP_RESP_FAIL:
    case MSG_TYPE_NS_LOOKUP_RESP_FAIL:
    case MSG_TYPE_NS_LOOKUP_RESP_OK:
    case MSG_TYPE_RPC_BOOTSTRAP_OK:
    case MSG_TYPE_SPAWN_RESP_FAIL:
    case MSG_TYPE_SPAWN_RESP_OK:
    case RPC_NUMBER_PAYLOAD_FIELD:
      //Since handling happens in client in this case just break
      break;

    case MSG_TYPE_NULL:
    default:
      err = invoke_handler(aos_rpc->default_handler, aos_rpc);
      break;
    }
  }
  if(err == LIB_ERR_RPC_FUNC_NULL) {
    debug_printf("Error while invoke handler for msg type: %s probabbly function pointer to handler not set!\n",msg_to_string(aos_rpc->msg->words[RPC_TYPE_SLOT]));
  } else if(err_is_fail(err)) {
    DEBUG_ERR(err, "in handle msg");
  }
  if (aos_rpc->reregister)
  {
    err = lmp_chan_register_recv(&aos_rpc->chan, aos_rpc->default_waitset,
                                 MKCLOSURE(msg_recv_handler, aos_rpc));
    if (err_is_fail(err))
    {
      DEBUG_ERR(err,"msg_recv_handler >>> Error in reregister channel\n");
    }
  }
}

errval_t invoke_handler(aos_rpc_recv_handler_t handler, struct aos_rpc *rpc) {
  if(handler && rpc) {
    handler(rpc);
  } else {
    debug_printf("handler not set\n");
    return LIB_ERR_RPC_FUNC_NULL;
  }
  return SYS_ERR_OK;
}

char *get_ptable(struct aos_process *pt)
{
  struct aos_process *head = pt;
  size_t bufsize = 0;
  for (struct aos_process *pt_ptr = head; pt_ptr != NULL; pt_ptr = pt_ptr->next)
  {
    bufsize += strlen(pt_ptr->binary_name)+2; //Strlen + seperator and delimiter
  }
  bufsize += 2; //Delimiters
  char *ret = malloc(bufsize);
  memset(ret, '\0', bufsize);
  strcat(ret, ">");

  for (struct aos_process *pt_ptr = head; pt_ptr != NULL; pt_ptr = pt_ptr->next)
  {
    char pid[5];
    char core[2];
    memset(pid, '\0', sizeof(pid));
    memset(core, '\0', sizeof(core));
    aos_itoa(pt_ptr->core, core, 2);

    strcat(ret, core);
    strcat(ret, ".");
    aos_itoa(pt_ptr->pid, pid, 5);

    strcat(ret, pid);
    strcat(ret, ":");
    strcat(ret, pt_ptr->binary_name);
    if(pt_ptr->next != NULL) {
      strcat(ret, "|");
    }

  }
      strcat(ret, "<");

  return ret;
}

char *get_name_by_pid(struct aos_rpc *aos_rpc, coreid_t core, domainid_t pid)
{
  for (struct aos_process *pt = pt_head; pt != NULL;pt = pt->next) {
    if(pt->core == core && pt->pid == pid) {
      return pt->binary_name;
    }
  }
  return NULL;
}

void aos_itoa(uint32_t src, char *dest, size_t length)
{
  int base = 10;
  for (int i = length - 2; i >= 0; i--)
  {
    dest[i] = ('0' + (src % base));
    src /= base;
  }
  dest[length - 1] = '\0';
}

void get_ram_request_data(struct aos_rpc *chan, size_t *size, size_t *align)
{
  *size = chan->msg->words[RPC_RAM_SIZE_FIELD];
  *align = chan->msg->words[RPC_RAM_ALIGN_FIELD];
  return;
}




errval_t aos_rpc_forward_bind(struct aos_rpc *rpc, enum msg_type type,coreid_t dest_core, domainid_t dest_pid, coreid_t source_core, domainid_t source_pid, genpaddr_t urpc_frame, size_t bytes) {
  uintptr_t msg_words[LMP_MSG_LENGTH];

  errval_t err;
  msg_words[RPC_TYPE_SLOT] = type;
  msg_words[RPC_BIND_DEST_CORE_FIELD]         = dest_core;
  msg_words[RPC_BIND_DESP_PID_FIELD]          = dest_pid;
  msg_words[RPC_BIND_SRC_CORE_FIELD]          = source_core;
  msg_words[RPC_BIND_SRC_PID_FIELD]           = source_pid;
  msg_words[RPC_BIND_URPC_FRAME_PART1_FIELD]  = (uintptr_t) (urpc_frame >> 32);
  msg_words[RPC_BIND_URPC_FRAME_PART2_FIELD]  = (uintptr_t) urpc_frame;
  msg_words[RPC_BIND_URPC_FRAME_SIZE_FIELD]   = bytes;

  while (true)
  {
    err = lmp_chan_send8(&rpc->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP,
                        msg_words[RPC_TYPE_SLOT], msg_words[RPC_BIND_DEST_CORE_FIELD], msg_words[RPC_BIND_DESP_PID_FIELD],
                        msg_words[RPC_BIND_SRC_CORE_FIELD], msg_words[RPC_BIND_SRC_PID_FIELD], msg_words[RPC_BIND_URPC_FRAME_PART1_FIELD], 
                        msg_words[RPC_BIND_URPC_FRAME_PART2_FIELD], msg_words[RPC_BIND_URPC_FRAME_SIZE_FIELD]);
    if (lmp_err_is_transient(err))
    {
      debug_printf("transient error while sending\n");
      continue;
    }
    else if (err_is_fail(err))
    {
      return err;
    }
    else
    {
      //debug_printf("bind req sent\n");
      break;
    }
  }

  return SYS_ERR_OK;
}

void aos_rpc_bind_receive(uintptr_t *words, coreid_t *dest_core, domainid_t *dest_pid, coreid_t *source_core, domainid_t *source_pid, genpaddr_t *frame_addr, size_t *bytes) {
  *dest_core = words[RPC_BIND_DEST_CORE_FIELD];
  *dest_pid = words[RPC_BIND_DESP_PID_FIELD];
  *source_core = words[RPC_BIND_SRC_CORE_FIELD];
  *source_pid = words[RPC_BIND_SRC_PID_FIELD];
  *frame_addr = (((genpaddr_t) words[RPC_BIND_URPC_FRAME_PART1_FIELD]) << 32) + (genpaddr_t) words[RPC_BIND_URPC_FRAME_PART2_FIELD];
  *bytes = words[RPC_BIND_URPC_FRAME_SIZE_FIELD];
}

void aos_rpc_bind_cap_resp_handler(struct aos_rpc *rpc) {
  coreid_t dest_core;
  coreid_t source_core;
  domainid_t dest_pid;
  domainid_t source_pid;
  genvaddr_t frame_addr;
  lvaddr_t urpc_frame_buf;
  size_t bytes;
  errval_t err;
  struct capref urpc_frame = rpc->chan.endpoint->recv_slot;
  aos_rpc_bind_receive(rpc->msg->words, &dest_core, &dest_pid, &source_core, &source_pid, &frame_addr, &bytes);
  err = paging_map_frame_attr(get_current_paging_state(), (void **)&urpc_frame_buf, bytes, urpc_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
  if (err_is_fail(err))
  {
    debug_printf("Error while mapping URPC Frame from core: %d pid: %d to core: %d pid: %d\n", source_core, source_pid, dest_core, dest_pid);
  }

  err = init_urpc_descriptors(&urpc_head, urpc_frame_buf, urpc_frame_buf + (URPC_NUM_SLOTS * sizeof(struct urpc_slot)), source_core, source_pid);
  if(err_is_fail(err)) {
    debug_printf("error in create urpc desc\n");
  }
}

void aos_rpc_cap_handler(struct aos_rpc *rpc) {
  // Handles cap requests
  errval_t err;
  cslot_t req_slot = rpc->msg->words[2];
  switch(req_slot){
  case TASKCN_SLOT_DEVCAP:
  case TASKCN_SLOT_IRQ:
    break;
  default:
    debug_printf("not allowed to send cap slot %d. Allowed (%d,%d)\n",req_slot,TASKCN_SLOT_DEVCAP,TASKCN_SLOT_IRQ);
    aos_rpc_send_number_type(rpc, 0, MSG_TYPE_CAP_RESP_FAIL);
    return;
  }
  struct capref ret = {
    .cnode = cnode_task,
    .slot = req_slot
  };
  while(true) {
    err = lmp_chan_send1(&rpc->chan, LMP_SEND_FLAGS_DEFAULT, ret, MSG_TYPE_CAP_RESP_OK);
    if (lmp_err_is_transient(err)){
      continue;
    }
    else if (err_is_fail(err)) {
      DEBUG_ERR(err, "err in lmp chan send in aos_rpc_cap_handler");
      return;
    } else {
      break;
    }
  }

}

void bind_fail_handler(struct aos_rpc* rpc){
  debug_printf("bind_fail_handler not yet implemented!\n");
}

void aos_rpc_bootstrap_handler(struct aos_rpc *rpc){
  errval_t err;
  struct lmp_chan serial_lmp;
  lmp_chan_init(&serial_lmp);
  err = slot_alloc(&serial_lmp.remote_cap);
  if(err_is_fail(err)) {
    debug_printf("failed to slot_alloc\n");
  }
  if(capref_is_null(rpc->chan.endpoint->recv_slot)) {
    debug_printf("ep cap is null\n");
  }
  err = cap_copy(serial_lmp.remote_cap, rpc->chan.endpoint->recv_slot);
  if (err_is_ok(err))
  {
    lmp_chan_alloc_recv_slot(&rpc->chan);
    slot_alloc(&serial_lmp.local_cap);
    cap_copy(serial_lmp.local_cap, rpc->chan.local_cap);
    serial_lmp.endpoint = rpc->chan.endpoint;
    err = lmp_chan_send1(&serial_lmp, LMP_SEND_FLAGS_DEFAULT, NULL_CAP, MSG_TYPE_RPC_BOOTSTRAP_OK);
    if (err_is_fail(err))
    {
      DEBUG_ERR(err, "in lmp_ep_send0");
      return;
    }
  } else {
    debug_printf("failed to copy\n");
  }
}

struct lmp_chan *get_rpc_by_pid(struct aos_process *pt_start,coreid_t core,domainid_t pid) {
  if(pt_start == NULL) {
    debug_printf("start is null\n");
    return NULL;
  }
  struct aos_process *pt;
  for (pt = pt_start; pt != NULL;pt = pt->next) {
    if(pt->pid == pid && pt->core == core) {
      return &pt->si->channel;
    }
  }
  return NULL;
}

struct lmp_chan *get_rpc_by_name(struct aos_process *pt_start, char *name)
{
  if (pt_start == NULL)
  {
    return NULL;
  }
  struct aos_process *pt;
  for (pt = pt_start; pt != NULL; pt = pt->next)
  {
    if (strcmp(pt->binary_name, name) == 0)
    {
      return &pt->si->channel;
    }
  }
  return NULL;
}

void number_handler(struct aos_rpc *rpc) {
    int n = rpc->msg->words[2];
    debug_printf("received %d\n", n);
}

void aos_rpc_ram_handler_init(struct aos_rpc *rpc) {
    //debug_printf("MSG_TYPE_RAM_REQ is found\n");
    errval_t err;
    size_t size;
    size_t align;
    struct capref ram;
    get_ram_request_data(rpc, &size, &align);
    err = ram_alloc_aligned(&ram, size, align);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Ram_Alloc_aligned failed in aos_rpc\n");
        return;
    }
    err = aos_rpc_send_cap_type(rpc, ram, MSG_TYPE_RAM_RESP_OK);
    if(err_is_fail(err)) {
      DEBUG_ERR(err, "error while sending ram cap");
    }
}

errval_t aos_rpc_remote_cap_invoke_local(struct aos_rpc *chan, struct capref root, capaddr_t src, uint8_t level, enum cap_invocation_type invoke_type)
{
  chan->recv_cap = root;
  chan->msg->words[0] = MSG_TYPE_REMOTE_CAP_INVOKE_LOCAL;
  chan->msg->words[1] = src;
  chan->msg->words[2] = level;
  chan->msg->words[3] = invoke_type;
  errval_t err = invoke_handler(chan->remote_cap_invoke_handler, chan);
  if (err_is_fail(err)) {
    return err;
  }
  if (chan->msg->words[0] == MSG_TYPE_REMOTE_CAP_INVOKE_FAIL)
  {
    return chan->msg->words[1];
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_remote_cap_invoke(struct aos_rpc *chan, struct capref root, capaddr_t src, uint8_t level, enum cap_invocation_type invoke_type)
{
    debug_printf("in aos_rpc_remote_cap_invoke with chan = %p\n", chan);
    errval_t err = aos_rpc_send_number_type(chan, 0, MSG_TYPE_REMOTE_CAP_INVOKE_REQ);
    if(err_is_fail(err))
    {
    return err;
    }
    debug_printf("in remote_cap_invoke requested\n");
    err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_REMOTE_CAP_INVOKE_ACK);
    if (err_is_fail(err))
    {
        return err;
    }
    debug_printf("in remote_cap_invoke acked\n");
    while (true)
    {
      err = lmp_chan_send4(&chan->chan, LMP_SEND_FLAGS_DEFAULT, root,
              MSG_TYPE_REMOTE_CAP_INVOKE_SEND, src, level, invoke_type);
      if (lmp_err_is_transient(err))
      {
          DEBUG_ERR(err, "lmp_chan_send4: Transient error");
          continue;
      }
      else if (err_is_fail(err))
      {
          DEBUG_ERR(err, "lmp_chan_send4: Failed to send cap\n");
          return err;
      }
      else
      {
          break;
      }
    }
    err = wait_for_msg_or_err(chan, get_default_waitset(), MSG_TYPE_REMOTE_CAP_INVOKE_ACK, MSG_TYPE_REMOTE_CAP_INVOKE_FAIL);
    if (err_is_fail(err)) 
    {
      if (err == LIB_ERR_RPC_ERR_RESP)
      {
         return chan->msg->words[1];
      }
      return err;
    }
    return SYS_ERR_OK;
}


errval_t aos_rpc_send_cap_remote(struct aos_rpc* chan, struct capref cap, coreid_t core,
         domainid_t pid)
{
    coreid_t my_core = disp_get_core_id();
    domainid_t my_pid = disp_get_domain_id();
    if (my_core == core)
    {
        return LIB_ERR_NOT_IMPLEMENTED;
    }
    if (my_pid == INIT_PID)
    {
        return LIB_ERR_NOT_IMPLEMENTED;
    }
    if (my_pid == pid) {
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
    if (pid != INIT_PID) {
      return LIB_ERR_NOT_IMPLEMENTED;
    }
    debug_printf("Sending cap send req\n");
    errval_t err = aos_rpc_send_number_type(chan, 0, MSG_TYPE_CAP_REMOTE_SEND_REQ);
    if (err_is_fail(err))
    {
      return err;
    }
    debug_printf("waiting for ack\n");
    err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_CAP_REMOTE_SEND_ACK);
    if (err_is_fail(err))
    {
        return err;
    }
    debug_printf("sending cap\n");
    while (true)
    {
        err = lmp_chan_send5(&chan->chan, LMP_SEND_FLAGS_DEFAULT, cap_root, MSG_TYPE_CAP_REMOTE_SEND, get_cap_addr(cap), get_cap_level(cap),
            core, pid);
        if (lmp_err_is_transient(err))
        {
            DEBUG_ERR(err, "lmp_chan_send5: Transient error");
            continue;
        }
        else if (err_is_fail(err))
        {
            DEBUG_ERR(err, "lmp_chan_send5: Failed to send cap\n");
            return err;
        }
        else
        {
            break;
        }
    }
    debug_printf("waiting for ack\n");
    err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_CAP_REMOTE_SEND_ACK);
    if (err_is_fail(err))
    {
        return err;
    }
    debug_printf("cap succesfully sent\n");
    return SYS_ERR_OK;
}