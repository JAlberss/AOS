/**
 * \file
 * \brief Implementation of terminal related RPC functions
 */

#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <aos/aos_rpc_terminal.h>

errval_t aos_rpc_serial_getchar_nonblock(struct aos_rpc *chan, char *retc)
{
  errval_t err;
  //err = aos_rpc_send_number_type(chan, 0, MSG_TYPE_GET_CHAR_REQ);
  while ((err = event_dispatch_non_block(get_default_waitset())) != LIB_ERR_NO_EVENT)
  {
    if (!lmp_err_is_transient(err) && err_is_fail(err))
    {
      DEBUG_ERR(err, "cannot get default waitset\n");
      return err;
    }

    if (chan->msg->words[RPC_TYPE_SLOT] == MSG_TYPE_GET_CHAR_RESP)
    {
      *retc = chan->msg->words[RPC_GETCHAR_CHAR_FIELD];
      return SYS_ERR_OK;
    }
  }
  *retc = 0;
  return SYS_ERR_OK;
}

errval_t aos_rpc_serial_getchar(struct aos_rpc *chan, char *retc)
{
  errval_t err;
  err = aos_rpc_send_number_type(chan, 0, MSG_TYPE_GET_CHAR_REQ);
  if(err_is_fail(err)) {
    return err;
  }
  err = wait_for_msg(chan, get_default_waitset(), MSG_TYPE_GET_CHAR_RESP);
  if(err_is_fail(err)) {
    return err;
  }
  *retc = chan->msg->words[RPC_GETCHAR_CHAR_FIELD];
  return SYS_ERR_OK;
}

errval_t aos_rpc_serial_putchar(struct aos_rpc *chan, char c)
{
  return aos_rpc_send_number_type(chan, c, MSG_TYPE_PUT_CHAR);
}


errval_t aos_rpc_next_window(struct aos_rpc *chan)
{
  return aos_rpc_send_number_type(chan, WINDOW_NEXT, MSG_TYPE_TERMINAL_CTRL);
}

char aos_rpc_serial_putchar_receive(struct aos_rpc *chan)
{
  return (char)chan->msg->words[2];
}