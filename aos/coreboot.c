/*
 * Copyright (c) 2016, ETH Zurich.
 *
 * This file is distributed under the terms in the attached LICENSE file.  If
 * you do not find this file, copies can be found by writing to: ETH Zurich
 * D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
 */

#include <stdio.h>

#include <aos/aos.h>
#include <aos/kernel_cap_invocations.h>
#include <aos/coreboot.h>
#include <aos/aos_ump.h>
#include <elf/elf.h>
#include <spawn/multiboot.h>
#include <barrelfish_kpi/arm_core_data.h>
#include <barrelfish_kpi/asm_inlines_arch.h>

#define INIT_NAME "init"
extern struct bootinfo *bi;

errval_t spawn_core(char *binary_name, struct spawn_core_info *spawn_core_info)
{
    errval_t err;
    lvaddr_t elf_module_address = 0;
    lvaddr_t elf_out = 0;
    lvaddr_t core_data_lvaddr = 0;
    size_t retbytes;

    struct capref coredata_frame;    //Cap for target frame for the cloned arm_core_data
    struct capref urpc_frame;        //Cap for frame used for shared memory between cores used for ipc
    struct capref coredata_ram;      //Cap for Memory which gets retyped to ObjType_KernelControlBlock
    struct capref coredata_kcb;      //Cap for ObjType_KernelControlBlock after retype
    struct capref ret_frame;         //Frame for relocated ELF File
    struct capref init_memory_frame; //Frame for memory init using on early startup

    //Just the identitys of the allocated framed. Mainly used for cache clearing and referencing addreses Kernel Virtual
    struct frame_identity coredata_frame_id;
    struct frame_identity kcb_id;
    struct frame_identity urpc_frame_id;
    struct frame_identity init_frame_id;
    struct frame_identity elf_frame_id;
    struct frame_identity ret_frame_id;

    struct paging_state *st = get_current_paging_state();
    //Find Kernel Module
    struct mem_region *module = multiboot_find_module(bi, binary_name);
    if (module == NULL)
    {
        debug_printf("spawn_core >>> mem_region is NULL, could not find module\n");
        return SPAWN_ERR_FIND_MODULE;
    }
    const char *optstring = multiboot_module_opts(module);
    debug_printf("spawn_core >>> optstring is: %s\n", optstring);
    struct capref elf_frame = {
        .cnode = cnode_module,
        .slot = module->mrmod_slot,
    };
    //Map elf frame into user vspace to relocate
    err = paging_map_frame_attr(st, (void **)&elf_module_address, module->mrmod_size,
                                elf_frame, VREGION_FLAGS_READ, NULL, NULL);
    if (err_is_fail(err) || !IS_ELF(*(struct Elf32_Ehdr *)elf_module_address))
    {
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    err = frame_identify(elf_frame, &elf_frame_id);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error while identify elf source frame");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }

    //Target frame for relocated elf
    err = frame_alloc(&ret_frame, elf_frame_id.bytes, &retbytes);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error while allocate elf target frame");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    err = paging_map_frame_attr(st, (void **)&elf_out, retbytes, ret_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error while mapping elf target frame");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    err = frame_identify(ret_frame, &ret_frame_id);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error while identify elf target frame");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    memset((void *)elf_out, 0, retbytes);

    err = slot_alloc(&coredata_ram);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error while alloc slot for kcb ram cap");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }
    err = slot_alloc(&coredata_kcb);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error while alloc slot for actuall kcb cap");
        return err_push(err, SPAWN_ERR_MAP_MODULE);
    }

    err = ram_alloc(&coredata_ram, OBJSIZE_KCB);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in ram alloc kcb\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    err = cap_retype(coredata_kcb, coredata_ram, 0, ObjType_KernelControlBlock, OBJSIZE_KCB, 1);
    if (err_is_fail(err))
    {
        debug_printf("test\n");
        DEBUG_ERR(err, "error in ram retype to kcb\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    debug_printf("allocating core data frame\n");
    err = frame_alloc(&coredata_frame, sizeof(struct arm_core_data), &retbytes);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in frame_alloc\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    debug_printf("invoke_kcb_clone\n");
    err = frame_identify(coredata_frame, &coredata_frame_id);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in identify core_frame_id\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    //Mapping Frame which holds clone arm_core_data and zero...
    err = paging_map_frame_attr(st, (void **)&core_data_lvaddr, retbytes, coredata_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in paging_map_frame_attre\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    memset((void *)core_data_lvaddr, 0, retbytes);

    err = invoke_kcb_clone(coredata_kcb, coredata_frame);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in invoke_kcb_clone\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    err = frame_identify(coredata_kcb, &kcb_id);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in identify core_frame_id\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    spawn_core_info->arm_core_data = (struct arm_core_data *)core_data_lvaddr;
    err = load_cpu_relocatable_segment((void *)elf_module_address, (void *)(elf_out + BASE_PAGE_SIZE), ret_frame_id.base + BASE_PAGE_SIZE, spawn_core_info->arm_core_data->kernel_load_base, &spawn_core_info->arm_core_data->got_base);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in load_cpu_reloc_segment\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    debug_printf("map kcb\n");

    err = frame_alloc(&urpc_frame, MON_URPC_SIZE, &retbytes);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in frame_alloc\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    debug_printf("invoke_kcb_clone\n");

    err = frame_identify(urpc_frame, &urpc_frame_id);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in identify core_frame_id\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    lvaddr_t urpc_lvaddr;
    err = paging_map_frame_attr(st, (void **)&urpc_lvaddr, retbytes, urpc_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in paging_map_frame_attre\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }
    memset((void *)urpc_lvaddr, 0, MON_URPC_SIZE);
    err = serialise_urpc(urpc_lvaddr, urpc_frame_id.base, retbytes, spawn_core_info);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in urpc serialise\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    debug_printf("find module\n");

    struct mem_region *init = multiboot_find_module(bi, "init");
    //struct mem_region *multiboot_find_module(struct bootinfo *bi, const char *name)
    debug_printf("init base: %p, init size: %u\n", init->mr_base, init->mrmod_size);

    debug_printf("frame alloc init memory\n");

    err = frame_alloc(&init_memory_frame, ARM_CORE_DATA_PAGES * BASE_PAGE_SIZE, &retbytes);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in frame_alloc\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    debug_printf("identify frame for init mem\n");

    err = frame_identify(init_memory_frame, &init_frame_id);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in frame_identify\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    //arm_core_data->kernel_load_base = elf_frame_id.base;
    //arm_core_data->got_base = got_base;
    spawn_core_info->arm_core_data->monitor_module.mod_start = init->mr_base; //Address of the ELF file with the init module
    spawn_core_info->arm_core_data->monitor_module.mod_end = init->mr_base + init->mrmod_size;
    spawn_core_info->arm_core_data->kcb = (genpaddr_t)kcb_id.base;          //Location of KCB
    spawn_core_info->arm_core_data->memory_base_start = init_frame_id.base; //Initial Memory for init to use (stack, data and stuff)
    spawn_core_info->arm_core_data->memory_bytes = init_frame_id.bytes;     //Size of the initial init memory
    spawn_core_info->arm_core_data->urpc_frame_base = urpc_frame_id.base;   //URPC Frame adress
    spawn_core_info->arm_core_data->urpc_frame_size = urpc_frame_id.bytes;

    strcpy(spawn_core_info->arm_core_data->cmdline_buf, optstring);                             //cmdline for kernel which gets read from the menu.lst file
    size_t offset = core_data_lvaddr - (lvaddr_t)(spawn_core_info->arm_core_data->cmdline_buf); //Since its all kernel virtual we need the offset
    spawn_core_info->arm_core_data->cmdline = coredata_frame_id.base + offset;                  //Pointer to cmdline buffer
    const char *init_name = INIT_NAME;                                                          //Name of the init process. Just for display, which does not work anyway
    strcpy(spawn_core_info->arm_core_data->init_name, init_name);
    spawn_core_info->arm_core_data->init_name[strlen(init_name)] = '\0';

    debug_printf("clean cache and spawn core. See you in the bootloader\n");
    //Extensive flush of arm cache. There are two cache types poc and pou. While pou is on a per core basis poc is the better choise
    sys_armv7_cache_clean_poc((void *)((uint32_t)(init_frame_id.base)), (void *)((uint32_t)(init_frame_id.base + init_frame_id.bytes)));
    sys_armv7_cache_clean_poc((void *)((uint32_t)(coredata_frame_id.base)), (void *)((uint32_t)(coredata_frame_id.base + coredata_frame_id.bytes)));
    sys_armv7_cache_clean_poc((void *)((uint32_t)(elf_frame_id.base)), (void *)((uint32_t)(elf_frame_id.base + elf_frame_id.bytes)));
    sys_armv7_cache_clean_poc((void *)((uint32_t)(ret_frame_id.base)), (void *)((uint32_t)(ret_frame_id.base + ret_frame_id.bytes)));

    dmb();
    //Invalid date cache after flush to make sure the entries gets reread from memory
    sys_armv7_cache_invalidate((void *)((uint32_t)(init_frame_id.base)), (void *)((uint32_t)(init_frame_id.base + init_frame_id.bytes)));
    sys_armv7_cache_invalidate((void *)((uint32_t)(coredata_frame_id.base)), (void *)((uint32_t)(coredata_frame_id.base + coredata_frame_id.bytes)));
    sys_armv7_cache_invalidate((void *)((uint32_t)(elf_frame_id.base)), (void *)((uint32_t)(elf_frame_id.base + elf_frame_id.bytes)));
    sys_armv7_cache_invalidate((void *)((uint32_t)(ret_frame_id.base)), (void *)((uint32_t)(ret_frame_id.base + ret_frame_id.bytes)));
    dmb();
    err = invoke_monitor_spawn_core(spawn_core_info->coreid, CPU_ARM7, coredata_frame_id.base);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in invoke_monitor_spawn_core\n");
        return err_push(err, MON_ERR_SPAWN_CORE);
    }

    return SYS_ERR_OK;
}

errval_t serialise_urpc(lvaddr_t buf, lvaddr_t kv_buf, size_t size, struct spawn_core_info *spawn_core_info)
{
    debug_printf("serialize urpc: KV:%p UV:%p\n", kv_buf, buf);
    lvaddr_t start = buf;
    struct urpc_status *urpc_status = (struct urpc_status *)buf;
    spawn_core_info->urpc_status = urpc_status;

    buf += sizeof(struct urpc_status);
    urpc_status->bi = (kv_buf + (buf - start));

    // Getting physical address of module cap
    struct capref module_cap;
    module_cap.cnode = cnode_module;
    module_cap.slot = 0;
    errval_t err = frame_identify(module_cap, &(urpc_status->module_strings));
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Serialize urpc: frame identity fail\n");
        return err;
    }
    
    // Setup bootinfo struct in buf
    memcpy((void *)buf, bi, sizeof(struct bootinfo));
    buf += sizeof(struct bootinfo);

    // Copying over bootinfo mem regions
    for (int i = 0; i < bi->regions_length; i++)
    {
        memcpy((void *)buf, &bi->regions[i], sizeof(struct mem_region));
        buf += sizeof(struct mem_region);
    }

    //
    urpc_status->urpc = (kv_buf + (buf - start));
    spawn_core_info->urpc_frame = buf;
    struct urpc *urpc = (struct urpc *)buf;
    urpc->has_data = false;
    buf += sizeof(struct urpc);

    debug_printf("create urpc struct base: %p \n", urpc);
    urpc->buf = (char *)(kv_buf + (buf - start));
    spawn_core_info->urpc_frame_buf = buf;
    debug_printf("is %p", urpc->buf);
    urpc->buf_len = (size_t)((buf - start) - size);

    // set and initialize urpc ring buffers
    // receive buffer of new core == send buffer of this core
    urpc_status->urpc_recv_buf = kv_buf + (buf - start);
    spawn_core_info->urpc_send_buf = buf;
    memset((void*) buf, URPC_BUFFER_EMPTY, sizeof(struct urpc_slot) * URPC_NUM_SLOTS);
    buf += URPC_NUM_SLOTS * sizeof(struct urpc_slot);
    
    // send buffer of new core == recv buffer of this core
    urpc_status->urpc_send_buf = kv_buf + (buf - start);
    spawn_core_info->urpc_recv_buf = buf;    
    memset((void*) buf, URPC_BUFFER_EMPTY, sizeof(struct urpc_slot) * URPC_NUM_SLOTS);
    buf += URPC_NUM_SLOTS * sizeof(struct urpc_slot);

    

    return SYS_ERR_OK;
}

errval_t
load_cpu_relocatable_segment(void *elfdata, void *out, lvaddr_t vbase,
                             lvaddr_t text_base, lvaddr_t *got_base)
{
    /* Find the full loadable segment, as it contains the dynamic table. */
    struct Elf32_Phdr *phdr_full = elf32_find_segment_type(elfdata, PT_LOAD);

    if (!phdr_full)
        return ELF_ERR_HEADER;

    void *full_segment_data = elfdata + phdr_full->p_offset;

    /* Find the relocatable segment to load. */
    struct Elf32_Phdr *phdr = elf32_find_segment_type(elfdata, PT_BF_RELOC);
    if (!phdr)
        return ELF_ERR_HEADER;

    /* Copy the raw segment data. */
    void *in = elfdata + phdr->p_offset;
    assert(phdr->p_filesz <= phdr->p_memsz);
    memcpy(out, in, phdr->p_filesz);

    /* Find the dynamic segment. */
    struct Elf32_Phdr *phdr_dyn = elf32_find_segment_type(elfdata, PT_DYNAMIC);
    if (!phdr_dyn)
        return ELF_ERR_HEADER;

    /* The location of the dynamic segment is specified by its *virtual
     * address* (vaddr), relative to the loadable segment, and *not* by its
     * p_offset, as for every other segment. */
    struct Elf32_Dyn *dyn =
        full_segment_data + (phdr_dyn->p_vaddr - phdr_full->p_vaddr);
    /* There is no *entsize field for dynamic entries. */
    size_t n_dyn = phdr_dyn->p_filesz / sizeof(struct Elf32_Dyn);
    // dyn -= 3*sizeof(struct Elf32_Dyn);
    /* Find the relocations (REL only). */
    void *rel_base = NULL;
    size_t relsz = 0, relent = 0;
    void *dynsym_base = NULL;
    const char *dynstr_base = NULL;
    size_t syment = 0, strsz = 0;
    for (size_t i = 0; i < n_dyn; i++)
    {
        //, DT_RELA : % d, DT_REL : % d DT_RELSZ : % d, DT_RELENT : % d DT_SYMTAB : % d DT_SYMEMT : % d, DT_STRTAB : % d, DT_STRSZ : % d\n ",dyn[i].d_tag,DT_RELA,DT_REL,DT_RELSZ,DT_RELENT,DT_SYMTAB,DT_SYMENT,DT_STRTAB,DT_STRSZ);
        switch (dyn[i].d_tag)
        {
        /* There shouldn't be any RELA relocations. */
        case DT_RELA:
            return ELF_ERR_HEADER;

        case DT_REL:
            if (rel_base != NULL)
                return ELF_ERR_HEADER;

            /* Pointers in the DYNAMIC table are *virtual* addresses,
                 * relative to the vaddr base of the segment that contains
                 * them. */
            rel_base = full_segment_data +
                       (dyn[i].d_un.d_ptr - phdr_full->p_vaddr);
            break;

        case DT_RELSZ:
            relsz = dyn[i].d_un.d_val;
            break;

        case DT_RELENT:
            relent = dyn[i].d_un.d_val;
            break;

        case DT_SYMTAB:
            dynsym_base = full_segment_data +
                          (dyn[i].d_un.d_ptr - phdr_full->p_vaddr);
            break;

        case DT_SYMENT:
            syment = dyn[i].d_un.d_val;
            break;

        case DT_STRTAB:
            dynstr_base = full_segment_data +
                          (dyn[i].d_un.d_ptr - phdr_full->p_vaddr);
            break;

        case DT_STRSZ:
            strsz = dyn[i].d_un.d_val;
        }
    }
    if (rel_base == NULL || relsz == 0 || relent == 0 ||
        dynsym_base == NULL || syment == 0 ||
        dynstr_base == NULL || strsz == 0)
        return ELF_ERR_HEADER;

    /* XXX - The dynamic segment doesn't actually tell us the size of the
     * dynamic symbol table, which is very annoying.  We should fix this by
     * defining and implementing a standard format for dynamic executables on
     * Barrelfish, using DT_PLTGOT.  Currently, GNU ld refuses to generate
     * that for the CPU driver binary. */
    assert((size_t)dynstr_base > (size_t)dynsym_base);
    size_t dynsym_len = (size_t)dynstr_base - (size_t)dynsym_base;

    /* Walk the symbol table to find got_base. */
    size_t dynsym_offset = 0;
    struct Elf32_Sym *got_sym = NULL;
    debug_printf("search got\n");
    while (dynsym_offset < dynsym_len)
    {
        got_sym = dynsym_base + dynsym_offset;
        if (!strcmp(dynstr_base + got_sym->st_name, "got_base"))
            break;

        dynsym_offset += syment;
    }
    if (dynsym_offset >= dynsym_len)
    {
        return ELF_ERR_HEADER;
    }

    /* Addresses in the relocatable segment are relocated to the
     * newly-allocated region, relative to their addresses in the relocatable
     * segment.  Addresses outside the relocatable segment are relocated to
     * the shared text segment, relative to their position in the
     * originally-loaded segment. */
    uint32_t relocatable_offset = vbase - phdr->p_vaddr;
    uint32_t text_offset = text_base - phdr_full->p_vaddr;

    /* Relocate the got_base within the relocatable segment. */
    *got_base = vbase + (got_sym->st_value - phdr->p_vaddr);

    /* Process the relocations. */
    size_t n_rel = relsz / relent;
    debug_printf("Have %zu relocations of size %zu\n", n_rel, relent);
    for (size_t i = 0; i < n_rel; i++)
    {
        struct Elf32_Rel *rel = rel_base + i * relent;

        size_t sym = ELF32_R_SYM(rel->r_info);
        size_t type = ELF32_R_TYPE(rel->r_info);

        /* We should only see relative relocations (R_ARM_RELATIVE) against
         * sections (symbol 0). */
        if (sym != 0 || type != R_ARM_RELATIVE)
            return ELF_ERR_HEADER;

        uint32_t offset_in_seg = rel->r_offset - phdr->p_vaddr;
        uint32_t *value = out + offset_in_seg;

        uint32_t offset;
        if (*value >= phdr->p_vaddr &&
            (*value - phdr->p_vaddr) < phdr->p_memsz)
        {
            /* We have a relocation to an address *inside* the relocatable
             * segment. */
            offset = relocatable_offset;
            // debug_printf("r ");
        }
        else
        {
            /* We have a relocation to an address in the shared text segment.
             * */
            offset = text_offset;
            //  debug_printf("t ");
        }

        // debug_printf("REL@%08"PRIx32" %08"PRIx32" -> %08"PRIx32"\n",
        //       rel->r_offset, *value, *value + offset);
        *value += offset;
    }

    return SYS_ERR_OK;
}
