/**
 * \file
 * \brief Barrelfish library initialization.
 */

/*
 * Copyright (c) 2007-2016, ETH Zurich.
 * Copyright (c) 2014, HP Labs.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, CAB F.78, Universitaetstr. 6, CH-8092 Zurich,
 * Attn: Systems Group.
 */

#include <stdio.h>
#include <aos/aos.h>
#include <aos/dispatch.h>
#include <aos/curdispatcher_arch.h>
#include <aos/dispatcher_arch.h>
#include <barrelfish_kpi/dispatcher_shared.h>
#include <aos/morecore.h>
#include <aos/paging.h>
#include <barrelfish_kpi/domain_params.h>
#include <aos/aos_rpc.h>
#include <aos/aos_ump.h>
#include "threads_priv.h"
#include "init.h"

#define SERIAL_USERLAND_WORKS 1

int threadid = 0;


/// Are we the init domain (and thus need to take some special paths)?
static bool init_domain;
extern size_t (*_libc_terminal_read_func)(char *, size_t);
extern size_t (*_libc_terminal_write_func)(const char *, size_t);
extern void (*_libc_exit_func)(int);
extern void (*_libc_assert_func)(const char *, const char *, const char *, int);

void libc_exit(int);

void libc_exit(int status)
{
    // Use spawnd if spawned through spawnd
    if (disp_get_domain_id() == 0)
    {
        errval_t err = cap_revoke(cap_dispatcher);
        if (err_is_fail(err))
        {
            sys_print("revoking dispatcher failed in _Exit, spinning!", 100);
            while (1)
            {
            }
        }
        err = cap_delete(cap_dispatcher);
        sys_print("deleting dispatcher failed in _Exit, spinning!", 100);

        // XXX: Leak all other domain allocations
    }
    else
    {
        //debug_printf("libc_exit NYI!\n");
    }

    thread_exit(status);
    // If we're not dead by now, we wait
    while (1)
    {
    }
}

static void libc_assert(const char *expression, const char *file,
                        const char *function, int line)
{
    char buf[512];
    size_t len;

    /* Formatting as per suggestion in C99 spec 7.2.1.1 */
    len = snprintf(buf, sizeof(buf), "Assertion failed on core %d in %.*s: %s,"
                                     " function %s, file %s, line %d.\n",
                   disp_get_core_id(), DISP_NAME_LEN,
                   disp_name(), expression, function, file, line);
    sys_print(buf, len < sizeof(buf) ? len : sizeof(buf));
}

__attribute__((__unused__)) static size_t syscall_terminal_write(const char *buf, size_t len)
{
    if (len)
    {
        return sys_print(buf, len);
    }
    return 0;
}

bool is_break_char(char c);
bool is_break_char(char c)
{
    switch (c)
    {
    case 10: //New line
    case 13: //Carriage return
    case 4:  //EOF
        return true;
    default:
        return false;
    }
}

static size_t aos_terminal_read(char *buf, size_t len)
{
    if(init_domain){
      return sys_getchar(buf);
    }
    struct aos_rpc *serial_chan = aos_rpc_get_serial_channel();
    if (serial_chan == NULL || serial_chan->chan.connstate != LMP_CONNECTED)
    {
        return -1;
    }
    size_t i;
    for (i = 0; i < len; i++)
    {
        aos_rpc_serial_getchar(serial_chan, &buf[i]);
        break;        //TODO FIX Should not break after every char but then we can make use of linenoise early
        if (is_break_char(buf[i]) || buf[i] == 0)
        {
            break;
        }
    }
    return i+1;
}

static size_t aos_terminal_write(const char *buf, size_t len)
{
    if(init_domain){
         return syscall_terminal_write(buf, len);
    }
    struct aos_rpc *serial_chan = aos_rpc_get_serial_channel(); //aos_rpc_get_serial_channel();

    if (serial_chan == NULL || serial_chan->chan.connstate != LMP_CONNECTED)
    {
        return -1;
    }
    for (size_t i = 0; i < len; i++)
    {
        aos_rpc_serial_putchar(serial_chan, buf[i]);
    }
    return len;
}

/* Set libc function pointers */
void barrelfish_libc_glue_init(void)
{
    // XXX: FIXME: Check whether we can use the proper kernel serial, and
    // what we need for that
    // TODO: change these to use the user-space serial driver if possible
    _libc_terminal_read_func = aos_terminal_read;
    _libc_terminal_write_func = aos_terminal_write; //syscall_terminal_write;
    _libc_exit_func = libc_exit;
    _libc_assert_func = libc_assert;
    /* morecore func is setup by morecore_init() */

    // XXX: set a static buffer for stdout
    // this avoids an implicit call to malloc() on the first printf
    static char buf[BUFSIZ];
    setvbuf(stdout, buf, _IOLBF, sizeof(buf));
    static char ebuf[BUFSIZ];
    setvbuf(stderr, ebuf, _IOLBF, sizeof(buf));
}

errval_t recv_handler(void *arg);
errval_t recv_handler(void *arg)
{
    errval_t err;
    struct lmp_chan *lc = arg;
    struct lmp_recv_msg msg = LMP_RECV_MSG_INIT;
    struct capref cap;
    err = lmp_chan_recv(lc, &msg, &cap);
    if (err_is_fail(err) && lmp_err_is_transient(err))
    {
        debug_printf("re register\n");
        err = lmp_chan_register_recv(lc, get_default_waitset(), MKCLOSURE((void *)recv_handler, (void *)arg));
    }
    debug_printf("msg buflen %zu\n", msg.buf.msglen);
    debug_printf("msg->words[RPC_TYPE_SLOT] = 0x%lx\n", msg.words[RPC_TYPE_SLOT]);
    lmp_chan_register_recv(lc, get_default_waitset(), MKCLOSURE((void *)recv_handler, (void *)arg));
    return SYS_ERR_OK;
}

void ep_recv_handler(void *arg);
void ep_recv_handler(void *arg)
{
    return;
}

/** \brief Initialise libbarrelfish.
 *
 * This runs on a thread in every domain, after the dispatcher is setup but
 * before main() runs.
 */
errval_t barrelfish_init_onthread(struct spawn_domain_params *params)
{
    errval_t err;

    set_init_rpc(NULL);
    // do we have an environment?
    if (params != NULL && params->envp[0] != NULL)
    {
        extern char **environ;
        environ = params->envp;
    }

    // Init default waitset for this dispatcher
    struct waitset *default_ws = get_default_waitset();
    waitset_init(default_ws);

    // Initialize ram_alloc state
    ram_alloc_init();
    /* All domains use smallcn to initialize */
    err = ram_alloc_set(ram_alloc_fixed);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_RAM_ALLOC_SET);
    }

    err = paging_init();
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_VSPACE_INIT);
    }

    if (params->vspace_buf_len > 0 && !init_domain)
    {
        struct paging_state *st = get_current_paging_state();
        st->head = (struct l2_page_state *)params->vspace_buf;
    }
    char *rpc_init_buf = params->vspace_buf + params->vspace_buf_len;
    err = slot_alloc_init();
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_SLOT_ALLOC_INIT);
    }

    err = morecore_init();
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_MORECORE_INIT);
    }

    lmp_endpoint_init();

    // init domains only get partial init
    if (init_domain)
    {
        return SYS_ERR_OK;
    }

    /* allocate lmp channel structure */
    struct lmp_chan channel;
    lmp_chan_init(&channel);
    /* create local endpoint */
    struct capref *endpoint_cap = &channel.local_cap;
    struct lmp_endpoint **local_endpoint = &channel.endpoint;
    err = endpoint_create(LMP_RECV_LENGTH, endpoint_cap, local_endpoint);
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_ENDPOINT_CREATE);
    }

    /* set remote endpoint to init's endpoint */
    channel.remote_cap = cap_initep;

    // debug_printf("set remote_ep with %d selfep: %d addr: %p\n",get_croot_addr(cap_initep),get_croot_addr(cap_selfep),get_cap_addr(cap_initep));
    // /* set receive handler */
    err = lmp_chan_register_recv(&channel, default_ws, MKCLOSURE((void *)ep_recv_handler, NULL));
    if (err_is_fail(err))
    {
        return err_push(err, LIB_ERR_CHAN_REGISTER_RECV);
    }

    /* send local ep to init */
    err = lmp_ep_send0(cap_initep, LMP_SEND_FLAGS_DEFAULT, *endpoint_cap);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in lmp_ep_send0");
        return err;
    }

    // /* wait for init to acknowledge receiving the endpoint */
    err = event_dispatch(default_ws);
    channel.connstate = LMP_CONNECTED;
    /* initialize init RPC client with lmp channel */

    struct aos_rpc *rpc_client = (struct aos_rpc *)rpc_init_buf;
    rpc_init_buf = rpc_init_buf + sizeof(struct aos_rpc);

    err = aos_rpc_alloc(rpc_client, &channel, true);
    if (err_is_fail(err))
    {
        return err;
    }
    rpc_client->msg = (struct lmp_recv_msg *)rpc_init_buf;
    rpc_init_buf = rpc_init_buf + sizeof(struct lmp_recv_msg);
    /* set init RPC client in our program state */
    set_init_rpc(rpc_client);
    ram_alloc_set(NULL);
    err = lmp_chan_register_recv(&rpc_client->chan, get_default_waitset(), MKCLOSURE(msg_recv_handler, rpc_client));
    if (lmp_err_is_transient(err))
    {
        DEBUG_ERR(err, "Lmp chan err is transient");
    }
    else if (err_is_fail(err))
    {
        DEBUG_ERR(err, "lmp chan cannot registern\n");
        return err;
    }
    err = aos_rpc_send_number_type(rpc_client, 0, MSG_TYPE_CHILD_SPAWNED);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Error in sending MSG_TYPE_CHILD_SPAWNED");
        USER_PANIC("Not able to spawn child");
    }

    err = aos_set_page_fault_handler();
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "Something went wrong when setting the page fault");
        return err;
    }

    // dump_dispatcher(get_dispatcher_shared_generic(curdispatcher()));
    return SYS_ERR_OK;
}

/**
 *  \brief Initialise libbarrelfish, while disabled.
 *
 * This runs on the dispatcher's stack, while disabled, before the dispatcher is
 * setup. We can't call anything that needs to be enabled (ie. cap invocations)
 * or uses threads. This is called from crt0.
 */
void barrelfish_init_disabled(dispatcher_handle_t handle, bool init_dom_arg);
void barrelfish_init_disabled(dispatcher_handle_t handle, bool init_dom_arg)
{
    init_domain = init_dom_arg;
    disp_init_disabled(handle);
    thread_init_disabled(handle, init_dom_arg);
}


