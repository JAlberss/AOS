/**
 * \file
 * \brief AOS paging helpers.
 */

/*
 * Copyright (c) 2012, 2013, 2016, ETH Zurich.
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached LICENSE file.
 * If you do not find this file, copies can be found by writing to:
 * ETH Zurich D-INFK, Universitaetstr. 6, CH-8092 Zurich. Attn: Systems Group.
 */

#include <aos/aos.h>
#include <aos/paging.h>
#include <aos/except.h>
#include <aos/slab.h>
#include "threads_priv.h"
#include <bitmap.h>

#include <stdio.h>
#include <string.h>
static struct paging_state current;
//static char l2buf[sizeof(struct l2_page_state)*65];
bool refilling = false;
/**
 * \brief Helper function that allocates a slot and
 *        creates a ARM l2 page table capability
 */
__attribute__((unused)) static errval_t arml2_alloc(struct paging_state *st, struct capref *ret)
{
    errval_t err;
    err = st->slot_alloc->alloc(st->slot_alloc, ret);
    if (err_is_fail(err))
    {
        debug_printf("slot_alloc failed: %s\n", err_getstring(err));
        return err;
    }

    err = vnode_create(*ret, ObjType_VNode_ARM_l2);
    if (err_is_fail(err))
    {
        debug_printf("vnode_create failed: %s\n", err_getstring(err));
        return err;
    }
    return SYS_ERR_OK;
}

errval_t paging_init_state(struct paging_state *st, lvaddr_t start_vaddr,
                           struct capref pdir, struct slot_allocator *ca)
{
    //errval_t err;
    //debug_printf("paging_init_state %p\n", start_vaddr);
    // TODO (M4): Implement page fault handler that installs frames when a page fault
    // occurs and keeps track of the virtual address space.
    thread_mutex_init(&st->mutex);
    st->slot_alloc = ca;
    //st->l1_pagetable.cnode = cnode_page;
    //st->l1_pagetable.slot = 0;
    st->l1_pagetable = pdir;
    //debug_printf("set head in paging init state\n");
    st->head = NULL;
    st->base_addr = start_vaddr;
    st->slab_refill_count = 0;
    //debug_printf("\npdir.root %u, \npdir.cnode %u,\npdir.level %u\n", pdir.cnode.croot, pdir.cnode.cnode, pdir.cnode.level);
    slab_init(&st->slabs, sizeof(struct l2_page_state), &slab_refill_paging);
    slab_grow(&st->slabs, &st->init_l2_pages, sizeof(st->init_l2_pages));

    slab_init(&st->mapping_slabs, sizeof(struct l2_frame_mappings), &slab_refill_mappings);
    slab_grow(&st->mapping_slabs, &st->init_l2_frame_mappings, sizeof(st->init_l2_frame_mappings));
    //debug_printf("return from paging_init_state\n");

    //Init start frame
    //void *buf;
    //debug_printf("init first head\n");
    /*err = paging_alloc(st, &buf, BASE_PAGE_SIZE);
    if(err_is_fail(err)) {
        return err;
    }*/
    return SYS_ERR_OK;
}

errval_t slab_refill_paging(struct slab_allocator *slab)
{
    //debug_printf("slab_refill_paging need: %d\n", (PAGING_SLAB_REFILL_COUNT(current.slab_refill_count) + PAGING_SLAB_BUFFER));
    struct capref frame;
    refilling = true;
    errval_t err = slab_refill_no_pagefault(slab, frame, (PAGING_SLAB_REFILL_COUNT(current.slab_refill_count) + PAGING_SLAB_BUFFER) * sizeof(struct l2_page_state));
    refilling = false;
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in paging_init_state");
        return (err);
    }
    return SYS_ERR_OK;
}

errval_t slab_refill_mappings(struct slab_allocator *slab)
{
    //debug_printf("slab_refill_mappings need: %d\n", (PAGING_SLAB_REFILL_COUNT(current.mapping_refill_count) + PAGING_SLAB_BUFFER));
    struct capref frame;
    refilling = true;
    errval_t err = slab_refill_no_pagefault(slab, frame, (PAGING_SLAB_REFILL_COUNT(current.mapping_refill_count) + PAGING_SLAB_BUFFER) * sizeof(struct l2_frame_mappings));
    refilling = false;
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in paging_init_state");
        return (err);
    }
    return SYS_ERR_OK;
}

/**
 * \brief This function initializes the paging for this domain
 * It is called once before main.
 */
errval_t paging_init(void)
{
    //debug_printf("paging_init %d\n",sizeof(struct l2_page_state ));
    // TODO (M2): Call paging_init_state for &current
    // TODO (M4): initialize self-paging handler
    // TIP: use thread_set_exception_handler() to setup a page fault handler
    // TIP: Think about the fact that later on, you'll have to make sure that
    // you can handle page faults in any thread of a domain.
    // TIP: it might be a good idea to call paging_init_state() from here to
    // avoid code duplication.

    set_current_paging_state(&current);

    struct capref pdir = {
        .cnode = cnode_page,
        .slot = 0,
    };
    errval_t err = paging_init_state(&current, VADDR_OFFSET, pdir, get_default_slot_allocator());
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in paging_init_state");
        return (err);
    }

    return SYS_ERR_OK;
}

/**
 * \brief Initialize per-thread paging state
 */
void paging_init_onthread(struct thread *t)
{
    // TODO (M4): setup exception handler for thread `t'.
    //debug_printf("XXXXXXX in paging_init_onthread XXXXXX\n");
    struct paging_state *st = get_current_paging_state();
    errval_t err;
    //debug_printf("mapping guard page\n");
    lvaddr_t stack = THREAD_STACK_GUARD((lvaddr_t)t->stack);
    struct capref guard_frame;
    size_t retbytes = 0;
    err = frame_alloc(&guard_frame, BASE_PAGE_SIZE, &retbytes); // TODO: only do this once
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "Can't alloc frame");
    }
    err = paging_map_fixed_attr(st, stack, guard_frame, retbytes, VREGION_FLAGS_READ);
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "Can't map guard");
    }

    // debug_printf("setting page fault handler\n");

    //Setting up stack for the page fault handler
    //set page fault handler
    lvaddr_t *exception_stack_base = NULL;

    struct capref frame_cap;
    size_t bytes = 0;
    //  size_t retbytes = 0;
    // in the init domain frame_alloc uses ram_alloc_fixed, which can only alloc memory in chunks of
    // size BASE_PAGE_SIZE. Therefore we have to and allocate and map the stack in a loop.
    err = frame_alloc(&frame_cap, 4 * BASE_PAGE_SIZE, &bytes);
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "No frame alloc'd\n");
    }
    // debug_printf("We received a frame in %s\n", __FUNCTION__);
    err = paging_map_frame_attr(st, (void **)&exception_stack_base, bytes, frame_cap,
                                VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "No mem");
    }

    //debug_printf("got frame: 0x%" PRIxGENPADDR " mapped at %p with %d\n", id.base, stack_base,retbytes);

    memset((char *)exception_stack_base, 0, BASE_PAGE_SIZE);

    void *exception_stack_top = (char *)exception_stack_base + bytes;
    //debug_printf("We have mapped our stack into the stack_base at %p stack_top: %p\n",
    //	 exception_stack_base, exception_stack_top);

    //debug_printf("setting page fault handler %p with %p to %p (%d->%d)\n", aos_page_fault_handler,
    //	 exception_stack_base, exception_stack_top, ARM_L2_OFFSET(exception_stack_base),
    //	 ARM_L2_OFFSET(exception_stack_top));

    t->exception_handler = aos_page_fault_handler;

    if (exception_stack_base != NULL && exception_stack_top != NULL)
    {
        t->exception_stack = (void *)exception_stack_base;
        t->exception_stack_top = (void *)exception_stack_top;
    }
}

/**
 * \brief return a pointer to a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 */
errval_t paging_region_init(struct paging_state *st, struct paging_region *pr, size_t size)
{
    void *base;
    errval_t err = paging_alloc(st, &base, size);
    if (err_is_fail(err))
    {
        debug_printf("paging_region_init: paging_alloc failed\n");
        return err_push(err, LIB_ERR_VSPACE_MMU_AWARE_INIT);
    }
    pr->base_addr = (lvaddr_t)base;
    pr->current_addr = pr->base_addr;
    pr->region_size = size;
    pr->next = NULL;
    return SYS_ERR_OK;
}

/**
 * \brief return a pointer to a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 */
errval_t paging_region_map(struct paging_region *pr, size_t req_size,
                           void **retbuf, size_t *ret_size)
{
    lvaddr_t end_addr = pr->base_addr + pr->region_size;
    ssize_t rem = end_addr - pr->current_addr;
    if (rem > req_size)
    {
        // ok
        *retbuf = (void *)pr->current_addr;
        *ret_size = req_size;
        pr->current_addr += req_size;
    }
    else if (rem > 0)
    {
        *retbuf = (void *)pr->current_addr;
        *ret_size = rem;
        pr->current_addr += rem;
        debug_printf("exhausted paging region, "
                     "expect badness on next allocation\n");
    }
    else
    {
        return LIB_ERR_VSPACE_MMU_AWARE_NO_SPACE;
    }
    return SYS_ERR_OK;
}

/**
 * \brief free a bit of the paging region `pr`.
 * This function gets used in some of the code that is responsible
 * for allocating Frame (and other) capabilities.
 * NOTE: Implementing this function is optional.
 */
errval_t paging_region_unmap(struct paging_region *pr, lvaddr_t base, size_t bytes)
{
    // TIP: you will need to keep track of possible holes in the region
    return SYS_ERR_OK;
}

/**
 *
 * \brief Find a bit of free virtual address space that is large enough to
 *        accomodate a buffer of size `bytes`.
 */
errval_t paging_alloc(struct paging_state *st, void **buf, size_t bytes)
{

    struct l2_page_state *curr = NULL;
    uint32_t freebits = 0;
    uint32_t l2_entries_needed = DIVIDE_ROUND_UP(bytes, BASE_PAGE_SIZE);
    struct l2_page_state *start = NULL;
    errval_t err;

    //Calculate how many slabs are needed to fullfill request and make sure sufficent space is available.
    //This prevents curcular dependencies while create the mapping structs
    size_t slabs_needed_with_refill = DIVIDE_ROUND_UP(bytes, ARM_L2_MAX_CAPACITY);
    slabs_needed_with_refill += DIVIDE_ROUND_UP(slabs_needed_with_refill, ARM_L2_MAX_CAPACITY);
    if ((slab_freecount(&st->slabs) < slabs_needed_with_refill + 2) && !refilling)
    {
        st->slab_refill_count = slabs_needed_with_refill;
        st->slabs.refill_func(&st->slabs);
    }

    //debug_printf("start paging alloc at addr %p %d %d\n", st->base_addr, ARM_L1_OFFSET(st->base_addr), ARM_L2_OFFSET(st->base_addr));
    //debug_printf("try to paging_alloc %u bytes on %p\n", bytes, st->base_addr);
    //Start to search at base Adress
    *buf = (void *)st->base_addr;
    //First case. No existing mappings at the moment. Start at base_addr
    if (!st->head)
    {
        //debug_printf("adding new head l2 table while paging_alloc\n");
        err = init_l2_page_state(st, &st->head, ARM_L1_OFFSET(st->base_addr), 0, 0);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "init_l2_page_state error in get head");
            return err;
        }
    }

    for (start = st->head; start->next != NULL; start = start->next)
    {
        if (start->l1_index == ARM_L1_OFFSET(*buf))
        {
            break;
        }
    }
    //Last element in list has not the correct l1 index. Create new one
    if (start->next == NULL && start->l1_index != ARM_L1_OFFSET(*buf))
    {
        //debug_printf("Last element in list has not the correct l1 index ((last)%d != %d(search)). Create new one\n",start->l1_index,ARM_L1_OFFSET(*buf));
        err = init_l2_page_state(st, &start->next, ARM_L1_OFFSET(*buf), 0, 0);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "init_l2_page_state error in get new");
            return err;
        }
        start = start->next;
    }
    //Now find l2_entries_needed free pages in a row
    // debug_printf("start to find free %p\n",start);
    for (curr = start; curr != NULL; curr = curr->next)
    {
        //Workaround for unlikly case that a slab refill (of mm) happens for the request of a ram cap for a arm l2 pagetable
        //This results in the case that the already created struct is used by paging_refill_no_pagefault without beeing completly valid by now
        //Setting the l2_alloc_ongoing flag while we make a arm_l2_alloc and checking for null cap fix this
        if (st->l2_alloc_ongoing && capref_is_null(curr->l2_cap))
        {
            continue;
        }
        for (int i = 0; i < ARM_L2_MAX_ENTRIES; i++)
        {
                if (!CHECK_BIT(curr->map, i))
                {
                    freebits++;
                    if (freebits == l2_entries_needed)
                    {
                        break;
                    }
            }
            else
            {
                *buf += (freebits + 1) * BASE_PAGE_SIZE;
                freebits = 0;
            }
        }
        if (freebits == l2_entries_needed)
        {
            break;
        }

        err = create_l2_if_not_exists(st,&curr->next, curr->l1_index + 1);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "init_l2_page_state error in get new / append");
            return err;
        }
    }
    set_l2_pages(st, *buf, freebits);
    return SYS_ERR_OK;
}

errval_t create_l2_if_not_exists(struct paging_state *st,struct l2_page_state * *state, uint32_t l1_index)
{
    errval_t err;
    if (*state == NULL)
    {
        err = init_l2_page_state(st, state, l1_index, 0, 0);
        if (err_is_fail(err))
        {
            return err;
        }
    }
    return SYS_ERR_OK;
}

errval_t set_l2_pages(struct paging_state *st, lvaddr_t *addr, size_t times)
{
    uint32_t l1_idx = ARM_L1_OFFSET(addr);
    uint32_t l2_idx = ARM_L2_OFFSET(addr);
    if (st->head == NULL)
    {
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
    struct l2_page_state *curr;
    for (curr = st->head; curr->l1_index != l1_idx; curr = curr->next)
        ;

    for (int i = 0; i < times; i++)
    {
        SET_BIT(curr->map, l2_idx);
        l2_idx++;
        if ((l2_idx % ARM_L2_MAX_ENTRIES) == 0)
        {
            l2_idx = 0;
            if (curr->next == NULL)
            {
                return LIB_ERR_SHOULD_NOT_GET_HERE;
            }
            else
            {
                curr = curr->next;
            }
        }
    }
    // debug_printf("return set_l2_pages\n");
    return SYS_ERR_OK;
}

/**
 * \brief map a user provided frame, and return the VA of the mapped
 *        frame in `buf`.
 */
errval_t paging_map_frame_attr(struct paging_state *st, void **buf,
                               size_t bytes, struct capref frame,
                               int flags, void *arg1, void *arg2)
{
    assert(bytes > 0);
    errval_t err = paging_alloc(st, buf, bytes);

    if (err_is_fail(err))
    {
        return err;
    }
    return paging_map_fixed_attr(st, (lvaddr_t)(*buf), frame, bytes, flags);
}

errval_t
slab_refill_no_pagefault(struct slab_allocator *slabs, struct capref frame, size_t minbytes)
{
    //debug_printf("slab_refill_no_pagefault %d bytes\n",minbytes);
    errval_t err;
    size_t retbytes;
    err = frame_alloc(&frame, minbytes, &retbytes);
    if (err_is_fail(err))
    {
        // debug_printf("err: %u\n", err);
        DEBUG_ERR(err, "in slab_refill_no_pagefault->frame_alloc\n");
        return err;
    }
    lvaddr_t addr;
    err = paging_map_frame_attr(&current, (void **)&addr, retbytes, frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        return err;
    }
    slab_grow(slabs, (void *)addr, retbytes);
    //debug_printf("refilling done\n");
    // Refill the two-level slot allocator without causing a page-fault
    return SYS_ERR_OK;
}

errval_t init_l2_page_state(struct paging_state *st, struct l2_page_state **dest, capaddr_t l1_index, capaddr_t l2_index, size_t pte_count)
{
    //debug_printf("slabs left: %d\n",slab_freecount(&st->slabs));
    struct l2_page_state *old_head = NULL;
    //debug_printf("get new l2_page for     %d slabs left: %d\n", l1_index, slab_freecount(&st->slabs));
    struct l2_page_state *next = NULL;
    struct l2_page_state *prev = NULL;
    if (*dest && ((*dest)->l1_index > l1_index))
    {
        old_head = st->head;
        st->head = NULL;
    }
    else if (*dest)
    {
        while (*dest)
        {
            //debug_printf("dest l1 is: %d search l1 is: %d\n", (*dest)->l1_index, l1_index);
            if ((*dest)->l1_index == l1_index)
            {
                //debug_printf("found ret: %p %d\n",*dest,l1_index);
                return SYS_ERR_OK;
            }
            if ((*dest)->next == NULL)
            {
                prev = *dest;
                break;
            }
            if ((*dest)->l1_index < l1_index && (*dest)->next->l1_index > l1_index)
            {
                prev = *dest;
                next = (*dest)->next;
                break;
            }
            *dest = (*dest)->next;
        }
    }
    else
    {
        next = NULL;
    }
    //debug_printf("create new one with %d\n", l1_index);
    *dest = slab_alloc(&st->slabs);
    if (dest == NULL)
    {
        return -1;
    }
    if (st->head == NULL)
    {
        //debug_printf("set head 1\n");
        st->head = *dest;
        if (old_head)
        {
            st->head->next = old_head;
        }
    }
    if (prev)
    {
        prev->next = *dest;
    }
    (*dest)->next = next;
    (*dest)->l1_index = l1_index;
    (*dest)->l1_l2_mapping = NULL_CAP;
    (*dest)->l2_cap = NULL_CAP;
    (*dest)->l2_frame_mappings = NULL;
    (*dest)->l2_frame_mappings_end = NULL;
    //debug_printf("set map to 0 and l1\n");
    for (uint32_t i = 0; i < (sizeof((*dest)->map) / sizeof(*(*dest)->map)); i++)
    {
        (*dest)->map[i] = 0;
    }
    //debug_printf("return from init_l2_page_state\n");
    return SYS_ERR_OK;
}

/**
 * \brief map a user provided frame at user provided VA.
 * TODO(M1): Map a frame assuming all mappings will fit into one L2 pt
 * TODO(M2): General case
 */
errval_t paging_map_fixed_attr(struct paging_state *st, lvaddr_t vaddr,
                               struct capref frame, size_t bytes, int flags)
{
    size_t pte_count = DIVIDE_ROUND_UP(bytes, BASE_PAGE_SIZE);
    //debug_printf("Paging map fixed attr. Addr: %p(%d->%d), bytes: %u\n", vaddr, ARM_L1_OFFSET(vaddr), ARM_L2_OFFSET(vaddr), bytes);

    // lvaddr_t save = st->base_addr;
    size_t offset = 0;
    errval_t err;
    // calculate indices into pagetables
    capaddr_t l1_pagetable_idx = ARM_L1_OFFSET(vaddr);
    capaddr_t l2_pagetable_idx = ARM_L2_OFFSET(vaddr);

    //st->base_addr = vaddr + ((pte_count)*BASE_PAGE_SIZE);
    //st->base_addr = ROUND_UP(st->base_addr, (BASE_PAGE_SIZE * ARM_L2_MAX_ENTRIES));
    //debug_printf("setting base addr to %psi(%d)\n", st->base_addr,ARM_L1_OFFSET(st->base_addr));
    // get L2 page table
    struct l2_page_state *node = st->head;
    // map frame into l2 pagetable
    if (pte_count + l2_pagetable_idx >= ARM_L2_MAX_ENTRIES)
    {
        int free = 0;
        while (pte_count > 0)
        {
            free = ARM_L2_MAX_ENTRIES - l2_pagetable_idx;
            if (pte_count <= free)
            {
                free = pte_count;
            }
            //debug_printf("pte_count: %d, offset: %d free: %d\n",pte_count,offset,free);
            err = init_and_map_l2(st, node, l1_pagetable_idx, l2_pagetable_idx, free, offset, frame, flags);
            if (err_is_fail(err))
            {
                DEBUG_ERR(err, "in init_and_map_l2");
                return err;
            }
            //debug_printf("mapping frame into l1_pagetable: %d l2_pagetable: %d offset:%d free %d pte_count %d \n",node->l1_index,l2_pagetable_idx,offset,free,pte_count);
            pte_count -= free;
            offset += free * BASE_PAGE_SIZE;
            if (pte_count <= free)
            {
                free = pte_count;
            }
            l2_pagetable_idx = ARM_L2_OFFSET(vaddr + offset);
            l1_pagetable_idx = ARM_L1_OFFSET(vaddr + offset);
        }
    }
    else
    {
        //debug_printf("pte_count: %d, offset: %d\n",pte_count,offset);
        err = init_and_map_l2(st, node, l1_pagetable_idx, l2_pagetable_idx, pte_count, offset, frame, flags);
        //debug_printf("mapping frame into l2_pagetable offset:%d count %d\n",offset,pte_count);
        //debug_printf("get_croot_addr(*l2_pagetable): %u\n", get_croot_addr(*l2_pagetable));
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "in init_and_map_l2");
            return err;
        }
    }
    // st->base_addr = save;
    //debug_printf("setting baseaddr again to %p\n",st->base_addr);
    //debug_printf("returning from paging_map_fixed_attr\n");
    return SYS_ERR_OK;
}

errval_t init_and_map_l2(struct paging_state *st, struct l2_page_state *node, uint32_t l1_pagetable_idx, uint32_t l2_pagetable_idx, size_t pte_count,
                         size_t offset, struct capref frame, int flags)
{
    if (pte_count == 0 || pte_count + l2_pagetable_idx > 256)
    {
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
    //debug_printf("init and map l2 l1_idx: %d, l2_idx: %d, pte_count: %d, offset: %d\n",l1_pagetable_idx,l2_pagetable_idx,pte_count,offset);
    errval_t err;
    err = init_l2_page_state(st, &node, l1_pagetable_idx, l2_pagetable_idx, pte_count);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in init_l2_page_state");
        return err;
    }
    if (capref_is_null(node->l2_cap))
    {
        //debug_printf("l2 capref is null\n");
        err = map_l1_l2_wrapper(st, node);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "in map_l1_l2_wrapper");
            return err;
        }
    }
    //debug_printf("create mapping %p %d %d\n",node->l2_frame_mappings_end,l2_pagetable_idx,pte_count );

    if (node->l2_frame_mappings_end == NULL || node->l2_frame_mappings_end->next == NULL)
    {
        //debug_printf("get slab for l2_frame_mappings\n");
        //debug_printf("frame_mapping slabs left: %d, needed: %d start: %d, end: %d\n", slab_freecount(&st->mapping_slabs),pte_count/ARM_L2_MAX_ENTRIES,l2_pagetable_idx,l2_pagetable_idx+pte_count);
        if (slab_freecount(&st->mapping_slabs) < 5 && !refilling)
        {
            //debug_printf("REFILL\n");
            st->mapping_refill_count = 20;
            st->mapping_slabs.refill_func(&st->mapping_slabs);
        }
        struct l2_frame_mappings *tmp = slab_alloc(&st->mapping_slabs);
        //debug_printf("new mapping: %p\n",tmp);
        tmp->next = NULL;
        tmp->start = l2_pagetable_idx;
        tmp->end = l2_pagetable_idx + pte_count;
        tmp->mapping = NULL_CAP;
        tmp->id.base = 0x0;
        tmp->id.bytes = 0;

        if (node->l2_frame_mappings_end == NULL)
        {
            //debug_printf("first mapping!\n");
            node->l2_frame_mappings_end = tmp;
            //debug_printf("first mapping is now %p %p",node->l2_frame_mappings_end, tmp);
            node->l2_frame_mappings = tmp;
        }
        else
        {
            //debug_printf("append mapping!\n");
            node->l2_frame_mappings_end->next = tmp;
            node->l2_frame_mappings_end = tmp;
        }
    }
    else
    {
        //debug_printf("l2_frame_mapping is: %p\n",node->l2_frame_mappings_end);
    }
    //debug_printf("found node: %p, mappings: %p\n",node, node->l2_frame_mappings);
    struct capref *l2_pagetable = &node->l2_cap;
    struct capref *l2_to_frame_mapping = &(node->l2_frame_mappings_end->mapping);
    if (l2_to_frame_mapping == NULL)
    {
        //debug_printf("ERR: l2_to_frame_maping NULL\n");
        return LIB_ERR_SHOULD_NOT_GET_HERE;
    }
    //debug_printf("alloc slot\n");
    err = st->slot_alloc->alloc(st->slot_alloc, l2_to_frame_mapping);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in alot_alloc");
        return err;
    }
    struct capref local_l2_pagetable;
    if (get_croot_addr(*l2_pagetable) != CPTR_ROOTCN)
    {
        err = slot_alloc(&local_l2_pagetable);
        if (err_is_fail(err))
        {
            err_push(err, LIB_ERR_SLOT_ALLOC);
        }
        err = cap_copy(local_l2_pagetable, *l2_pagetable);
        if (err_is_fail(err))
        {
            err_push(err, LIB_ERR_CAP_COPY);
        }
    }
    else
    {
        local_l2_pagetable = *l2_pagetable;
    }

    struct capref actual_frame = NULL_CAP;
    if (get_croot_addr(frame) == CPTR_ROOTCN)
    {
        actual_frame = frame;
    }
    else
    {
        err = slot_alloc(&actual_frame);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "Error before copy frame cap in slot_alloc");
        }
        err = cap_copy(actual_frame, frame);
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "Error in copy frame cap");
        }
    }
    err = invoke_frame_identify(actual_frame, &node->l2_frame_mappings_end->id);
    if (err_is_fail(err))
    {
        debug_printf("error in identify frame\n");
        DEBUG_ERR(err, "Error in frame identify");
    }
    //debug_printf("map frame l2slot: %d l2:node %d root: %d %d frameslot: %d framenode: %d\n",local_l2_pagetable.slot,local_l2_pagetable.cnode,get_croot_addr(*l2_pagetable),CPTR_ROOTCN,frame.slot,frame.cnode);
    //debug_printf("mapping frame on %d->%d with offset %d and count %d\n",l1_pagetable_idx,l2_pagetable_idx,offset,pte_count);
    err = vnode_map(local_l2_pagetable, actual_frame, l2_pagetable_idx, flags, offset, pte_count, *l2_to_frame_mapping);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in vnode_map");
        return err;
    }

    for (int i = l2_pagetable_idx; i < pte_count + l2_pagetable_idx; i++)
    {
        SET_BIT(node->map, i);
    }
    return SYS_ERR_OK;
}

errval_t map_l1_l2_wrapper(struct paging_state *st, struct l2_page_state *l2_page_state)
{
    errval_t err;
    st->l2_alloc_ongoing = true;
    err = arml2_alloc(st, &l2_page_state->l2_cap);
    if (err_is_fail(err))
    {
        return err;
    }
    st->l2_alloc_ongoing = false;
    err = st->slot_alloc->alloc(st->slot_alloc, &l2_page_state->l1_l2_mapping);
    if (err_is_fail(err))
    {
        return err;
    }

    //debug_printf("get_croot_addr(st->l1_pagetable): %u\n", get_croot_addr(st->l1_pagetable));
    struct capref local_l1_pagetable;
    if (get_croot_addr(st->l1_pagetable) != CPTR_ROOTCN)
    {
        err = slot_alloc(&local_l1_pagetable);
        if (err_is_fail(err))
        {
            err_push(err, LIB_ERR_SLOT_ALLOC);
        }
        err = cap_copy(local_l1_pagetable, st->l1_pagetable);
        if (err_is_fail(err))
        {
            err_push(err, LIB_ERR_CAP_COPY);
        }
    }
    else
    {
        local_l1_pagetable = st->l1_pagetable;
    }
    // debug_printf("map l2 with index %d with mapping on %p l2_cap: %p \n",l2_page_state->l1_index,&l2_page_state->l1_l2_mapping,&l2_page_state->l2_cap);
    err = vnode_map(local_l1_pagetable, l2_page_state->l2_cap, l2_page_state->l1_index, VREGION_FLAGS_READ_WRITE, 0, 1, l2_page_state->l1_l2_mapping);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "error in vnode_map");
        return err;
    }
    //debug_printf("return from map_l1_l2_wrapper\n");
    return SYS_ERR_OK;
}

/**
 * \brief unmap region starting at address `region`.
 * NOTE: Implementing this function is optional.
 */
errval_t paging_unmap(struct paging_state *st, const void *region)
{
    return SYS_ERR_OK;
}

struct capref *get_l2_pagetable(struct paging_state *st, lvaddr_t vaddr)
{
    for (struct l2_page_state *l2 = st->head; l2 != NULL; l2 = l2->next)
    {
        if (ARM_L1_OFFSET(vaddr) == l2->l1_index)
        {
            return &l2->l2_cap;
        }
    }
    return NULL;
}

char *exp_type_to_string(enum exception_type type);
char *exp_type_to_string(enum exception_type type)
{

    switch (type)
    {
    case EXCEPT_NULL:
        return "EXCEPT_NULL";
    case EXCEPT_PAGEFAULT:
        return "EXCEPT_PAGEFAULT";
    case EXCEPT_BREAKPOINT:
        return "EXCEPT_BREAKPOINT";
    case EXCEPT_SINGLESTEP:
        return "EXCEPT_SINGLESTEP";
    case EXCEPT_OTHER:
        return "EXCEPT_OTHER";
    default:
        return "Exception type unknown";
    }
}

char *exp_subtype_to_string(int subtype);
char *exp_subtype_to_string(int subtype)
{
    switch (subtype)
    {
    case PAGEFLT_NULL:
        return "PAGEFLT_NULL";
    case PAGEFLT_READ:
        return "PAGEFLT_READ";
    case PAGEFLT_WRITE:
        return "PAGEFLT_WRITE";
    case PAGEFLT_EXEC:
        return "PAGEFLT_EXEC";
    default:
        return "UNKNOWN";
    }
}

void debug_print_fault_handler(enum exception_type type, int subtype, void *addr, arch_registers_state_t *regs);
void debug_print_fault_handler(enum exception_type type, int subtype, void *addr, arch_registers_state_t *regs)
{
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(curdispatcher());
    debug_printf("aos_page_fault_handler >>> Exception handler was called on %s, type=%d(%s), subtype=%d(%s), addr=%p pc=%p sp=%p\n",
                 disp->name, type, exp_type_to_string(type), subtype, exp_subtype_to_string(subtype), addr, regs->named.pc, regs->named.stack);
}
//just a test function for testPageFaultHandler
void aos_page_fault_handler(enum exception_type type, int subtype, void *addr, arch_registers_state_t *regs,
                            arch_registers_fpu_state_t *fpuregs)
{
    struct dispatcher_shared_generic *disp = get_dispatcher_shared_generic(curdispatcher());
    if (type != EXCEPT_PAGEFAULT)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("Unexpected Exception");
    }
    struct paging_state *pstate = get_current_paging_state();
    struct thread *me = thread_self();
    lvaddr_t thread_stack_guard = THREAD_STACK_GUARD(me->stack);
    //debug_printf("stack: %p, stack_top: %p, ex_stack: %p, ex_stack_top: %p\n",
    //	 me->stack, me->stack_top, me->exception_stack, me->exception_stack_top);
    if (addr < ((void *)BASE_PAGE_SIZE))
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: Null pointer dereference at instruction at %p!",
                   disp->name, registers_get_ip(regs)); // How about a user panic?
    }
    errval_t err = SYS_ERR_OK;
    struct capref malloc_frame;
    lvaddr_t cast_addr = (lvaddr_t)addr;
    //debug_printf("addr; %p cast_addr; %p\n",addr,cast_addr);
    size_t retbytes = 0;
    if (cast_addr > thread_stack_guard && cast_addr <= thread_stack_guard + GUARD_PAGE_SIZE)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: STACK OVERFLOW AT instruction %p! Trying to access %p.",
                   disp->name, registers_get_ip(regs), cast_addr);
    }

    if (cast_addr < VADDR_OFFSET)
    {
        // We are in reserved area, apparently, so we exit
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: We are in a reserved area, below where our vaddr space starts. Exiting...\n",
                   disp->name);
    }
    else if (cast_addr <= END_RESERVED)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: End Reserved section access attempt. Region restricted to page table hack, i.e. writing over the allocating bits\n", disp->name);
    }
    else if (cast_addr <= HEAP_BOTTOM)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: access at %p bellow heap bottom: %p", disp->name, cast_addr, HEAP_BOTTOM);
    }
    else if (cast_addr <= HEAP_TOP)
    {
        thread_mutex_lock(&pstate->mutex);
        // We are trying to access something in the heap
        if (frame_is_mapped(pstate, cast_addr))
        {
            thread_mutex_unlock(&pstate->mutex);
            return;
        }
        err = frame_alloc(&malloc_frame, BASE_PAGE_SIZE, &retbytes);
        if (err_is_fail(err))
        {
            thread_mutex_unlock(&pstate->mutex);
            USER_PANIC_ERR(err, "%s: OUT OF MEM in %s\n", disp->name, __FUNCTION__);
        }
        err = paging_map_fixed_attr(pstate, cast_addr, malloc_frame, BASE_PAGE_SIZE, VREGION_FLAGS_READ_WRITE);
        if (err_is_fail(err))
        {
            thread_mutex_unlock(&pstate->mutex);
            USER_PANIC_ERR(err, "%s Could not map the frame in page fault handler\n", disp->name);
        }
        thread_mutex_unlock(&pstate->mutex);
    }
    else if (cast_addr <= BORDERLAND)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: You are in the region above the heap top but below the stack bottom. This region will be used for dynamic growth\n", disp->name);
        // This will need to be chagned for dynamic growth;
    }
    else if (cast_addr <= STACK_BOTTOM)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: You have overflowed the stack\n", disp->name);
    }
    else if (cast_addr <= STACK_TOP)
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC("%s: If you are here, something went really wrong. You are in the stack\n", disp->name);
    }
    else
    {
        debug_print_fault_handler(type, subtype, addr, regs);
        USER_PANIC_ERR(LIB_ERR_SHOULD_NOT_GET_HERE, "%s: Things went very wrong. You are above the stack\n", disp->name);
    }

    // debug_printf("We have exited %s\n", __FUNCTION__);

    // is_addr_valid function? returns true if

    //debug_print_fpu_state(fpuregs);
    //void debug_dump(regs);
}

/**
* \brief Will test if the page fault handler functionality is implemented correctly
*/
__attribute__((used))
errval_t
aos_set_page_fault_handler(void)
{
    //debug_printf("aos_set_page_fault_handler >>> START\n");

    //Setting up stack for the page fault handler
    //set page fault handler
    void *old_stack_base = NULL;
    void *old_stack_top = NULL;
    lvaddr_t *stack_base = NULL;

    struct capref frame_cap;
    size_t bytes = 0;
    //  size_t retbytes = 0;
    errval_t err;
    // in the init domain frame_alloc uses ram_alloc_fixed, which can only alloc memory in chunks of
    // size BASE_PAGE_SIZE. Therefore we have to and allocate and map the stack in a loop.
    err = frame_alloc(&frame_cap, 4 * BASE_PAGE_SIZE, &bytes);
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "No frame alloc'd\n");
    }
    err = paging_map_frame_attr(get_current_paging_state(), (void **)&stack_base, bytes, frame_cap, VREGION_FLAGS_READ_WRITE, NULL, NULL);
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "No mem");
    }

    //debug_printf("got frame: 0x%" PRIxGENPADDR " mapped at %p with %d\n", id.base, stack_base,retbytes);

    memset((char *)stack_base, 0, BASE_PAGE_SIZE);

    void *stack_top = (char *)stack_base + bytes;
    //debug_printf("We have mapped our stack into the stack_base at %p stack_top: %p\n", stack_base, stack_top);

    exception_handler_fn oldhandler;
    //dump_dispatcher(get_dispatcher_shared_generic(curdispatcher()));

    //debug_printf("setting page fault handler %p with %p to %p (%d->%d)\n", aos_page_fault_handler, stack_base, stack_top, ARM_L2_OFFSET(stack_base), ARM_L2_OFFSET(stack_top));
    err = thread_set_exception_handler(aos_page_fault_handler, &oldhandler, (void *)stack_base, (void *)stack_top,
                                       &old_stack_base, &old_stack_top);
    if (err_is_fail(err))
    {
        USER_PANIC_ERR(err, "Setting the handler failed");
    }
    // dump_dispatcher(get_dispatcher_shared_generic(curdispatcher()));
    // printf("aos_set_page_fault_handler >>> After trying to set the handler\n");
    if (old_stack_base != NULL && old_stack_top != NULL)
    {
        debug_printf("aos_set_page_fault_handler >>> old_stack_base=%p, old_stack_top=%p\n",
                     old_stack_base, old_stack_top);
    }

    // printf("aos_set_page_fault_handler >>> END\n");
    return SYS_ERR_OK;
}

bool frame_is_mapped(struct paging_state *st, lvaddr_t addr)
{
    for (struct l2_page_state *l2 = st->head; l2 != NULL; l2 = l2->next)
    {
        if (ARM_L1_OFFSET(addr) != l2->l1_index)
        {
            continue;
        }
        for (struct l2_frame_mappings *l2_frame = l2->l2_frame_mappings; l2_frame != NULL; l2_frame = l2_frame->next)
        {
            if (ARM_L2_OFFSET(addr) >= l2_frame->start && ARM_L2_OFFSET(addr) < l2_frame->end)
            {
                return true;
            }
        }
        break;
    }
    return false;
}

void print_paging_state(struct paging_state *st, bool verbose)
{
    debug_printf("\n\npaging state %p has base_addr: %p, mapping slabs free: %u, l2_page_state slabs free: %u\n", st, st->base_addr, slab_freecount(&st->slabs), slab_freecount(&st->mapping_slabs));
    debug_printf("\tIterating over l2_page_state\n");
    size_t setcount = 0;
    size_t usedcount = 0;
    size_t l2_count = 0;
    for (struct l2_page_state *l2 = st->head; l2 != NULL; l2 = l2->next)
    {
        l2_count++;
        for (int i = 0; i < ARM_L2_MAX_ENTRIES; i++)
        {
            if (CHECK_BIT(l2->map, i))
            {
                setcount++;
            }
        }
        for (struct l2_frame_mappings *l2_frame = l2->l2_frame_mappings; l2_frame != NULL; l2_frame = l2_frame->next)
        {
            usedcount += (l2_frame->end - l2_frame->start);
        }
    }
    debug_printf("\tFound %d l2_page_state. %d entries are reserved and %d entries are actuall used\n", l2_count, setcount, usedcount);
    if (verbose)
    {
        debug_printf("verbose flag set. Print l2_page_tables\n");
        for (struct l2_page_state *tmp = st->head; tmp != NULL; tmp = tmp->next)
        {
            debug_printf("l2_state: %p next: %p l1 index: %d\n", tmp, tmp->next, tmp->l1_index);
            for (int i = 0; i < 8; i++)
            {
                debug_printf("%d: 0x%x\n", i, tmp->map[i]);
            }
            for (struct l2_frame_mappings *l2_frame_mappings = tmp->l2_frame_mappings; l2_frame_mappings != NULL; l2_frame_mappings = l2_frame_mappings->next)
            {
                debug_printf("mapping: in l2: %d frame: %" PRIx64 " with %d bytes\n", tmp->l1_index, l2_frame_mappings->id.base, l2_frame_mappings->id.bytes);
            }
        }
    }
    debug_printf("\n");
}
