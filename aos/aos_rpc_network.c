/**
 * \file
 * \brief Implementation of Network related rpc functions
 */

#include <aos/aos_rpc.h>
#include <aos/aos_rpc_network.h>

void aos_rpc_network_in_packet_handler(struct aos_rpc *rpc)
{
  in_port_t dport;
  in_port_t sport;
  struct connection *curr;
  switch (rpc->msg->words[RPC_TYPE_SLOT])
  {
  case MSG_TYPE_NETWORK_PAYLOAD_START:
    dport = rpc->msg->words[RPC_NETWORK_START_DPORT_FIELD];
    sport = rpc->msg->words[RPC_NETWORK_START_SPORT_FIELD];
    size_t length;
    length = rpc->msg->words[RPC_NETWORK_START_LENGTH_FIELD];
    for (curr = rpc->connections; curr != NULL; curr = curr->next)
    {
      if (curr->dport == sport && dport == curr->sport)
      {
        struct in_addr src_ip = {
            .s_addr = rpc->msg->words[RPC_NETWORK_START_DST_IP],
        };
        struct in_addr dst_ip = {
            .s_addr = rpc->msg->words[RPC_NETWORK_START_SRC_IP],
        };
        curr->src_ip = src_ip;
        curr->dst_ip = dst_ip;
        curr->dport = rpc->msg->words[RPC_NETWORK_START_SPORT_FIELD];
        curr->sport = rpc->msg->words[RPC_NETWORK_START_DPORT_FIELD];
        curr->length = length;
        curr->buf_in = malloc(sizeof(uint8_t) * length);
        curr->offset = 0;
        return;
      }
    }
    if (!curr)
    {
      for (curr = rpc->connections; curr->next != NULL; curr = curr->next)
      {
      }
      curr->next = malloc(sizeof(struct connection));
      curr = curr->next;

      curr->sport = dport;
      curr->dport = sport;
      struct in_addr src_ip = {
          .s_addr = rpc->msg->words[RPC_NETWORK_START_DST_IP],
      };
      struct in_addr dst_ip = {
          .s_addr = rpc->msg->words[RPC_NETWORK_START_SRC_IP],
      };
      curr->src_ip = src_ip;
      curr->dst_ip = dst_ip;
      curr->next = NULL;
      curr->length = length;
      curr->buf_in = malloc(sizeof(uint8_t) * length);
      curr->offset = 0;
    }
    else
    {
      DEBUG_ERR(LIB_ERR_NOT_IMPLEMENTED, "dport not found\n");
    }
    break;
  case MSG_TYPE_NETWORK_PAYLOAD_CONTENT:
  {

    size_t bytes_per_package = (LMP_MSG_LENGTH - RPC_NETWORK_CONTENT_OCCUPIED_FIELDS) * sizeof(uintptr_t);
    dport = rpc->msg->words[RPC_NETWORK_CONTENT_DPORT];
    sport = rpc->msg->words[RPC_NETWORK_CONTENT_SPORT];
    for (curr = rpc->connections; curr != NULL; curr = curr->next)
    {
      if (curr->sport == dport && curr->dport == sport)
      {
        //debug_printf("Prior to words\n");
        words_print(rpc->msg->words);
        size_t copy_amount = (curr->length - curr->offset < bytes_per_package) ? curr->length - curr->offset : bytes_per_package;

        memcpy(((char *)curr->buf_in) + curr->offset, &rpc->msg->words[RPC_NETWORK_CONTENT_START], copy_amount);
        memset(((char *)curr->buf_in) + curr->offset + copy_amount, 0x00, bytes_per_package - copy_amount);
        //debug_printf("curr->length: %d curr->offset: %d bytes_per_package: %d copy_amount: %d %x\n", curr->length, curr->offset, bytes_per_package, copy_amount, *(curr->buf_in+curr->offset));
        curr->offset += bytes_per_package;
        return;
      }
    }
    DEBUG_ERR(LIB_ERR_NOT_IMPLEMENTED, "dport not found\n");
    break;
  }
  case MSG_TYPE_NETWORK_PAYLOAD_FIN:
    //debug_printf("FIN\n");
    //msg_print(rpc->msg->words, 6);
    break;
  }
}

void aos_rpc_network_out_packet_handler(struct aos_rpc *rpc){
  in_port_t dport;
  struct connection *conn;
  switch(rpc->msg->words[RPC_TYPE_SLOT]){
   case MSG_TYPE_NETWORK_UDP_PAYLOAD_START:
     //debug_printf("udp payload handler recv'd start\n");
     words_print(rpc->msg->words);
     /* Since there is no binding for the udp payload, we have to create
	the connection struct right here */
     dport = rpc->msg->words[RPC_NETWORK_START_DPORT_FIELD];
     conn = dport_lookup_connection(rpc, dport);
     // Attempt to look up the connection, if we can't find it then we create a new one
     if (conn == NULL) {
       //debug_printf("NEW CONNECTION ESTABLISHED\n");
       conn = malloc(sizeof(struct connection));
       memset(conn,0, sizeof(struct connection));
       struct in_addr src_ip = {
	 .s_addr = rpc->msg->words[RPC_NETWORK_START_SRC_IP],
       };
       struct in_addr dst_ip = {
           .s_addr = rpc->msg->words[RPC_NETWORK_START_DST_IP],
       };
       conn->src_ip = src_ip;
       conn->dst_ip = dst_ip;
       conn->sport = rpc->msg->words[RPC_NETWORK_START_SPORT_FIELD];
       conn->dport = rpc->msg->words[RPC_NETWORK_START_DPORT_FIELD];
       conn->length = rpc->msg->words[RPC_NETWORK_START_LENGTH_FIELD];
       conn->buf_out = malloc(sizeof(uint8_t) * conn->length);
       conn->offset = 0;
       struct connection *curr_connection = rpc->connections;
       if (rpc->connections == NULL)
       {
         rpc->connections = conn;
       }
       else
       {
         for (curr_connection = rpc->connections; curr_connection->next != NULL; curr_connection = curr_connection->next)
           ;
         curr_connection->next = conn;
       }
     }
     else
     {
       free(conn->buf_out);
       conn->length = rpc->msg->words[RPC_NETWORK_START_LENGTH_FIELD];
       conn->buf_out = malloc(sizeof(uint8_t) * conn->length);
       conn->offset = 0;
     }
     break;
   case MSG_TYPE_NETWORK_UDP_PAYLOAD_CONTENT:
   {
     //debug_printf("udp payload handler recvd content\n");
     size_t bytes_per_package = (LMP_MSG_LENGTH - RPC_NETWORK_CONTENT_OCCUPIED_FIELDS) * sizeof(uintptr_t);
     dport = rpc->msg->words[RPC_NETWORK_CONTENT_DPORT];
     for (conn = rpc->connections; conn != NULL; conn = conn->next)
     {
       if (conn->dport == dport)
       {
         words_print(rpc->msg->words);
         size_t copy_amount = (conn->length - conn->offset < bytes_per_package) ? conn->length - conn->offset : bytes_per_package;
         memcpy(((char *)conn->buf_out) + conn->offset, &rpc->msg->words[RPC_NETWORK_CONTENT_START], copy_amount);
         memset(((char *)conn->buf_out) + conn->offset + copy_amount, 0x00, bytes_per_package - copy_amount);
         conn->offset += bytes_per_package;
         return;
       }
     }
     DEBUG_ERR(LIB_ERR_NOT_IMPLEMENTED, "dport not found\n");
     break;
  }
  case MSG_TYPE_NETWORK_UDP_PAYLOAD_FIN:
    //debug_printf("FIN\n");
    break;
  }

}

void msg_print(uintptr_t *words, size_t len){
  printf("Print words\n");
  for(int i = 0; i < len; i++){
    printf("%08x\n",words[i]);
  }
}

struct connection *sport_lookup_connection(struct aos_rpc *rpc, in_port_t sport){
  for (struct connection *curr = rpc->connections; curr != NULL; curr = curr->next){
   // debug_printf("OUR SPORT %d\n", curr->sport);
    if (curr->sport == sport){
      return curr;
    }
  }
  return NULL;
}

struct connection *dport_lookup_connection(struct aos_rpc *rpc, in_port_t dport){
  for (struct connection *curr = rpc->connections; curr != NULL; curr = curr->next){
    if (curr->dport == dport)
    {
      return curr;
    }
  }
  return NULL;
}

void print_connections(struct aos_rpc *rpc){
  return;
  for (struct connection *curr = rpc->connections; curr != NULL; curr = curr->next)
  {
    debug_printf("src_ip: %d\n", curr->src_ip.s_addr);
    debug_printf("dst_ip: %d\n", curr->dst_ip.s_addr);
    debug_printf("sport: %d\n", curr->sport);
    debug_printf("dport: %d\n", curr->dport);
    debug_printf("Len: %d\n", curr->length);  
  }
}

void words_print(uintptr_t *words){
  return;
  for (int i = 0; i < 8; i++){
    debug_printf("%08x\n",words[i]);
  }
}

errval_t aos_rpc_network_bind(struct aos_rpc *chan,  in_port_t port)
{
  errval_t err;
  err = aos_rpc_send_number_type(chan, port, MSG_TYPE_NETWORK_BIND);
  if(err_is_fail(err)) {
    DEBUG_ERR(err, "in send MSG_TYPE_NETWORK_BIND to init\n");
    return err;
  }

  err = wait_for_msg_or_err(chan, get_default_waitset(), MSG_TYPE_NETWORK_BIND_OK,MSG_TYPE_NETWORK_BIND_FAIL);
  if(err_is_fail(err)) {
    DEBUG_ERR(err, "in wait for REGISTER_RESP");
    return err;
  }

  struct connection *curr_connection = chan->connections;
  if (curr_connection == NULL){
    curr_connection = malloc(sizeof(struct connection));
    curr_connection->sport = port;
    curr_connection->next = NULL;
    chan->connections = curr_connection;
  } else {
    for (curr_connection = chan->connections; curr_connection->next != NULL; curr_connection = curr_connection->next){

    }
    curr_connection->next = malloc(sizeof(struct connection));
    curr_connection->next->sport = port;
    curr_connection->next->next = NULL;
  }
  return SYS_ERR_OK;
}

errval_t aos_rpc_network_send_payload(struct aos_rpc *rpc, size_t length, struct in_addr src_ip, struct in_addr dst_ip, in_port_t sport, in_port_t dport, uintptr_t *buf_out, enum msg_type start, enum msg_type content, enum msg_type fin){
  errval_t err;
  uintptr_t msg[LMP_MSG_LENGTH];
  msg[RPC_TYPE_SLOT] = start;
  msg[RPC_NETWORK_START_LENGTH_FIELD] = length;
  msg[RPC_NETWORK_START_SRC_IP] = src_ip.s_addr;
  msg[RPC_NETWORK_START_DST_IP] = dst_ip.s_addr;
  msg[RPC_NETWORK_START_SPORT_FIELD] = sport;
  msg[RPC_NETWORK_START_DPORT_FIELD] = dport;
  while(true){
    err = lmp_chan_send6(&rpc->chan,LMP_SEND_FLAGS_DEFAULT,NULL_CAP,msg[RPC_TYPE_SLOT],
			 msg[RPC_NETWORK_START_LENGTH_FIELD],msg[RPC_NETWORK_START_SRC_IP],
			 msg[RPC_NETWORK_START_DST_IP],msg[RPC_NETWORK_START_SPORT_FIELD],
			 msg[RPC_NETWORK_START_DPORT_FIELD]);
    if (lmp_err_is_transient(err)){
      continue;
    } else if (err_is_fail(err)){
      DEBUG_ERR(err, "could not send network packet\n");
    } else {
      break;
    }
  }
  size_t bytes_per_package = (LMP_MSG_LENGTH - RPC_NETWORK_CONTENT_OCCUPIED_FIELDS) * sizeof(uintptr_t); //bytes that we can send not including the message type
  size_t content_chunks = (length / bytes_per_package)+1;
  size_t offset = 0;
  uintptr_t content_msg[LMP_MSG_LENGTH];
  content_msg[RPC_TYPE_SLOT] = content;
  content_msg[RPC_NETWORK_CONTENT_DPORT] =  msg[RPC_NETWORK_START_DPORT_FIELD];
  content_msg[RPC_NETWORK_CONTENT_SPORT] =  msg[RPC_NETWORK_START_SPORT_FIELD];
  for (int i = 0; i < content_chunks; i++)
  {
    memcpy(content_msg + RPC_NETWORK_CONTENT_OCCUPIED_FIELDS, ((char *)buf_out) + offset, bytes_per_package);
    offset += bytes_per_package;
    words_print(content_msg);
    while (true)
    {
      err = lmp_chan_send9(&rpc->chan, LMP_SEND_FLAGS_DEFAULT, NULL_CAP,
                           content_msg[RPC_TYPE_SLOT],
                           content_msg[RPC_NETWORK_CONTENT_DPORT],
                           content_msg[RPC_NETWORK_CONTENT_SPORT], content_msg[3], content_msg[4],
                           content_msg[5], content_msg[6], content_msg[7],
                           content_msg[8]);
      if (lmp_err_is_transient(err))
      {
        while ((err = event_dispatch_non_block(rpc->default_waitset)) != LIB_ERR_NO_EVENT)
        {
          if (!lmp_err_is_transient(err) && err_is_fail(err))
          {
            DEBUG_ERR(err, "cannot get default waitset\n");
            return err;
          }
        }
        continue;
      }
      else if (err_is_fail(err))
      {
        DEBUG_ERR(err, "could not send network packet\n");
      }
      else
      {
        break;
      }
    }
  }
  msg[RPC_TYPE_SLOT] = fin;
  while(true) {
    err = lmp_chan_send6(&rpc->chan,LMP_SEND_FLAGS_DEFAULT,NULL_CAP,msg[RPC_TYPE_SLOT],
			 msg[RPC_NETWORK_START_LENGTH_FIELD],msg[RPC_NETWORK_START_SRC_IP],
			 msg[RPC_NETWORK_START_DST_IP],msg[RPC_NETWORK_START_SPORT_FIELD],
			 msg[RPC_NETWORK_START_DPORT_FIELD]);
    if (lmp_err_is_transient(err)){
      while ((err = event_dispatch_non_block(rpc->default_waitset)) != LIB_ERR_NO_EVENT)
        {
          if (!lmp_err_is_transient(err) && err_is_fail(err))
          {
            DEBUG_ERR(err, "cannot get default waitset\n");
            return err;
          }
        }
      thread_yield();
      continue;
    } else if (err_is_fail(err)){
      DEBUG_ERR(err, "could not send network packet\n");
    } else {
      break;
    }		 			 
  }
  return err;
}


errval_t aos_rpc_network_in_packet(struct aos_rpc *rpc, size_t length, struct in_addr src_ip, struct in_addr dst_ip, in_port_t sport, in_port_t dport, uint8_t *packet){
  // Getting a message on wire and forwarding to init i.e. serial uses this
  return aos_rpc_network_send_payload(rpc, length, src_ip, dst_ip, sport,dport, (uintptr_t*)packet, MSG_TYPE_NETWORK_PAYLOAD_START, MSG_TYPE_NETWORK_PAYLOAD_CONTENT, MSG_TYPE_NETWORK_PAYLOAD_FIN);
}

errval_t aos_rpc_network_network_out_packet_handler(struct aos_rpc *rpc, size_t length, struct in_addr src_ip, struct in_addr dst_ip, in_port_t sport, in_port_t dport, uintptr_t *packet){
  return aos_rpc_network_send_payload(rpc, length, src_ip, dst_ip, sport, dport, packet, MSG_TYPE_NETWORK_UDP_PAYLOAD_START, MSG_TYPE_NETWORK_UDP_PAYLOAD_CONTENT, MSG_TYPE_NETWORK_UDP_PAYLOAD_FIN);
}