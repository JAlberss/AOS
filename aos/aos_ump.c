#include <aos/aos.h>
#include <aos/aos_rpc.h>
#include <spawn/spawn.h>
#include <barrelfish_kpi/asm_inlines_arch.h>

struct urpc_buffer_container *urpc_head = NULL;
umphandler_t spawn_func = aos_ump_spawn_handler;
umphandler_t pt_func = aos_ump_ptable_handler;
umphandler_t getpid_func = aos_ump_get_pids_handler;
umphandler_t putchar_func = aos_ump_putchar_handler;
umphandler_t getchar_func = aos_ump_getchar_handler;
umphandler_t network_bind_func = NULL;
umphandler_t network_bind_resp_func = NULL;
umphandler_t network_in_handler_func = NULL;
umphandler_t network_udp_packet_handler_func = NULL;
umphandler_t cap_send_func = NULL;

errval_t urpc_buffer_read_line(struct urpc_buffer_container *urpc)
{
    uintptr_t ret[URPC_DATA_SIZE];
    errval_t err = urpc_buffer_read_words(urpc->recv_buf, ret);
    if (err == SYS_ERR_OK)
    {
        switch (ret[UMP_TYPE_SLOT])
        {
        case MSG_TYPE_BIND_REQ:
            aos_ump_bind_handler(ret);
            break;
        case MSG_TYPE_BIND_ACK:
            aos_ump_bind_ack_handler(ret);
            break;
        case MSG_TYPE_BIND_FAIL:
            aos_ump_bind_fail_handler(ret);
            break;
        case MSG_TYPE_TEST:
              printf("%d.%d received: %s\n", disp_get_core_id(), disp_get_domain_id(), msg_to_string(ret[UMP_TYPE_SLOT]));
              break;
        case MSG_TYPE_SPAWN_REQ:
            spawn_func(urpc, ret);
            break;
        case MSG_TYPE_PT_REQ:
            pt_func(urpc, ret);
            break;
        case MSG_TYPE_PIDLIST_END:
        case MSG_TYPE_PIDLIST_REQ:
            getpid_func(urpc, ret);
            break;
        case MSG_TYPE_PIDLIST_RESP:
            aos_ump_pidlist_resp_handler(urpc, ret);
            break;
        case MSG_TYPE_CHILD_SPAWNED:
            aos_ump_child_spawned_handler(urpc, ret);
            break;
        case MSG_TYPE_PT_RESP:
            aos_ump_pt_resp_handler(urpc, ret);
            break;
        case MSG_TYPE_PUT_CHAR:
            putchar_func(urpc, ret);
            break;
        case MSG_TYPE_GET_CHAR_RESP:
        case MSG_TYPE_GET_CHAR_REQ:
            getchar_func(urpc, ret);
            break;
        case MSG_TYPE_NETWORK_BIND:
            network_bind_func(urpc, ret);
            break;
        case MSG_TYPE_NETWORK_BIND_FAIL:
        case MSG_TYPE_NETWORK_BIND_OK:
            network_bind_resp_func(urpc, ret);
            break;
        case MSG_TYPE_NETWORK_PAYLOAD_START:
        case MSG_TYPE_NETWORK_PAYLOAD_CONTENT:
        case MSG_TYPE_NETWORK_PAYLOAD_FIN:
            network_in_handler_func(urpc, ret);
            break;
        case MSG_TYPE_NETWORK_UDP_PAYLOAD_CONTENT:
        case MSG_TYPE_NETWORK_UDP_PAYLOAD_FIN:
        case MSG_TYPE_NETWORK_UDP_PAYLOAD_START:
            network_udp_packet_handler_func(urpc, ret);
            break;
        case MSG_TYPE_CAP_SEND:
            cap_send_func(urpc, ret);
            break;
        default:
            debug_printf("MSG TYPE unknown: %s\n", msg_to_string(ret[UMP_TYPE_SLOT]));
        }
    }
    return err;
}

errval_t aos_ump_child_spawned_handler(struct urpc_buffer_container *urpc, uintptr_t *msg)
{
    struct aos_process *curr;
    for (curr = pt_head; curr->next != NULL;curr = curr->next)
        ;
    assert(curr && !curr->next);
    curr->next = malloc(sizeof(struct aos_process));
    if(!curr->next) {
        return LIB_ERR_MALLOC_FAIL;
    }
    curr = curr->next;
    curr->core = msg[3];
    curr->pid = msg[4];
    curr->next = NULL;
    curr->si = NULL;
    size_t name_len = (URPC_NUM_DATA - 5) * sizeof(uintptr_t);
    curr->binary_name = malloc(name_len);
    strncpy(curr->binary_name, (char *)(msg + 5), strlen((char *)(msg + 5)));
    curr->binary_name[name_len] = 0;
    curr->out_buf = malloc(SERIAL_OUTPUT_BUF_SIZE);
    //debug_printf("child %s spawned with %d.%d\n",curr->binary_name,curr->core,curr->pid);
    return SYS_ERR_OK;
}

errval_t aos_ump_pidlist_resp_handler(struct urpc_buffer_container *urpc, uintptr_t *msg)
{
    errval_t err;
    debug_printf("revieced a pidlist\n");
    debug_printf("Pid is %d\n", msg[1]);
    while (true)
    {
        err = urpc_buffer_read_words(urpc->recv_buf, msg);
        if(err == LIB_ERR_NO_UMP_MSG) {
            continue;
        }
        debug_printf("Pid is %d\n", msg[1]);
        if (err == LIB_ERR_NO_UMP_MSG)
        {
            continue;
        }
        if (msg[UMP_TYPE_SLOT] == MSG_TYPE_PIDLIST_END)
        {
            debug_printf("pidlist end\n");
            break;
        }
    }
    return SYS_ERR_OK;
}

errval_t aos_ump_pt_resp_handler(struct urpc_buffer_container *urpc, uintptr_t *msg)
{
    uintptr_t ret[URPC_DATA_SIZE];
    size_t str_len = msg[1];
    errval_t err;
    size_t parsed = 0;
    char *buf = malloc(sizeof(char) * str_len) + 1;
    if (str_len / sizeof(uintptr_t) > URPC_LONGSTR_INIT_DATA)
    {
        memcpy(buf, (char *)(msg + 2), URPC_LONGSTR_NUM_DATA * sizeof(uintptr_t));
        parsed = URPC_LONGSTR_INIT_DATA * sizeof(uintptr_t);
    }
    else
    {
        memcpy(buf, (char *)(msg + 2), str_len);
        parsed = str_len;
    }
    while (true)
    {
        size_t remaining = str_len - parsed;
        err = urpc_buffer_read_words(urpc->recv_buf, ret);
        if (err == LIB_ERR_NO_UMP_MSG)
        {
            continue;
        }
        if (ret[UMP_TYPE_SLOT] == MSG_TYPE_STRING_FIN)
        {
            break;
        }
        else if (ret[UMP_TYPE_SLOT] == MSG_TYPE_STRING_PARTIAL)
        {
            if (remaining < URPC_NUM_DATA * sizeof(uintptr_t))
            {
                memcpy(buf + parsed, (char *)(ret + 1), remaining);
                parsed += remaining;
            }
            else
            {
                memcpy(buf + parsed, (char *)(ret + 1), URPC_NUM_DATA * sizeof(uintptr_t));
                parsed += URPC_NUM_DATA * sizeof(uintptr_t);
            }
        }
        else
        {
            debug_printf("Error unexpected msg type: %s\n", msg_to_string(ret[UMP_TYPE_SLOT]));
        }
    }
    buf[str_len + 1] = 0;

    debug_printf("recievec ptlist: %s\n", (char *)(buf));
    return SYS_ERR_OK;
}

errval_t aos_ump_ptable_handler(struct urpc_buffer_container *urpc, uintptr_t *msg)
{
    char *table;
    errval_t err;
    err = aos_rpc_process_get_ptable(get_init_rpc(), &table);
    if(err_is_fail(err)) {
        DEBUG_ERR(err, "Error while getting pt list via rpc");
        return err;
    }
    err = urpc_buffer_write_long_string(urpc->send_buf, MSG_TYPE_PT_RESP, table, strlen(table));
    if(err_is_fail(err)) {
        DEBUG_ERR(err, "Error while sending pt list via ump");
        return err;
    }
    return SYS_ERR_OK;
}

errval_t aos_ump_get_pids_handler(struct urpc_buffer_container *urpc, uintptr_t *msg)
{
     errval_t err;
    domainid_t *pids = malloc(20*sizeof(domainid_t));
    size_t count;
    err = aos_rpc_process_get_all_pids(get_init_rpc(), &pids, &count);
    if(err_is_fail(err)) {
        DEBUG_ERR(err, "Error while get pid list via rpc");
        return err;
    }
    for (int i = 0; i < count; i++)
    {
        if (i+1 == count)
        {
            while ((err = urpc_buffer_write_message_val(urpc->send_buf, MSG_TYPE_PIDLIST_END, pids[i], NULL, 0)) == LIB_ERR_UMP_CHAN_FULL)
                ;
        }
        else
        {
            while ((err = urpc_buffer_write_message_val(urpc->send_buf, MSG_TYPE_PIDLIST_RESP, pids[i], NULL, 0)) == LIB_ERR_UMP_CHAN_FULL)
                ;
        }
        if (err_is_fail(err))
        {
            DEBUG_ERR(err, "Error while sending pidlist");
            return err;
        }
    }
    debug_printf("aos_ump_ptable_handler NYI\n");
    return SYS_ERR_OK;
}

errval_t aos_ump_spawn_handler(struct urpc_buffer_container *urpc, uintptr_t *msg)
{
    char *buf = malloc(sizeof(char) * 60);
    if (disp_get_domain_id() == INIT_PID)
    {
        debug_printf("spawn req on %s Not implemented in default handler. use init handler instead\n", disp_name());
    }
    else
    {
        for (int i = 2; i < (60 / sizeof(uintptr_t)); i++)
        {
            parse_int_to_string(msg[i], buf + ((i - 2) * 4));
        }
        domainid_t pid;
        aos_rpc_process_spawn(get_init_rpc(), buf, msg[1], &pid);
    }
    return SYS_ERR_OK;
}

errval_t urpc_buffer_write_long_string(struct urpc_buffer_descriptor *send_buf, enum msg_type type, char *msg, size_t length)
{
    errval_t err;
    size_t send_length = 0;
    if (length < URPC_DATA_SIZE - sizeof(uintptr_t))
    {
        send_length = (length / sizeof(uintptr_t));
    }
    else
    {
        send_length = (URPC_DATA_SIZE / sizeof(uintptr_t));
    }
    send_length -= 2;
    size_t remaining_send = length - (send_length * sizeof(uintptr_t));

    err = urpc_buffer_write_message_val(send_buf, type, length, (uintptr_t *)(msg), send_length);
    if (err_is_fail(err))
    {
        DEBUG_ERR(err, "in sending fist string msg with type %s\n", msg_to_string(type));
    }
    type = MSG_TYPE_STRING_PARTIAL;
    size_t msg_count = (remaining_send / URPC_DATA_SIZE) + 1;
    for (int i = 0; i < msg_count; i++)
    {
        if (remaining_send >= 60)
        {
            send_length = URPC_DATA_SIZE / sizeof(uintptr_t);
        }
        else
        {
            send_length = (remaining_send / sizeof(uintptr_t)) + 1;
        }
        size_t offset = (length - remaining_send);
        err = urpc_buffer_write_message(send_buf, type, (uintptr_t *)(msg + offset), send_length);
        remaining_send -= send_length;
        if (err_is_fail(err))
        {
            return err;
        }
    }
    err = urpc_buffer_write_message(send_buf, MSG_TYPE_STRING_FIN, NULL, 0);

    return SYS_ERR_OK;
}

errval_t aos_ump_getchar_handler(struct urpc_buffer_container * urpc, uintptr_t *msg) {
    return LIB_ERR_NOT_IMPLEMENTED;
}

errval_t aos_ump_putchar_handler(struct urpc_buffer_container * urpc, uintptr_t *msg) {
    errval_t err;
    char c = msg[1];
    if (disp_get_domain_id() == INIT_PID)
    {
        //debug_printf("On some init process\n");
        if (disp_get_core_id() == BSP_CORE)
        {
            sys_print(&c, sizeof(c));
        }
        else
        {
            struct urpc_buffer_container *BSP_init = get_urpc_buffer_container(urpc_head, 0, INIT_PID);
            err = aos_ump_serial_putchar(BSP_init->send_buf, c);
            if (err_is_fail(err))
            {
                return err;
            }
        }
    }
    else
    {
        // other domains just rpc their serial channel
        struct aos_rpc *rpc = aos_rpc_get_serial_channel();
        err = aos_rpc_serial_putchar(rpc, c);
        if (err_is_fail(err))
        {
            return err;
        }
    }

    return SYS_ERR_OK;
}


errval_t aos_ump_serial_putchar(struct urpc_buffer_descriptor *send_buf, char c) {
    uintptr_t msg[4];
    msg[UMP_TYPE_SLOT]  = 0;
    msg[UMP_PUT_CHAR_PAYLOAD_FIELD] = c;
    msg[UMP_PUT_CHAR_SRC_CORE_FIELD] = disp_get_core_id();
    msg[UMP_PUT_CHAR_SRC_PID_FIELD] = disp_get_domain_id();
    return urpc_buffer_write_message(send_buf, MSG_TYPE_PUT_CHAR, msg,sizeof(msg)/sizeof(uintptr_t));
}

errval_t urpc_buffer_write_message(struct urpc_buffer_descriptor *send_buf, enum msg_type type, uintptr_t *word, size_t length)
{
    if (length > URPC_NUM_DATA)
    {
        return LIB_ERR_MSGBUF_OVERFLOW;
    }
    uintptr_t msg[length + 1];
    memset(msg, 0, sizeof(uintptr_t) * length + 1);
    msg[UMP_TYPE_SLOT] = type;
    if (length > 0)
    {
        memcpy(msg + 1, word, length * sizeof(uintptr_t));
    }

    return urpc_buffer_write_word(send_buf, msg, length + 1);
}

errval_t urpc_buffer_write_message_val(struct urpc_buffer_descriptor *send_buf, enum msg_type type, uintptr_t val, uintptr_t *word, size_t length)
{
    if (length > URPC_NUM_DATA - 1)
    {
        return LIB_ERR_MSGBUF_OVERFLOW;
    }
    uintptr_t msg[length + 2];
    memset(msg, 0, sizeof(uintptr_t) * (length + 2));
    msg[UMP_TYPE_SLOT] = type;
    msg[UMP_GENERIC_VAL_SLOT] = val;
    if (length > 0)
    {
        memcpy((msg + 2), word, length * sizeof(uintptr_t));
    }
    return urpc_buffer_write_word(send_buf, msg, length + 2);
}

errval_t aos_ump_write_bind_fail(struct urpc_buffer_descriptor *send_buf, uintptr_t *word, size_t length)
{
    return urpc_buffer_write_message(send_buf, MSG_TYPE_BIND_FAIL, word, length);
}

errval_t urpc_buffer_write_word(struct urpc_buffer_descriptor *send_buf, uintptr_t *word, size_t length)
{
    assert(send_buf != NULL);
    if (length > URPC_NUM_DATA + 1)
    {
        return LIB_ERR_MSGBUF_OVERFLOW;
    }

    if (send_buf->tx->state == URPC_BUFFER_READY)
    {
        return LIB_ERR_UMP_CHAN_FULL;
    }
    //debug_printf("urpc_buffer_write_word write msg: %s\n",msg_to_string(word[0]));
    dmb(); //Ensure the check for the READY Check happens before the actuall writing
    uintptr_t *dest = (uintptr_t *)send_buf->tx->data;

    for (int i = 0; i < length; i++)
    {
        dest[i] = word[i];
    }
    dmb();
    send_buf->tx->state = URPC_BUFFER_READY;
    send_buf->tx++;
    if (send_buf->tx > send_buf->last)
    {
        //debug_printf("URPC_write: wrapping around\n");
        send_buf->tx = send_buf->buffer;
    }
    return SYS_ERR_OK;
}

errval_t urpc_buffer_write_block(struct urpc_buffer_descriptor *send_buf, char *data, size_t data_length)
{
    errval_t err;
    size_t size = 0;
    size_t ret = 0;
    while (size != data_length)
    {
        err = urpc_buffer_write(send_buf, data + size, data_length - size, &ret);
        if (err_is_fail(err))
        {
            return err;
        }
        size += ret;
    }
    return SYS_ERR_OK;
}

errval_t urpc_buffer_write(struct urpc_buffer_descriptor *send_buf, char *data, size_t data_length, size_t *written)
{
    size_t bytes_per_cache_line = URPC_DATA_SIZE; // WE need one word to keep state
    // We must figure out the number of cache lines we need
    size_t buckets = (data_length / (bytes_per_cache_line));
    size_t leftover_data = data_length % bytes_per_cache_line; // bytes that didn't fit
    //debug_printf("buckets: %d, leftover: %d\n",buckets,leftover_data);
    if (leftover_data != 0)
    {
        buckets++;
    }
    size_t length = data_length;
    for (int i = 0; i < buckets; i++)
    {
        if (send_buf->tx->state == URPC_BUFFER_READY)
        {
            break;
        }
        dmb(); //Ensure the check for the READY Check happens before the actuall writing
        char *dest = (char *)send_buf->tx->data;
        for (int j = 0; j < bytes_per_cache_line && length != 0; j++)
        {
            dest[j] = *data;
            data++;
            length--;
        }
        dmb(); // Do we need these barriers? same for read
        send_buf->tx->state = URPC_BUFFER_READY;
        //debug_printf("We wrote at %p on core %d\n", send_buf->tx, my_core_id);
        send_buf->tx++;
        if (send_buf->tx > send_buf->last)
        {
            //debug_printf("URPC_write: wrapping around\n");
            send_buf->tx = send_buf->buffer;
        }
    }
    if (written)
    {
        *written = data_length - length;
    }
    return SYS_ERR_OK;
}

errval_t urpc_buffer_read_words(struct urpc_buffer_descriptor *recv_buf, uintptr_t *dest)
{
    // We need to find a place to store the data. We will just print it out now
    struct urpc_slot *curr_rx = recv_buf->rx;
    if (curr_rx->state != URPC_BUFFER_READY)
    {
        return LIB_ERR_NO_UMP_MSG;
    }
    dmb(); // Do we need these barriers? same for write
    //debug_printf("reading %d bytes\n", URPC_DATA_SIZE);
    for (int i = 0; i < URPC_NUM_DATA; i++)
    {
        dest[i] = curr_rx->data[i];
    }
    memset(curr_rx->data, 0, sizeof(curr_rx->data));
    dmb(); // Do we need these barriers? same for write
    curr_rx->state = URPC_BUFFER_ACK;
    recv_buf->rx++;
    if (recv_buf->rx > recv_buf->last)
    {
        recv_buf->rx = recv_buf->buffer;
    }
    return SYS_ERR_OK;
}

errval_t urpc_buffer_read_single(struct urpc_buffer_descriptor *recv_buf, char *dest)
{
    return urpc_buffer_read_block(recv_buf, dest, URPC_DATA_SIZE);
}

errval_t urpc_buffer_read_block(struct urpc_buffer_descriptor *recv_buf, char *dest, size_t length)
{
    size_t ret_length = 0;
    size_t read_length = 0;
    errval_t err;
    while (read_length != length)
    {
        err = urpc_buffer_read(recv_buf, dest + read_length, length - read_length, &ret_length);
        if (err_is_fail(err))
        {
            return err;
        }
        read_length += ret_length;
    }
    return SYS_ERR_OK;
}

struct urpc_buffer_container *get_last_urpc_buffer_container(struct urpc_buffer_container *urpc_start)
{
    for (struct urpc_buffer_container *curr = urpc_start; curr != NULL; curr = curr->next)
    {
        if (curr->next == NULL)
        {
            return curr;
        }
    }
    return NULL;
}

struct urpc_buffer_container *get_urpc_buffer_container(struct urpc_buffer_container *urpc_start, coreid_t core, domainid_t pid)
{
    for (struct urpc_buffer_container *curr = urpc_start; curr != NULL; curr = curr->next)
    {
        if (curr->coreid == core && curr->pid == pid)
        {
            return curr;
        }
    }
    return NULL;
}

errval_t urpc_buffer_read(struct urpc_buffer_descriptor *recv_buf, char *dest, size_t length, size_t *ret_length)
{
    // We need to find a place to store the data. We will just print it out now
    struct urpc_slot *curr_rx = recv_buf->rx;
    if (curr_rx->state != URPC_BUFFER_READY)
    {
        *ret_length = 0;
        return SYS_ERR_OK;
    }
    if (length > URPC_DATA_SIZE)
    {
        length = URPC_DATA_SIZE;
    }
    *ret_length = length;

    //debug_printf("We are trying to read from the urpc at %p in the vaddr space of %d\n",curr_rx, my_core_id);
    dmb(); // Do we need these barriers? same for write
    //debug_printf("reading %d bytes\n", URPC_DATA_SIZE);
    for (int i = 0; i < length; i++)
    {
        dest[i] = ((char *)curr_rx->data)[i];
    }
    //TODO: Ensure readed data via protocol and remove memset
    memset(curr_rx->data, 0, sizeof(curr_rx->data));
    dmb(); // Do we need these barriers? same for write
    curr_rx->state = URPC_BUFFER_ACK;
    recv_buf->rx++;
    if (recv_buf->rx > recv_buf->last)
    {
        recv_buf->rx = recv_buf->buffer;
    }
    return SYS_ERR_OK;
}

errval_t init_urpc_descriptors(struct urpc_buffer_container **container,
                               lvaddr_t urpc_recv_buf, lvaddr_t urpc_send_buf, coreid_t core, domainid_t pid)
{
    //debug_printf("init descriptors\n");
    assert(*container == NULL);
    *container = malloc(sizeof(struct urpc_buffer_container));
    if (!*container)
    {
        return LIB_ERR_MALLOC_FAIL;
    }
    struct urpc_buffer_container *last = *container;
    last->next = NULL;
    last->coreid = core;
    last->pid = pid;
    last->recv_buf = malloc(sizeof(struct urpc_buffer_descriptor));
    last->send_buf = malloc(sizeof(struct urpc_buffer_descriptor));
    if (!last->recv_buf || !last->send_buf)
    {
        return LIB_ERR_MALLOC_FAIL;
    }
    // debug_printf("urpc_recv_buf: %p urpc_send_buf: %p in %s on core: %d\n", urpc_recv_buf, urpc_send_buf, __FUNCTION__, disp_get_core_id());
    last->recv_buf->buffer = (struct urpc_slot *)urpc_recv_buf;
    last->recv_buf->tx = last->recv_buf->buffer;
    last->recv_buf->rx = last->recv_buf->buffer;
    last->recv_buf->ack = last->recv_buf->buffer + URPC_NUM_SLOTS;
    last->recv_buf->last = last->recv_buf->buffer + URPC_NUM_SLOTS - 1;

    last->send_buf->buffer = (struct urpc_slot *)urpc_send_buf;
    last->send_buf->tx = last->send_buf->buffer;
    last->send_buf->rx = last->send_buf->buffer;
    last->send_buf->ack = last->send_buf->buffer + URPC_NUM_SLOTS;
    last->send_buf->last = last->send_buf->buffer + URPC_NUM_SLOTS - 1;

    return SYS_ERR_OK;
}

void debug_printf_urpcbuf(struct urpc_buffer_descriptor *buf)
{
    debug_printf("BUFFER:");
    for (int e = 0; e < 16; e++)
    {
        struct urpc_slot *start = buf->buffer + e;

        if (start->state == URPC_BUFFER_EMPTY)
        {
            debug_printf_clean("|E");
        }
        else if (start->state == URPC_BUFFER_ACK)
        {
            debug_printf_clean("|A");
        }
        else if (start->state == URPC_BUFFER_READY)
        {
            debug_printf_clean("|R");
        }
        else
        {
            debug_printf_clean("|F");
        }
        if (start == buf->ack)
        {
            debug_printf_clean("(AP)");
        }
        if (start == buf->tx)
        {
            debug_printf_clean("(TX)");
        }
        if (start == buf->rx)
        {
            debug_printf_clean("(RX)");
        }
        if (start == buf->last)
        {
            debug_printf_clean("(LP)");
        }
        debug_printf_clean("|");
    }
    debug_printf_clean("\n");
}

void aos_ack_receive(uintptr_t *words, coreid_t *dest_core, domainid_t *dest_pid,
                     coreid_t *source_core, domainid_t *source_pid)
{
    assert(MSG_TYPE_BIND_ACK == words[UMP_TYPE_SLOT]);
    *dest_core = words[1];
    *dest_pid = words[2];
    *source_core = words[3];
    *source_pid = words[4];
}

errval_t aos_ump_bind_fail_handler(uintptr_t *ret)
{
    debug_printf("in aos_ump_bind_fail_handler\n");
    coreid_t dest_core;
    errval_t err;
    domainid_t dest_pid;
    coreid_t source_core;
    domainid_t source_pid;
    genpaddr_t frame_addr;
    size_t bytes;
    aos_rpc_bind_receive(ret, &dest_core, &dest_pid, &source_core, &source_pid, &frame_addr, &bytes);
    //debug_printf("get %s from core: %d from pid: %d to core: %d, pid: %d with frame: %" PRIx64 " and size: %d\n", msg_to_string(ret[UMP_TYPE_SLOT]), source_core, source_pid, dest_core, dest_pid, frame_addr, bytes);
    // TODO: Get rpc channel to source
    // If on same core forward fail
    if (source_core == disp_get_core_id())
    {
        if (source_pid == INIT_PID)
        {
            return LIB_ERR_NOT_IMPLEMENTED;
        }
        struct lmp_chan *chan = get_rpc_by_pid(pt_head, source_core,source_pid);
        if (chan)
        {
            struct aos_rpc rpc;
            memcpy(&rpc.chan, chan, sizeof(struct lmp_chan));
            err = aos_rpc_send_bind_fail(&rpc, dest_core, dest_pid, source_core, source_pid, frame_addr, bytes);
            if(err_is_fail(err)) {
              DEBUG_ERR(err, "aos_rpc_send_bind_fail");
            }
            return err;
        }
    }
    else
    {
        return LIB_ERR_NOT_IMPLEMENTED;
    }
    return LIB_ERR_SHOULD_NOT_GET_HERE;
}

errval_t aos_ump_bind_ack_handler(uintptr_t *ret)
{
    coreid_t dest_core;
    //errval_t err;
    domainid_t dest_pid;
    coreid_t source_core;
    domainid_t source_pid;
    genpaddr_t frame_addr;
    size_t bytes;
    aos_rpc_bind_receive(ret, &dest_core, &dest_pid, &source_core, &source_pid, &frame_addr, &bytes);
    if (dest_core == disp_get_core_id())
    {
        struct lmp_chan *target_chan = get_rpc_by_pid(pt_head, dest_core, dest_pid);
        if (!target_chan)
        {
            return LIB_ERR_CHILD_NOT_FOUND;
        }
        struct aos_rpc rpc;
        memcpy(&rpc.chan, target_chan, sizeof(struct lmp_chan));
        aos_rpc_forward_bind(&rpc, MSG_TYPE_BIND_ACK, dest_core, dest_pid, source_core, source_pid, frame_addr, bytes);
    }

    return SYS_ERR_OK;
}

errval_t aos_ump_bind_handler(uintptr_t *ret)
{
    coreid_t dest_core;
    errval_t err;
    domainid_t dest_pid;
    coreid_t source_core;
    domainid_t source_pid;
    genpaddr_t frame_addr;
    size_t bytes;
    aos_rpc_bind_receive(ret, &dest_core, &dest_pid, &source_core, &source_pid, &frame_addr, &bytes);
    //debug_printf("get %s from core: %d from pid: %d to core: %d, pid: %d with frame: %" PRIx64 " and size: %d\n", msg_to_string(ret[UMP_TYPE_SLOT]), source_core, source_pid, dest_core, dest_pid, frame_addr, bytes);
    if (dest_core == disp_get_core_id())
    {

        struct urpc_buffer_container *source_init = get_urpc_buffer_container(urpc_head, source_core, INIT_PID);
        lvaddr_t urpc_frame_addr;
        if (dest_pid == INIT_PID)
        {
            struct capref urpc_frame;
            slot_alloc(&urpc_frame);
            err = frame_forge(urpc_frame, frame_addr, bytes, dest_core);
            if (err_is_fail(err))
            {
                debug_printf("Error while frame forge for bind request to init\n");
                aos_ump_write_bind_fail(source_init->send_buf, ret + 1, 7);
            }
            paging_map_frame_attr(get_current_paging_state(), (void **)&urpc_frame_addr, bytes, urpc_frame, VREGION_FLAGS_READ_WRITE, NULL, NULL);
            struct urpc_buffer_container *last = get_last_urpc_buffer_container(urpc_head);
            init_urpc_descriptors(&last->next, urpc_frame_addr, urpc_frame_addr + (URPC_NUM_SLOTS * sizeof(struct urpc_slot)), source_core, source_pid);
        }
        else
        {
            struct lmp_chan *chan = get_rpc_by_pid(pt_head, dest_core,dest_pid);
            if (chan)
            {
                struct aos_rpc rpc;
                memcpy(&rpc.chan, chan, sizeof(struct lmp_chan));
                err = aos_rpc_forward_bind(&rpc, MSG_TYPE_BIND_REQ, dest_core, dest_pid, source_core, source_pid, frame_addr, bytes);
                if (err_is_fail(err))
                {
                    DEBUG_ERR(err, "in aos_rpc_forward_bind");
                    aos_ump_write_bind_fail(source_init->send_buf, ret + 1, 7);
                    return err;
                }
                return SYS_ERR_OK;
            }
            else
            {
                aos_ump_write_bind_fail(source_init->send_buf, ret + 1, 7);
                return LIB_ERR_UMP_CHAN_BIND;
            }
        }

        uintptr_t msg[4];
        msg[0] = source_core;
        msg[1] = source_pid;
        msg[2] = dest_core;
        msg[3] = dest_pid;
        err = urpc_buffer_write_message(source_init->send_buf, MSG_TYPE_BIND_ACK, msg, 4);
        if (err_is_fail(err))
        {
            return err;
        }
    }
    return SYS_ERR_OK;
}

bool err_is_fail_ump(errval_t err)
{
    if (err == LIB_ERR_NO_UMP_MSG)
    {
        return false;
    }
    else
    {
        return err_is_fail(err);
    }
}

errval_t forward_rpc_to_urpc(struct urpc_buffer_container *urpc_target,struct aos_rpc *chan) {
    errval_t err;
    while ((err = urpc_buffer_write_word(urpc_target->send_buf, chan->msg->words, 9)) == LIB_ERR_UMP_CHAN_FULL)
        ;
    return err;
}

void debug_print_urpc_head(struct urpc_buffer_container *urpc_start)
{
    uint32_t i = 0;
    for (struct urpc_buffer_container *curr = urpc_start; curr != NULL; curr = curr->next)
    {
        debug_printf("urpc_container %d: core: %d, pid: %d, recv_addr: %p send_addr: %p\n", i, curr->coreid, curr->pid, curr->recv_buf->buffer, curr->send_buf->buffer);
        i++;
    }
}

struct urpc_buffer_container *get_urpc_container_from_core_info(struct spawn_core_info *core_info)
{
    for (struct urpc_buffer_container *curr = urpc_head; curr != NULL; curr = curr->next)
    {
        if (curr->coreid == core_info->coreid && curr->pid == INIT_PID)
        {
            return curr;
        }
    }
    return NULL;
}

errval_t aos_rpc_upc_waitloop(struct waitset *ws,struct urpc_buffer_container **head) {
    errval_t err;
    while (true)
    {
        err = event_dispatch_non_block(ws);
        if (err_is_fail(err) && err != LIB_ERR_NO_EVENT)
        {
            DEBUG_ERR(err, "in event_dispatch");
            return err;
        }
            for (struct urpc_buffer_container *urpc_curr = *head; urpc_curr != NULL; urpc_curr = urpc_curr->next)
            {
                err = urpc_buffer_read_line(urpc_curr);
                if (err_is_fail_ump(err))
                {
                    DEBUG_ERR(err, "Failure in urpc_buffer_write\n");
                    return err;
                }
            }

        thread_yield();
    }
    return err;
}
